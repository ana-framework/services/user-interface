# Vue base web

This is the User interface "UI" for the Advanced automation tool "ana". The project is a vue single web page. This user
UI is a user-friendly way to use the GitLab dev ops api.

## Installation

The UI is a part of the ANA Framework and can be installed wit Ana helm chart.

## Development

The development is based on docker and docker-compose. The docker-compose file is used to build the project and install 
all the dependencies. The configuration use volume to map the project folder to the container. This way, you can edit 
the code and the changes will be reflected in the container. the container use the tty to allow you to use the terminal 
inside the container. Also we have tow environments, one for development and one for production. The development is 
based on a node image and allow you te positivity to run the UI in a simple node server. The production environment is 
based on a nginx image and serve the static files.

### Build setup

1. Run the container to start the development environment
```bash
docker compose -f compose.build.yaml up -d
```

2. Enter the container
```bash
docker compose -f compose.build.yaml exec server bash
```
3. Install the dependencies
```bash
npm install
```

4. Run the development server
```bash
npm run dev -- --host 0.0.0.0 --port 8095
```

This run the server for the dev mode and allow you to access the server from the host machine.

5. Open the browser and go to http://localhost:8095

You can also lint the code with the command:
```bash
npm run html-lint
```
this command create a html lint report ``eslint-report.html`` in the root of the project. 

### Development setup

1. Run the container to start the development environment
```bash
docker compose -f compose.dev.yaml up --build -d
```

2. Open the browser and go to http://localhost:8095

### Production setup

1. Run the container to start the development environment
```bash
docker compose -f compose.yaml up --build -d
```

2. Open the browser and go to http://localhost:8095

### Scripts

In the scripts folder you can find some scripts to help you to develop and build the project. The scripts are:
* hard_clean.sh: This script delete all the containers, images and volumes. This is useful to clean the environment.
* run.sh: This script run the project in different environments. 
The run.sh script hav a set of param and flags to facilitate the devlop an the build. Yo can see the parameters and 
flags at this way:
```bash
./scripts/run.sh -h
```

The output is:
```bash
Help information:

Usage: ./scripts/run.sh [ENVIRONMENT] [FLAGS]

ENVIRONMENT:
- build (default): Run the user interface using only the node image. This allow run teh application in an
                   environment similar to production but with http-server in node.
- dev: Run the user interface in a develop environment. Your can use the docker to install new
       packages and develop the application.
- prod: Run the user interface in a environment similar to production.

FLAGS:
-i: Optional argument to install all libraries. This is only valid for build environment.
-f: Optional argument to follow the logs of the containers. This is only valid for prod and development environment.
-r, --restart: Optional argument to restart the project.
-h, --help: Show this help information.

All functionality is based on docker-compose. For more information, see the documentation.
```

------------------------------------------
# Modifications ALejandro

  vee-validate (imported by /app/src/components/forms/NewEventForm.vue?id=0)
  @validations (imported by /app/src/components/inventory/IntegrationDeleteModal.vue?id=0)
  ansi_up (imported by /app/src/components/components/GitlabViews/pipeline/report/LogsConsoleComponent.vue?id=0)
  vue-ripple-directive (imported by /app/src/components/inventory/IntegrationDeleteModal.vue?id=0)
  vue-form-wizard (imported by /app/src/components/forms/PipelineSetupFormComponent.vue?id=0)
  app/src/store (imported by /app/src/components/error/Error404.vue?id=0)
  vue-select (imported by /app/src/components/forms/PipelineSetupFormComponent.vue?id=0)
  cron-parser (imported by /app/src/components/components/GitlabViews/ScheduleTableComponent.vue?id=0)
  vue-form-wizard/dist/vue-form-wizard.min.css (imported by /app/src/components/forms/PipelineSetupFormComponent.vue?id=0)
  date-fns (imported by /app/src/components/components/GitlabViews/pipeline/comon/pipelineMixin.js)
  
ICON WEB PAGE:
https://tablericons.com/

VUEXY WEB PAGE:
https://demos.pixinvent.com/vuexy-vuejs-admin-template/demo-1/components/chip

VUE3 WEB PAGE:
https://v3.router.vuejs.org/api/#to

VUE-FLOW
https://vueflow.dev/examples/transition.html

To add an Icon:
<VIcon icon="tabler-layout-grid" class="align-top" size="22"/>



------------------------------------------
settings added new file route
seettings inside view
  event-delete-modal
  event-list
  settingpage



---
1. Base URL Configuration
The createRouter function is initialized with createWebHistory(import.meta.env.BASE_URL). Ensure that import.meta.env.BASE_URL is correctly defined in your environment settings:

If you're using Vite, import.meta.env.BASE_URL should be automatically set based on your vite.config.js. Make sure this is correctly pointing to the root of where your application is served. For example, if your application is served from the root (/), BASE_URL should be '/'. If it's from a subdirectory, it should reflect that, such as '/subdirectory/'.


#### FIXED all the environment variables

the vuetify was chenged from:

    "vuetify": "3.3.22",   to   3.7.0
to


    "vite-plugin-vuetify": "1.0.2",  to 2.0.3


ACTUALICE la version de node a la 14

curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs

node -v
npm -v

First, you can try to remove the package safely with:
bash
Copy code
sudo apt-get remove libnode-dev
If you are not actively using this package for development, removing it should not impact your projects. However, if it is needed, you might want to check whether there are updated versions compatible with the newer Node.js.
2. Clean Up and Update Your Package Lists
After removing the conflicting package, clean up your system and update the package lists to ensure that all repositories are up-to-date:

bash
Copy code
sudo apt-get clean
sudo apt-get update
3. Retry Installing Node.js
Once the conflict is resolved and your package lists are updated, try installing Node.js again:

bash
Copy code
sudo apt-get install nodejs
This should install the new version without conflicts.

4. Verify Node.js Installation
After installation, verify that Node.js has been updated successfully by checking the version:

bash
Copy code
node -v
5. Consider Using dpkg with Force Options (If Needed)
If the above steps do not resolve the conflict and you are sure that overwriting the conflicting files will not break your system, you can force the installation using dpkg. Be cautious with this approach as it can lead to unstable system states.

bash
Copy code
sudo dpkg --install --force-overwrite /var/cache/apt/archives/nodejs_20.12.2-1nodesource1_amd64.deb
This command tells dpkg to forcefully overwrite the conflicting files.

6. Resolve Any Remaining Dependency Issues
After forcing the installation, ensure all dependencies are correctly installed and there are no residual issues:

bash
Copy code
sudo apt-get -f install
This command will fix broken dependencies and finish configuring any partially installed packages.


import: npm install pdfjs-dist
