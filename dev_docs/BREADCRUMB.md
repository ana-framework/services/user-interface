  {
    path: '/settings/subscriber-new',
    name: 'settings-subscriber-new',
    component: () => import('@/views/settings/SubscriberNew.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      roles: ['admin', 'root'],
      pageTitle: 'New subscriber',
      breadcrumb: [
        {
          text: 'Settings',
          to: { name: 'settings-page' },
        },
        {
          text: 'Subscribers',
          to: { name: 'settings-subscribers-list' },
        },
      ],
    },
  },
  {
    path: '/settings/subscriber-edit/:id',
    name: 'settings-subscriber-edit',
    component: () => import('@/views/settings/SubscriberEdit.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      roles: ['admin', 'root'],
      pageTitle: 'Edit subscriber',
      breadcrumb: [
        {
          text: 'Settings',
          to: { name: 'settings-page' },
        },
        {
          text: 'Subscribers',
          to: { name: 'settings-subscribers-list' },
        },
      ],
    },
  },