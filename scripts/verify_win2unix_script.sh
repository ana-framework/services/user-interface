# Directorio padre (un nivel arriba del actual)
PARENT_DIR="../"

# Verifica si el directorio existe
if [ ! -d "$PARENT_DIR" ]
then
    echo "El directorio $PARENT_DIR no existe."
    exit 1
fi

# Recorre todos los archivos en el directorio padre y subdirectorios
find "$PARENT_DIR" -type f -print0 | while IFS= read -r -d '' file; do
    # Verifica si el archivo está en formato Windows (CRLF)
    if file "$file" | grep -q "CRLF"; then
        # Imprime el nombre del archivo en rojo
        echo -e "\e[31mERROR: $file está en formato Windows (CRLF)\e[0m"
    fi
done

echo "Verificación completada."