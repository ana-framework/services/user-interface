import subprocess
from datetime import datetime
from typing import Tuple


def generate():
    # Get the current version from the latest Git tag
    try:
        tag = subprocess.check_output(['git', 'describe', '--abbrev=0', '--tags']).strip().decode()
    except subprocess.CalledProcessError:
        print("No tags found, start with initial version")
        exit(1)

    now = datetime.now()
    year = int(now.strftime("%y"))
    month = int(now.strftime("%m"))
    year, month = get_date(year, month)

    # transform to string
    major, minor, patch = map(lambda x: int(x), tag.split('.'))
    if year != major or month != minor:
        major, minor = year, month
        patch = '0'
    else:
        patch = str(int(patch) + 1)

    return f"{major}.{minor}.{patch}"


def get_date(year: int, month: int) -> Tuple[int, int]:
    if month <= 4:
        return year, 4
    elif month <= 10:
        return year, 10
    else:
        return int(year) + 1, 4


if __name__ == '__main__':
    print(generate())
