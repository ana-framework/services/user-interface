#!/bin/bash
# Version: 2.2.0
# Date: 2023-05-31
# Source: msn-service
# Objective:
# Description:
# ChangeLog:
# * 2.1.0 Version
# - added help for script
# * 2.2.0 Version
# - added mor help
# - the purpose of the environments change

INSTALL_LIBS=false
LAUNCH_LINT=false
LOGS=''

if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
  echo "Help information:"
  echo
  echo "Usage: $0 [ENVIRONMENT] [FLAGS]"
  echo
  echo "ENVIRONMENT:"
  echo "- build (default): Run the user interface using only the node image. This allow run teh application in an"
  echo "                   environment similar to production but with http-server in node."
  echo "- dev: Run the user interface in a develop environment. Your can use the docker to install new"
  echo "       packages and develop the application."
  echo "- prod: Run the user interface in a environment similar to production."
  echo
  echo "FLAGS:"
  echo "-i: Optional argument to install all libraries. This is only valid for build environment."
  echo "-l: Optional argument to launch the linting. This is only valid for build environment."
  echo "-f: Optional argument to follow the logs of the containers. This is only valid for prod and development environment."
  echo "-r, --restart: Optional argument to restart the project."
  echo "-h, --help: Show this help information."
  echo
  echo "All functionality is based on docker-compose. For more information, see the documentation."
  exit 0
fi

if [ "$1" = "prod" ]; then
  echo "Running in production environment"
  COMPOSE_FILE="compose.yaml"
  ENVIRONMENT="prod"
elif [ "$1" = "dev" ]; then
  echo "Running in development environment"
  COMPOSE_FILE="compose.dev.yaml"
  ENVIRONMENT="dev"
else
  echo "Running in build environment"
  COMPOSE_FILE="compose.build.yaml"
  ENVIRONMENT="build"
fi

echo "==========================================="

remove_docker_images() {
  echo "Removing docker images"
  docker compose -f "$COMPOSE_FILE" down --rmi all -v
}

if [[ "$1" = "-r" || "$1" = "--restart" || "$2" = "-r" || "$2" = "--restart" ]]; then
  echo
  echo "1) Optional project restart"
  remove_docker_images
fi

for arg in "$@"
do
    case $arg in
        -l)
        LAUNCH_LINT=true
        shift # Remove -l from processing
        ;;
        -i)
        INSTALL_LIBS=true
        shift # Remove -i from processing
        ;;
        -f)
        LOGS='-f'
        shift # Remove -f from processing
        ;;
        *)
        shift # Remove generic argument from processing
        ;;
    esac
done

echo
echo "2) Starting docker containers"
docker compose -f "$COMPOSE_FILE" up --build -d || exit

if [ "$ENVIRONMENT" = "build" ]; then

  echo
  echo "3) Installing all libraries ..."
  if [ "$INSTALL_LIBS" = true ] ; then
    docker compose -f "$COMPOSE_FILE" exec server npm install
  fi

  echo
  echo "4) Running linting ..."
  if [ "$LAUNCH_LINT" = true ] ; then
    docker compose -f "$COMPOSE_FILE" exec server bash -c "npm run html-lint --no-fix" || exit
  fi

  echo
  echo "5) Tests and coverage ..."
#  docker compose -f "$COMPOSE_FILE" exec build bash -c "npm run test" || exit

  echo
  echo "6) documentation ..."
#  docker compose -f "$COMPOSE_FILE" exec build bash -c "npm run doc" || exit

  echo
  echo "7) run bash inside the container"
  docker compose -f "$COMPOSE_FILE" exec server bash
else
  echo
  echo "3) Waiting for the app service"
  while ! curl http://127.0.0.1:8095 -m1 -o/dev/null -s; do
    echo "Waiting ..."
    sleep 1
  done

  echo
  echo "4) Services status"
  docker compose -f "$COMPOSE_FILE" ps

  echo
  echo "5) Showing logs"
  docker compose -f "$COMPOSE_FILE" logs server "$LOGS"

  echo
  echo "App is up"
fi

