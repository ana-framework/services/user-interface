#!/bin/bash

# Verifica si la herramienta dos2unix está instalada
if ! command -v dos2unix &> /dev/null
then
    echo "La herramienta dos2unix no está instalada. Instalándola ahora..."
    sudo apt-get update
    sudo apt-get install -y dos2unix

    # Verifica nuevamente si la instalación fue exitosa
    if ! command -v dos2unix &> /dev/null
    then
        echo "Error: No se pudo instalar dos2unix. Por favor, instálala manualmente."
        exit 1
    fi
fi

# Directorio padre (un nivel arriba del actual)
PARENT_DIR="../"

# Verifica si el directorio existe
if [ ! -d "$PARENT_DIR" ]
then
    echo "El directorio $PARENT_DIR no existe."
    exit 1
fi

# Recorre todos los archivos en el directorio padre y subdirectorios
find "$PARENT_DIR" -type f -print0 | while IFS= read -r -d '' file; do
    # Verifica si el archivo está en formato Windows (CRLF)
    if file "$file" | grep -q "CRLF"; then
        echo "Convirtiendo $file a formato Unix..."
        dos2unix "$file"
    fi
done

echo "Conversión completada."
