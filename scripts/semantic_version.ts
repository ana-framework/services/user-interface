const { execSync } = require('child_process');
const moment = require('moment');

function generate(verbose = false) {
    let tag;
    let major, minor, patch;

    const now = moment();
    const year = now.year() % 100;
    const month = now.month() + 1;

    try {
        tag = execSync('git describe --abbrev=0 --tags', { encoding: 'utf8' }).trim();
        [major, minor, patch] = tag.split('.').map(Number);
    } catch (error) {
        if (verbose) {
            console.log("No tags found, using current date for version");
        }
    }

    const [newYear, newMonth] = getDate(year, month);

    if (!tag || newYear !== major || newMonth !== minor) {
        return `${newYear}.${newMonth}.0`;
    } else {
        return `${major}.${minor}.${patch + 1}`;
    }
}

function getDate(year, month) {
    if (month <= 4) {
        return [year, 4];
    } else if (month <= 10) {
        return [year, 10];
    } else {
        return [year + 1, 4];
    }
}

const verbose = process.argv.includes('--verbose');
console.log(generate(verbose));