import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { fileURLToPath } from 'node:url'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'

// vite.config.js o vite.config.ts
import { defineConfig } from 'vite'


// import { VueRouterAutoImports, getPascalCaseRouteName } from 'unplugin-vue-router'
// import VueRouter from 'unplugin-vue-router/vite'
import Layouts from 'vite-plugin-vue-layouts'
import vuetify from 'vite-plugin-vuetify'

/*  1. Base URL Configuration
    The createRouter function is initialized with createWebHistory(import.meta.env.BASE_URL).
    Ensure that import.meta.env.BASE_URL is correctly defined in your environment settings:

    If you're using Vite, import.meta.env.BASE_URL should be automatically set based on your vite.config.js.
    Make sure this is correctly pointing to the root of where your application is served.
    For example, if your application is served from the root (/), BASE_URL should be '/'. If it's from a subdirectory,
    it should reflect that, such as '/subdirectory/'.
* */

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    // Docs: https://github.com/posva/unplugin-vue-router
    // ℹ️ This plugin should be placed before vue plugin
    /// /VueRouter({
    /// /  getRouteName: routeNode => {
    /// /    // Convert pascal case to kebab case
    /// /    return getPascalCaseRouteName(routeNode)
    /// /      .replace(/([a-z0-9])([A-Z])/g, '$1-$2')
    /// /      .toLowerCase()
    /// /  },
    ////
    /// /}),
    vue({
      template: {
        compilerOptions: {
          isCustomElement: tag => tag === 'swiper-container' || tag === 'swiper-slide',
        },
      },
    }),
    vueJsx(),

    // Docs: https://github.com/vuetifyjs/vuetify-loader/tree/master/packages/vite-plugin
    vuetify({
      styles: {
        configFile: 'src/assets/styles/variables/_vuetify.scss',
      },
    }),

    // Docs: https://github.com/johncampionjr/vite-plugin-vue-layouts#vite-plugin-vue-layouts
    Layouts({
      layoutsDirs: './src/layouts/',
    }),

    // Docs: https://github.com/antfu/unplugin-vue-components#unplugin-vue-components
    Components({
      dirs: ['src/@core/components', 'src/views/demos', 'src/components'],
      dts: true,
      resolvers: [
        componentName => {
          // Auto import `VueApexCharts`
          if (componentName === 'VueApexCharts')
            return { name: 'default', from: 'vue3-apexcharts', as: 'VueApexCharts' }
        },
      ],
    }),

    // Docs: https://github.com/antfu/unplugin-auto-import#unplugin-auto-import
    AutoImport({
      imports: ['vue', '@vueuse/core', '@vueuse/math', 'vue-i18n', 'pinia'],
      dirs: [
        './src/@core/utils',
        './src/@core/composable/',
        './src/composables/',
        './src/utils/',
        './src/plugins/*/composables/*',
      ],
      vueTemplate: true,

      // ℹ️ Disabled to avoid confusion & accidental usage
      ignore: ['useCookies', 'useStorage'],
    }),

  ],
  define: {

    // 🌐 Global Environment Settings
    // The following are placeholders for defining global environment variables.
    'process.env': {
    },

    // 🔗 Base URLs Configuration
    // group (process.env) involves the more traditional Node.js process.env method of defining environment variables that are
    // typically used server-side but are defined here for global front-end access after build-time embedding.
    /*
    'process.env.BASE_URL': JSON.stringify(process.env.BASE_URL),
    'process.env.VITE_BASE_URL': JSON.stringify(process.env.VITE_BASE_URL),
    'process.env.BACKEND_URI': JSON.stringify(process.env.BACKEND_URI),
    'process.env.VITE_BACKEND_URI': JSON.stringify(process.env.VITE_BACKEND_URI),

    // 📡 API Endpoints Configuration
    // group (import.meta.env) uses Vite's built-in support for import.meta.env, which is more secure for client-side exposure,
    // allowing defaults to be set dynamically at runtime based on what's defined or defaults as fallbacks.
    'window.BACKEND_URI': JSON.stringify(process.env.BACKEND_URI || undefined),
    'window.USER_END_POINT': JSON.stringify(process.env.USER_END_POINT || undefined),
    'window.MSN_END_POINT': JSON.stringify(process.env.MSN_END_POINT || undefined),
    'window.GITLAB_END_POINT': JSON.stringify(process.env.GITLAB_END_POINT || undefined),
    'window.GITLAB_DEFAULT_PASSWORD': JSON.stringify(process.env.GITLAB_DEFAULT_PASSWORD || undefined),
    'window.GITLAB_USERNAME_PREFIX': JSON.stringify(process.env.GITLAB_USERNAME_PREFIX || undefined),
    'window.GITLAB_URI': JSON.stringify(process.env.GITLAB_URI || undefined),
    'window.GITLAB_VENDOR_ID': JSON.stringify(process.env.GITLAB_VENDOR_ID || undefined),
    'window.UI_BACKEND_PROJECT': JSON.stringify(process.env.UI_BACKEND_PROJECT || undefined),
    'window.UI_FORM_VERSION': JSON.stringify(process.env.UI_FORM_VERSION || undefined),
    'window.DOCS_URI': JSON.stringify(process.env.DOCS_URI || undefined),
    'window.KIBANA_URI': JSON.stringify(process.env.KIBANA_URI || undefined),
    'window.LONG_HORN_URI': JSON.stringify(process.env.LONG_HORN_URI || undefined),
    'window.NEXUS_URI': JSON.stringify(process.env.NEXUS_URI || undefined),
    'window.GRAFANA_URI': JSON.stringify(process.env.GRAFANA_URI || undefined),
    'window.MINIO_URI': JSON.stringify(process.env.MINIO_URI || undefined),
    'window.BUILD_NUMBER': JSON.stringify(process.env.BUILD_NUMBER || undefined),
    */
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      '@themeConfig': fileURLToPath(new URL('./themeConfig.ts', import.meta.url)),
      '@core': fileURLToPath(new URL('./src/@core', import.meta.url)),
      '@layouts': fileURLToPath(new URL('./src/@layouts', import.meta.url)),
      '@images': fileURLToPath(new URL('./src/assets/images/', import.meta.url)),
      '@styles': fileURLToPath(new URL('./src/assets/styles/', import.meta.url)),
      '@configured-variables': fileURLToPath(new URL('./src/assets/styles/variables/_template.scss', import.meta.url)),
      'apexcharts': fileURLToPath(new URL('node_modules/apexcharts-clevision', import.meta.url)),
      '@db': fileURLToPath(new URL('./src/plugins/fake-api/handlers/', import.meta.url)),
      '@api-utils': fileURLToPath(new URL('./src/plugins/fake-api/utils/', import.meta.url)),
      '@validations': fileURLToPath(new URL('./src/validations', import.meta.url)),
      '@components': fileURLToPath(new URL('./src/components', import.meta.url)),

      // 📁 Importing route configurations NO NEEDED
      '@composables': fileURLToPath(new URL('./src/composables', import.meta.url)),
      '@utils': fileURLToPath(new URL('./src/utils', import.meta.url)),
      '@plugins': fileURLToPath(new URL('./src/plugins', import.meta.url)),
      '@views': fileURLToPath(new URL('./src/views', import.meta.url)),
    },
  },
  build: {
    chunkSizeWarningLimit: 5000,
  },
  optimizeDeps: {
    exclude: ['vuetify'],
    entries: [
      './src/**/*.vue',
    ],
  },
})
