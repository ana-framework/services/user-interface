#!/bin/bash
SRC=dist/config.js

# quoting is required to keep line breaks
NEW_CONFIG="$(./create_config_js.sh $SRC)"
echo "$NEW_CONFIG" > $SRC

# start webserver, e.g. http-server
http-server -P http://localhost:8080? ./dist -a 0.0.0.0;
