#!/bin/bash
SRC=config.js

# quoting is required to keep line breaks
NEW_CONFIG="$(./create_config_js.sh $SRC)"
echo "$NEW_CONFIG" > $SRC

# start webserver, e.g. nginx
nginx -g 'daemon off;'
