# syntax=docker/dockerfile:1

ARG NODE_VERSION=20.11.0

################################################################################
# Use node image for base image for all stages.
FROM node:${NODE_VERSION}-bookworm-slim as base

# Set working directory for all build stages.
WORKDIR /app

################################################################################
# Create a stage for installing dependencies.
FROM base as install

# Copy package.json and package-lock.json into the image.
COPY package*.json ./

# Copy the specific file into the image.
COPY ../../src/plugins/iconify/build-icons.ts ./src/plugins/iconify/

# Install dependencies.
RUN npm ci

################################################################################
# Create a stage for building the application.
FROM install as build

# Copy the rest of the source files into the image.
COPY . .

# Build the application.
RUN npm run build --production

################################################################################
# Create a final stage for serving the application.
FROM nginx:1.20.2 as final

# Add nginx configuration file
RUN rm /etc/nginx/conf.d/default.conf
COPY img-build/bookworm-slim/nginx.conf /etc/nginx/conf.d

# Redirect nginx logs to stdout and stderr
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

# Copy static files from build-stage to nginx root directory
COPY --from=build /app/dist /usr/share/nginx/html

# Make the 'html' folder the current working directory
WORKDIR /usr/share/nginx/html

# Copy project files and folders to the current working directory (i.e. 'html' folder)
COPY create_config_js.sh .
RUN chmod +x create_config_js.sh
COPY img-build/bookworm-slim/entrypoint.sh .
RUN chmod +x entrypoint.sh

# Expose the port that the application listens on.
EXPOSE 80

# Serve the application.
CMD ["/bin/sh", "entrypoint.sh"]
