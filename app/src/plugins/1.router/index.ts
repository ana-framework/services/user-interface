import type { App } from 'vue';

import { setupLayouts } from 'virtual:generated-layouts';
import type { RouteRecordRaw } from 'vue-router/auto';

import { createRouter, createWebHistory } from 'vue-router/auto';

// 📁 Importing route configurations
import { settings as settingsRoutes } from '@/plugins/1.router/general-settings'; // Specific routes for settings section
import { routes as baseRoutes } from '@/plugins/1.router/routes'; // General routes for the application
import { userProfile as userRoutes } from '@/plugins/1.router/user-profile'; // Specific routes for user settings section

import store from '@/store';

function localLayouts(route: RouteRecordRaw): RouteRecordRaw {
  // Apply the layout to children first if they exist
  const processedRoutes = setupLayouts([route])

  return processedRoutes[0] // Ensure setupLayouts handles this correctly
}

// 🔗 Combine general and settings-specific routes into one array
// IMPORTANT ...pages is the defaults routes created automatically from all the pages components (Do not delete)
// 🛠️ Create a Vue Router instance
const router = createRouter({
  history: createWebHistory(window.BASE_URL), // Use browser history
  routes: [...baseRoutes, ...settingsRoutes, ...userRoutes].map(localLayouts),
})

router.beforeEach((to, from, next) => {
  // 🔍 Intenta obtener el usuario desde Vuex o localStorage
  // let user = store.getters.user || sessionStorage.getItem('user');
  let user = sessionStorage.getItem('user') || store.getters.user;
  // 📌 Si `user` viene de `localStorage`, parsearlo a objeto
  if (typeof user === 'string') {
    try {
      user = JSON.parse(user);
    } catch (error) {
      console.error('Error al parsear user desde localStorage:', error);
      user = null; // Evita errores si el JSON es inválido
    }
  }

  // 📌 Extrae el rol correctamente (puede ser 'rol' o 'role', revisa bien el JSON)
  // const userRole = user?.role || user?.rol || ''; // Asegura compatibilidad
  const userRole = (user?.role || user?.rol || '').toLowerCase(); // Asegura compatibilidad y convierte a minúsculas

  // 1️⃣ Si la ruta requiere autenticación y no hay token, redirigir a login
  if (to.meta.requiresAuth && !store.getters.token) {
    console.warn('🔴 No autenticado, redirigiendo a login...');
    return next({ name: 'login' });
  }

  // 2️⃣ Si la ruta tiene restricción de roles, verificar si el usuario tiene permiso
  if (to.meta.roles && to.meta.roles.length > 0) {  //  SI roles esta vacio [], no hay restricción
    if (!userRole || !to.meta.roles.includes(userRole.toLowerCase()) ) { 
      alert('🚫 No tienes permiso para acceder a esta página.');
      console.warn(`⛔ Acceso denegado. Rol actual: "${userRole}". Rutas permitidas:`, to.meta.roles);
      console.warn("from.fullPath", from.fullPath);
      return next(from.fullPath);   // Evita la navegación y redirige a la página path: '/' 
    }
  }

  next(); // ✅ Si todo está bien, continuar con la navegación
});


export { router };

// 🌐 Register router with the Vue application
export default function (app: App) {
  app.use(router)
}
