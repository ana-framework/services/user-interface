

export const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('@/views/Home.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      breadcrumb: [
        {
          title: 'Home',
          disabled: true,
          href: '/',
        },
      ],
    },
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/Login.vue'),
    meta: {
      layout: 'blank', // specify the layout for the login page
    },
  },
  {
    path: '/dashboards',
    name: 'dashboards',
    component: () => import('@/views/Dashboards.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      breadcrumb: [
        {
          title: 'Dashboards',
          disabled: true,
          href: '/dashboards',
        },
      ],
    },
  },
  {
    path: '/vendors/:id?',
    name: 'vendors',
    component: () => import('@/views/Vendors.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      breadcrumb: [
        {
          title: 'Vendors',
          disabled: false,
          href: '/vendors',
        },
      ],
    },
  },
  {
    path: '/vendors/:id/project/:projectId/workspace',
    name: 'workspace',
    component: () => import('@/views/Workspace.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      breadcrumb: [
        {
          title: 'Vendors',
          disabled: false,
          href: '/vendors/:id',
        },
        {
          title: 'Workspace',
          disabled: true,
          href: '/vendors/:id/project/:projectId/workspace',
        },
      ],
    },
  },
  {
    path: '/vendors/:id/project/:projectId/pipelines/:pipelineId/execution',
    name: 'vendors-pipeline-execution',
    component: () => import('@/views/pipeline/PipelineExecution.vue'),
    meta: {
      requiresAuth: true,
      breadcrumb: [
        {
          title: 'Search',
          disabled: false,
          href: '/search-pipeline',
        },
        {
          title: 'Execution result',
          disabled: true,
          href: '/vendors/:id/project/:projectId/pipelines/:pipelineId/execution',
        },
      ],
    },
  },
  {
    path: '/vendors/:id/projects/:projectId/jobs/:jobId',
    name: 'vendors-job-logs',
    component: () => import('@/views/vendors/jobLogs.vue'),
    meta: {
      roles: ['user', 'admin', 'developer', 'root'],
      pageTitle: 'Vendors - Job logs',
      breadcrumb: [
        {
          text: 'Vendors',
          to: { name: 'vendors' },
        },
      ],
    },
  },
  {
    path: '/search-pipeline',
    name: 'search-pipeline',
    component: () => import('@/views/search-pipeline.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      breadcrumb: [
        {
          title: 'Search Pipeline',
          disabled: true,
          href: '/search-pipeline',
        },
      ],
    },
  },
  {
    path: '/scheduler',
    name: 'scheduler',
    component: () => import('@/views/scheduler.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      breadcrumb: [
        {
          title: 'Scheduler',
          disabled: true,
          href: '/scheduler',
        },
      ],
    },
  },
  {
    path: '/integrations',
    name: 'integrations',
    component: () => import('@/views/integrations.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      breadcrumb: [
        {
          title: 'Integrations',
          disabled: true,
          href: '/integrations',
        },
      ],
    },
  },
  {
    path: '/infrastructure',
    name: 'infrastructure',
    component: () => import('@/views/infrastructure.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      breadcrumb: [
        {
          title: 'Infrastructure',
          disabled: true,
          href: '/infrastructure',
        },
      ],
    },
  },
  {
    path: '/templates',
    name: 'templates',
    component: () => import('@/views/templates.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      breadcrumb: [
        {
          title: 'Templates',
          disabled: true,
          href: '/templates',
        },
      ],
    },
  },
  {
    path: '/plugins',
    name: 'plugins',
    component: () => import('@/views/plugins.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      breadcrumb: [
        {
          title: 'Plugins',
          disabled: true,
          href: '/plugins',
        },
      ],
    },
  },
  {
    path: '/faq',
    name: 'faq',
    component: () => import('@/views/faq.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      breadcrumb: [
        {
          title: 'FAQ',
          disabled: true,
          href: '/faq',
        },
      ],
    },
  },
  {
    path: '/developer',
    name: 'developer',
    component: () => import('@/views/developer.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      roles: ['developer', 'root'],
      breadcrumb: [
        {
          title: 'Developer Guide',
          disabled: true,
          href: '/developer',
        },
      ],
    },
  },
  {
    path: '/chat',
    name: 'chat',
    component: () => import('@/views/chat/ChatBots.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      breadcrumb: [
        {
          title: 'Chat',
          disabled: true,
          href: '/chat',
        },
      ],
    },
  },
  {
    path: '/newDashboards',
    name: 'newDashboards',
    component: () => import('@/views/newDashboards.vue'),
    meta: {
      layout: 'default', // specify the layout for the login page
      requiresAuth: true, // specify the route requires authentication
      breadcrumb: [
        {
          title: 'newDashboards',
          disabled: true,
          href: '/newDashboards',
        },
      ],
    },
  },
  {
    path: '/load_json',
    name: 'load_json',
    component: () => import('@/views/load_json.vue'),
    meta: {
      layout: 'default', // specify the layout for the login page
      requiresAuth: true, // specify the route requires authentication
      breadcrumb: [
        {
          title: 'load_json',
          disabled: true,
          href: '/load_json',
        },
      ],
    },
  },
]


