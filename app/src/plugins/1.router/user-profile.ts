// userProfile is the name that is used in the import { userProfile } from ../settings.ts
export const userProfile = [
  {
    path: '/user/setting',
    name: 'user-setting',
    component: () => import('@/views/user/setting/Setting.vue'),
    meta: {
      requiresAuth: true, // Indica que la ruta requiere autenticación
      roles: [], // Mantengo la misma configuración de roles vacíos
      pageTitle: 'User Setting',
      breadcrumb: [
        {
          title: 'Home',
          disabled: false,
          href: '/',
        },
        {
          title: 'User Settings',
          disabled: true,
          href: '/user/setting',
        },
      ],
    },
  },
];
