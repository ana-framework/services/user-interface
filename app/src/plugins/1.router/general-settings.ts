// settings is the name that is used in the import { settings } from ../settings.ts
export const settings = [
  {
    path: '/settings',
    name: 'settings-page',
    component: () => import('@/views/settings/SettingsPage.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      roles: ['admin', 'root'],
      breadcrumb: [
        {
          title: 'Settings',
          disabled: true,
          href: '/settings',
        },
      ],
    },
  },
  {
    path: '/settings/ldap-ad-configuration',
    name: 'settings-ldap-ad-configuration',
    component: () => import('@/views/settings/LdapAdConfiguration.vue'),

    // props: {url},
    meta: {
      requiresAuth: true, // specify the route requires authentication
      roles: ['admin', 'root'],
      pageTitle: 'LDAP AD Configuration',
      breadcrumb: [
        {
          title: 'Settings',
          disabled: false,
          href: '/settings',
        },
        {
          title: 'LDAP AD Configuration',
          disabled: true,
          href: '/settings/ldap-ad-configuration',
        },
      ],
    },
  },
  {
    path: '/settings/smtp-configuration',
    name: 'settings-smtp-configuration',
    component: () => import('@/views/settings/SmtpConfiguration.vue'),

    // props: {url},
    meta: {
      requiresAuth: true, // specify the route requires authentication
      roles: ['admin', 'root'],
      pageTitle: 'SMTP Configuration',
      breadcrumb: [
        {
          title: 'Settings',
          disabled: false,
          href: '/settings',
        },
        {
          title: 'SMTP Configuration',
          disabled: true,
          href: '/settings/smtp-configuration',
        },
      ],
    },
  },
  {
    path: '/settings/users-list',
    name: 'settings-users-list',
    component: () => import('@/views/settings/UserList.vue'),

    // props: {url},
    meta: {
      requiresAuth: true, // specify the route requires authentication
      roles: ['admin', 'root'],
      pageTitle: 'Users',
      breadcrumb: [
        {
          title: 'Settings',
          disabled: false,
          href: '/settings',
        },
        {
          title: 'Users',
          disabled: true,
          href: '/settings/users-list',
        },
      ],

    },
  },
  {
    path: '/settings/events-list',
    name: 'settings-events-list',
    component: () => import('@/views/settings/EventList.vue'),

    // props: {url},
    meta: {
      requiresAuth: true, // specify the route requires authentication
      roles: ['admin', 'root'],
      pageTitle: 'Events',
      breadcrumb: [
        {
          title: 'Settings',
          disabled: false,
          href: '/settings',
        },
        {
          title: 'Events',
          disabled: true,
          href: '/settings/events-list',
        },
      ],
    },
  },
  {
    path: '/settings/event-new',
    name: 'settings-event-new',
    component: () => import('@/views/settings/EventNew.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      roles: ['admin', 'root'],
      pageTitle: 'New event',
      breadcrumb: [
        {
          title: 'Settings',
          disabled: false,
          href: '/settings',
        },
        {
          title: 'Events',
          disabled: false,
          href: '/settings/events-list',
        },
        {
          title: 'New event',
          disabled: true,
          href: '/settings/event-new',
        },
      ],
    },
  },
  {
    path: '/settings/subscribers-list',
    name: 'settings-subscribers-list',
    component: () => import('@/views/settings/SubscribersList.vue'),

    // props: {url},
    meta: {
      requiresAuth: true, // specify the route requires authentication
      roles: ['admin', 'root'],
      pageTitle: 'Subscribers',
      breadcrumb: [
        {
          title: 'Settings',
          disabled: false,
          href: '/settings',
        },
        {
          title: 'Subscribers',
          disabled: true,
          href: '/settings/subscribers-list',
        },
      ],
    },
  },
  {
    path: '/settings/user-edit/:id',
    name: 'settings-user-edit',
    component: () => import('@/views/settings/UserEdit.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      roles: ['admin', 'root'],
      pageTitle: 'Edit user',
      breadcrumb: [
        {
          title: 'Settings',
          disabled: false,
          href: '/settings',
        },
        {
          title: 'Users',
          disabled: false,
          href: '/settings/users-list',
        },
        {
          title: ':id',
          disabled: true,
          href: '/settings/user-edit/:id',
        },
      ],
    },
  },
  {
    path: '/settings/user-new',
    name: 'settings-user-new',
    component: () => import('@/views/settings/UserNew.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      roles: ['admin', 'root'],
      pageTitle: 'New user',
      breadcrumb: [
        {
          title: 'Settings',
          disabled: false,
          href: '/settings',
        },
        {
          title: 'Users',
          disabled: false,
          href: '/settings/users-list',
        },
        {
          title: 'New user',
          disabled: true,
          href: '/settings/user-new',
        },
      ],
    },
  },
  {
    path: '/settings/subscriber-new',
    name: 'settings-subscriber-new',
    component: () => import('@/views/settings/SubscriberNew.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      roles: ['admin', 'root'],
      pageTitle: 'New subscriber',
      breadcrumb: [
        {
          text: 'Settings',
          to: { name: 'settings-page' },
        },
        {
          text: 'Subscribers',
          to: { name: 'settings-subscribers-list' },
        },
      ],
    },
  },
  {
    path: '/settings/subscriber-edit/:id',
    name: 'settings-subscriber-edit',
    component: () => import('@/views/settings/SubscriberEdit.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      roles: ['admin', 'root'],
      pageTitle: 'Edit subscriber',
      breadcrumb: [
        {
          text: 'Settings',
          to: { name: 'settings-page' },
        },
        {
          text: 'Subscribers',
          to: { name: 'settings-subscribers-list' },
        },
      ],
    },
  },
  {
    path: '/settings/application-configuration',
    name: 'application-configuration',
    component: () => import('@/views/settings/ApplicationConfiguration.vue'),
    meta: {
      requiresAuth: true, // specify the route requires authentication
      roles: ['admin', 'root'],
      pageTitle: 'Application Configuration',
      breadcrumb: [
        {
          text: 'Settings',
          to: { name: 'settings-page' },
        },
        {
          text: 'Application',
          to: { name: 'application-configuration' },
        },
      ],
    },
  },
  {
    path: '/settings/dashboards_setting',
    name: 'dashboards_setting',
    component: () => import('@/views/dashboards_setting.vue'),
    meta: {
      layout: 'default', // specify the layout for the login page
      requiresAuth: true, // specify the route requires authentication
      breadcrumb: [
        {
          title: 'Settings',
          disabled: false,
          href: '/settings',
        },
        {
          title: 'Dashboards setting',
          disabled: true,
          href: '/dashboards_setting',
        },
      ],
    },
  },

]
