import type { ThemeDefinition } from 'vuetify'

export const staticPrimaryColor = '#0473bc'

export const themes: Record<string, ThemeDefinition> = {
  light: {
    dark: false,
    colors: {
      'primary': staticPrimaryColor, // Blue
      'on-primary': '#fff',
      'secondary': '#A8AAAE',
      'on-secondary': '#fff',
      'success': '#4CAF50', // Green
      'on-success': '#fff',
      'error': '#F44336', // Red
      'danger': '#F44336', // Red
      'on-error': '#fff',
      'warning': '#FFC107', // Yellow
      'on-warning': '#fff',
      'info': '#03A9F4', // Light Blue
      'on-info': '#fff',
      'amber': '#FFC107', // Amber
      'cyan': '#00BCD4', // Cyan
      'yellow': '#FFEB3B', // Yellow
      'purple': '#9C27B0', // Purple
      'grey-100': '#F5F5F5', // Grey (Light)
      'grey-300': '#E0E0E0', // Light Grey
      'grey-400': '#BDBDBD',
      'grey-500': '#9E9E9E',
      'grey-600': '#757575',
      'grey-700': '#616161',
      'grey-800': '#424242',
      'grey-900': '#212121',
      'background': '#F8F7FA',
      'on-background': '#2F2B3D',
      'on-surface': '#2F2B3D',
      'perfect-scrollbar-thumb': '#DBDADE',
      'skin-bordered-background': '#fff',
      'skin-bordered-surface': '#fff',
    },

    variables: {
      'code-color': '#d400ff',
      'overlay-scrim-background': '#4C4E64',
      'tooltip-background': '#4A5072',
      'overlay-scrim-opacity': 0.5,
      'hover-opacity': 0.04,
      'focus-opacity': 0.12,
      'selected-opacity': 0.06,
      'activated-opacity': 0.16,
      'pressed-opacity': 0.14,
      'dragged-opacity': 0.1,
      'disabled-opacity': 0.42,
      'border-color': '#2F2B3D',
      'border-opacity': 0.16,
      'high-emphasis-opacity': 0.78,
      'medium-emphasis-opacity': 0.68,
      'switch-opacity': 0.2,
      'switch-disabled-track-opacity': 0.3,
      'switch-disabled-thumb-opacity': 0.4,
      'switch-checked-disabled-opacity': 0.3,

      // Shadows
      'shadow-key-umbra-color': '#2F2B3D',
    },
  },
  dark: {
    dark: true,
    colors: {
      'primary': staticPrimaryColor, // Blue
      'on-primary': '#fff',
      'secondary': '#A8AAAE',
      'on-secondary': '#fff',
      'success': '#4CAF50', // Green
      'on-success': '#fff',
      'error': '#F44336', // Red
      'danger': '#F44336', // Red
      'on-error': '#fff',
      'warning': '#FFC107', // Yellow
      'on-warning': '#fff',
      'info': '#03A9F4', // Light Blue
      'on-info': '#fff',
      'amber': '#FFC107', // Amber
      'cyan': '#00BCD4', // Cyan
      'yellow': '#FFEB3B', // Yellow
      'purple': '#9C27B0', // Purple
      'grey-100': '#F5F5F5', // Grey (Light)
      'grey-300': '#E0E0E0', // Light Grey
      'grey-400': '#BDBDBD',
      'grey-500': '#9E9E9E',
      'grey-600': '#757575',
      'grey-700': '#616161',
      'grey-800': '#424242',
      'grey-900': '#212121',
      'background': '#25293C',
      'on-background': '#D0D4F1',
      'surface': '#2F3349',
      'on-surface': '#D0D4F1',
      'perfect-scrollbar-thumb': '#4A5072',
      'skin-bordered-background': '#2f3349',
      'skin-bordered-surface': '#2f3349',
    },
    variables: {
      'code-color': '#d400ff',
      'overlay-scrim-background': '#101121',
      'tooltip-background': '#5E6692',
      'overlay-scrim-opacity': 0.6,
      'hover-opacity': 0.04,
      'focus-opacity': 0.12,
      'selected-opacity': 0.06,
      'activated-opacity': 0.16,
      'pressed-opacity': 0.14,
      'dragged-opacity': 0.1,
      'disabled-opacity': 0.42,
      'border-color': '#D0D4F1',
      'border-opacity': 0.16,
      'high-emphasis-opacity': 0.78,
      'medium-emphasis-opacity': 0.68,
      'switch-opacity': 0.4,
      'switch-disabled-track-opacity': 0.4,
      'switch-disabled-thumb-opacity': 0.8,
      'switch-checked-disabled-opacity': 0.3,

      // Shadows
      'shadow-key-umbra-color': '#0F1422',
    },
  },
}

export default themes
