export const required = value => !!value || 'Required.'
export const password = value => value.length >= 8 || 'Password must be at least 8 characters.'
export const confirmed = (value, target) => value === target || 'Passwords do not match.'
