export default [
  { heading: 'Project AI' },
  {
    title: 'Project AI',
    icon: { icon: 'tabler-layout-grid' },
    to: 'projectAI',
  },
]
