export default [
  { heading: 'HELP' },

  {
    title: 'FAQ',
    icon: { icon: 'tabler-help-octagon' },
    to: 'faq',
  },
  {
    title: 'Raise Support',
    icon: { icon: 'tabler-chart-donut-4' },
    to: null,
  },

]
