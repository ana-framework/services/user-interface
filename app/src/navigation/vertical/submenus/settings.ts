export default [
    { heading: 'Settings' },
        {
          title: 'Dashboards Setting',
          to: 'dashboards_setting',
          icon: { icon: 'tabler-device-laptop' },
          badgeContent: 'NEW',
          badgeClass: 'bg-global-primary',
        },
  ]
  