export default [
  { heading: 'RESOURCES' },
  {
    title: 'Inventory',
    icon: { icon: 'tabler-package' },
    children: [
      {
        title: 'Integrations',
        to: 'integrations',
        icon: { icon: 'tabler-toggle-right', size: '10' },
      },
      {
        title: 'Infrastructure',
        to: 'infrastructure',
        icon: { icon: 'tabler-shield', size: '10' },
      },

    ],
    badgeContent: 'BETA',
    badgeClass: 'bg-global-primary',
  },
  {
    title: 'Dev Tools',
    icon: { icon: 'tabler-device-desktop' },
    children: [
      {
        title: 'Templates',
        to: 'templates',
        icon: { icon: 'tabler-settings', size: '10' },
      },
      {
        title: 'Plugins',
        to: 'plugins',
        icon: { icon: 'tabler-loader', size: '10' },
      },
      {
        title: 'Flow Examples',
        to: 'flows',
        icon: { icon: 'tabler-list', size: '10' },
      },
      {
        title: 'Developer Guide',
        to: 'developer',
        icon: { icon: 'tabler-file-description', size: '10' },
      },

    ],
    badgeContent: 'BETA',
    badgeClass: 'bg-global-primary',
  },
]
