export default [
    { heading: 'Dashboard' },
        {
          title: 'New Dashboard',
          to: 'newDashboards',
          icon: { icon: 'tabler-list', size: '10' },
          badgeContent: 'NEW',
          badgeClass: 'bg-global-primary',
        },

  ]
  