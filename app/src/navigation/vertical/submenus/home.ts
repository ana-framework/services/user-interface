export default [
  {
    title: 'Home',
    icon: { icon: 'tabler-home' },
    to: 'home',
  },
  {
    title: 'Admin',
    icon: { icon: 'tabler-tool' },
    to: 'settings-page',
    meta: {
      showFor: ['root', 'admin'],
    },
  },
  
]


