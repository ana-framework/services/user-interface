export default [
  { heading: 'AUTOMATION' },
  {
    title: 'Vendors',
    icon: { icon: 'tabler-layout-grid' },
    to: 'vendors',
  },
  {
    title: 'Developers',
    icon: { icon: 'tabler-layout-grid' },
    to: 'developer',
    meta: {
      showFor: ['root', 'developer']
    }
  },
  {
    title: 'Search Pipeline',
    icon: { icon: 'tabler-search' },
    to: 'search-pipeline',
  },
  {
    title: 'Dashboards',
    icon: { icon: 'tabler-device-laptop' },
    to: 'dashboards',
  },
  {
    title: 'Scheduler',
    icon: { icon: 'tabler-square-check' },
    to: 'scheduler',
    badgeContent: 'BETA',
    badgeClass: 'bg-global-primary',
  },
]


