export default [
  { heading: 'ANA AI' },
  {
    title: 'ANA Intelligence',
    icon: { icon: 'tabler-square-check' },
    to: { name: 'chat' },
    badgeContent: 'BETA',
    badgeClass: 'bg-global-primary',
  },
]
