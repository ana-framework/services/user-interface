export default [
  { heading: 'EXTERNAL LINKS' },

  {
    title: 'External links',
    icon: { icon: 'tabler-external-link' },
    children: [
      {
        title: 'GitLab',
        href: 'https://gitlab.demo.vmware.5glabaltran.com/',
        target: '_blank',
        icon: { icon: 'tabler-circle', size: '10' },
      },
      {
        title: 'Kibana',
        href: null,
        target: '_blank',
        icon: { icon: 'tabler-circle', size: '10' },
      },
      {
        title: 'Long Horn',
        href: null,
        target: '_blank',
        icon: { icon: 'tabler-circle', size: '10' },
      },
      {
        title: 'Nexus',
        href: null,
        target: '_blank',
        icon: { icon: 'tabler-circle', size: '10' },
      },
      {
        title: 'Grafana',
        href: null,
        target: '_blank',
        icon: { icon: 'tabler-circle', size: '10' },
      },
      {
        title: 'Minio',
        href: null,
        target: '_blank',
        icon: { icon: 'tabler-circle' },
      },

    ],
  },
]
