
import type { VerticalNavItems } from '@layouts/types';
import { ref, watchEffect } from 'vue';
import anaAI from './submenus/anaAI';
import automation from './submenus/automation';
import external_links from './submenus/external_links';
import home from './submenus/home';

const userRole = ref<string>('');

// 🔹 Función para obtener el rol del usuario desde `sessionStorage`
const getUserRole = () => {
  const userRaw = sessionStorage.getItem('user');
  const user = userRaw ? JSON.parse(userRaw) : {};
  return user.role?.toLowerCase() || '';
};

// 🔹 Actualizar el rol cuando haya cambios en `sessionStorage`
watchEffect(() => {
  userRole.value = getUserRole();
});



// 🔹 Filtrar los botones según el rol
const filterMenuByRole = (menu: any[]) => {
  return menu.filter(item => !item.meta?.showFor || item.meta.showFor.includes(userRole.value));
};


// 🔹 Exportar los menús filtrados
export default [
  ...filterMenuByRole(home),
  ...filterMenuByRole(automation),
  ...filterMenuByRole(anaAI),
  ...filterMenuByRole(external_links),
] as VerticalNavItems;

