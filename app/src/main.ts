// src/main.ts
import { createApp } from 'vue'
import App from '@/App.vue'
import { registerPlugins } from '@core/utils/plugins'
import store from '@/store' // import the store
// Styles
import '@core/scss/template/index.scss'
import '@styles/styles.scss'

// Create vue app
const app = createApp(App)

// Use the store

app.use(store)

// Register plugins
registerPlugins(app)

// Mount vue app
app.mount('#app')
