export interface TableRow {
  Secuencia: string
  Jalon: string
  TRAT_ESP: string
  SAF: string
  Estado: string
  Red: string
  Prov: string
  F_Presen: string
  Sev: string
  Equipo: string
  E_Act: string
  Raiz: string
  E_Raiz: string
  Resp_Raiz: string
  ID2: string
  TIPO_SINTOMA: string
  Sintoma: string
  Responsable: string
  Cad_Delegacion: string
}

export const tableData: TableRow[] = [
  {
    Secuencia: 'INC-0028133889',
    Jalon: '',
    TRAT_ESP: 'Nulo',
    SAF: 'NO SOLICITADO',
    Estado: 'ACTIVO',
    Red: 'Red Fusión',
    Prov: 'Madrid',
    F_Presen: '05/06/2024 15:36',
    Sev: '3',
    Equipo: 'hl4fncr5-101',
    E_Act: '',
    Raiz: '',
    E_Raiz: '',
    Resp_Raiz: '',
    ID2: 'Link-Down',
    TIPO_SINTOMA: 'ENLACE CAIDO',
    Sintoma: 'hl4fncr5-101:SNMP_Traps_RIMA:Link_Sondeo:10/1/5\n'
            + 'hl4fncr5-101 /// FUSION&HL4 /// TIMOS\n'
            + 'IF 10/1/5 down in hostname: hl4fncr5-101 Alejandra-CANG(67097962120)\n'
            + 'USUARIO_FINAL_GRI: OMRSCOIPTR-01\n'
            + 'FRU:  PART NUMBER:  LMATERIAL: ---\n'
            + 'Historico de incidencias abiertas por OMEGA\n'
            + 'No hay incidencias relacionadas',
    Responsable: 'OMRSCOIPTR-01',
    Cad_Delegacion: 'OMEGA,OMRSCOIPTR-04, OMRSSOTX-OMEGA, OMRSCOIPTR-01',
  },
  {
    Secuencia: 'INC-0028124247',
    Jalon: '',
    TRAT_ESP: 'Nulo',
    SAF: 'NO SOLICITADO',
    Estado: 'ACTIVO',
    Red: 'Red Fusión',
    Prov: 'Palencia',
    F_Presen: '04/06/2024 15:30',
    Sev: '3',
    Equipo: 'hl4plsj1-302',
    E_Act: 'Cerrada',
    Raiz: 'INC-0028135417',
    E_Raiz: 'ACTIVO',
    Resp_Raiz: 'CNSI_ASTRO',
    ID2: 'Link-Down',
    TIPO_SINTOMA: 'ENLACE CAIDO',
    Sintoma: 'hl4plsj1-302:DAEMON-LFMD_3AH_LINK:xe-5/2/0\n'
            + 'hl4plsj1-302 /// FUSION&HL4 /// JUNOS\n'
            + 'DAEMON-5-LFMD_3AH_LINKDOWN: (xe-5/2/0): 802.3ah link-fault status changed to fault with reason [Local link fault]\n'
            + 'Equipo_extremo: hl5from1-201\n'
            + 'Puerto_extremo: GigabitEthernet0/1/0\n'
            + 'USUARIO_FINAL_GRI: OMRSCOIPTR-01\n'
            + 'FRU: SFP+-10G-ER PART NUMBER: 740-047680 LMATERIAL: ---\n'
            + 'Historico de incidencias abiertas por OMEGA\n'
            + 'No hay incidencias relacionadas',
    Responsable: 'OMRSCOIPTR-01',
    Cad_Delegacion: 'OMEGA,OMRSCOIPTR-04, OMRSSOTX-OMEGA, OMRSCOIPTR-01',
  },
  {
    Secuencia: 'INC-0028118016',
    Jalon: '99',
    TRAT_ESP: 'Nulo',
    SAF: 'NO SOLICITADO',
    Estado: 'ACTIVO',
    Red: 'Red Fusión',
    Prov: 'Castellon',
    F_Presen: '04/06/2024 2:59',
    Sev: '4',
    Equipo: 'hl4vnrsn1-301',
    E_Act: 'Cerrada',
    Raiz: 'INC-0028122997',
    E_Raiz: 'ACTIVO',
    Resp_Raiz: 'CNSI_ASTRO',
    ID2: 'Link-Down',
    TIPO_SINTOMA: 'ENLACE CAIDO',
    Sintoma: 'hl4vnrsn1-301:DAEMON-LFMD_3AH_LINK:ge-11/1/2\n'
            + 'hl4vnrsn1-301 /// FUSION&HL4 /// JUNOS\n'
            + 'DAEMON-5-LFMD_3AH_LINKDOWN: (ge-11/1/2): 802.3ah link-fault status changed to fault with reason [Local link fault]\n'
            + 'Raiz encontrada: BTP-0003181491\n'
            + 'Equipo_extremo: emmcati1-202\n'
            + 'Puerto_extremo: GigabitEthernet0/3/0\n'
            + 'USUARIO_FINAL_GRI: OMRSCOIPTR-01\n'
            + 'FRU:  PART NUMBER:  LMATERIAL: \n'
            + 'BTP-0003181491---\n'
            + 'Historico de incidencias abiertas por OMEGA\n'
            + 'No hay incidencias relacionadas',
    Responsable: 'OMRSCOIPTR-01',
    Cad_Delegacion: 'OMEGA,OMRSCOIPTR-04, OMRSSOTX-OMEGA, OMRSCOIPTR-01',
  },
  {
    Secuencia: 'INC-0028107920',
    Jalon: '',
    TRAT_ESP: 'Nulo',
    SAF: 'NO SOLICITADO',
    Estado: 'ACTIVO',
    Red: 'Red Fusión',
    Prov: 'Madrid',
    F_Presen: '02/06/2024 18:46',
    Sev: '3',
    Equipo: 'emmsresr2-102',
    E_Act: 'Cerrada',
    Raiz: 'INC-0028101823',
    E_Raiz: 'ACTIVO',
    Resp_Raiz: 'OMRSENBI',
    ID2: 'Link-Down',
    TIPO_SINTOMA: 'ENLACE CAIDO',
    Sintoma: 'emmsresr2-102:SNMP-WARNING-linkDown:1/6/2\n'
            + 'emmsresr2-102 /// FUSION&HL5 /// TIMOS\n'
            + 'TMNX: 120822 Base SNMP-WARNING-linkDown-2004 [1/6/2]: Interface 1/6/2 is not operational\n'
            + 'Equipo_extremo: manzanaresreal-sw\n'
            + 'Puerto_extremo: FastEthernet0/21\n'
            + 'USUARIO_FINAL_GRI: OMRSCOIPTR-01\n'
            + 'FRU:  PART NUMBER:  LMATERIAL: \n'
            + '---\n'
            + 'Historico de incidencias abiertas por OMEGA\n'
            + '\n'
            + '0028107097 con fecha 02/06/2024 11:42:15 en estado CERRADO\n'
            + 'FECHA REPARACION  : 02/06/2024 11:34:56\n'
            + 'Planta            : RN,RED,REDSER\n'
            + 'Causa             : RNDE,SC0100\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : Franqueo automatico por cese de alarma\n'
            + '\n'
            + '0028106734 con fecha 02/06/2024 08:28:02 en estado CERRADO\n'
            + 'FECHA REPARACION  : 02/06/2024 08:42:27\n'
            + 'Planta            : OP,OPRP,OPRP\n'
            + 'Causa             : OPRPOT,OPOPOU\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : SIN_TRAZADO_TX\n'
            + '\n'
            + '0028105966 con fecha 01/06/2024 22:58:24 en estado CERRADO\n'
            + 'FECHA REPARACION  : 01/06/2024 23:17:24\n'
            + 'Planta            : OP,OPRP,OPRP\n'
            + 'Causa             : OPRPOT,OPOPOU\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : SIN_TRAZADO_TX\n'
            + '\n'
            + '0028105694 con fecha 01/06/2024 19:27:58 en estado CERRADO\n'
            + 'FECHA REPARACION  : 01/06/2024 19:42:23\n'
            + 'Planta            : OP,OPRP,OPRP\n'
            + 'Causa             : OPRPOT,OPOPOU\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : SIN_TRAZADO_TX\n'
            + '\n'
            + '0028105260 con fecha 01/06/2024 16:01:53 en estado CERRADO\n'
            + 'FECHA REPARACION  : 01/06/2024 16:16:06\n'
            + 'Planta            : OP,OPRP,OPRP\n'
            + 'Causa             : OPRPOT,OPOPOU\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : SIN_TRAZADO_TX\n'
            + '\n'
            + '0028104862 con fecha 01/06/2024 13:09:58 en estado CERRADO\n'
            + 'FECHA REPARACION  : 01/06/2024 14:54:57\n'
            + 'Planta            : MS,OMEG,OMEGA\n'
            + 'Causa             : OTRAMS,\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : \n'
            + '\n'
            + '0028104319 con fecha 01/06/2024 10:24:00 en estado CERRADO\n'
            + 'FECHA REPARACION  : 01/06/2024 12:09:44\n'
            + 'Planta            : MS,OMEG,OMEGA\n'
            + 'Causa             : OTRAMS,\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : \n'
            + '\n'
            + '0028103850 con fecha 01/06/2024 07:41:05 en estado CERRADO\n'
            + 'FECHA REPARACION  : 01/06/2024 08:55:46\n'
            + 'Planta            : MS,OMEG,OMEGA\n'
            + 'Causa             : OTRAMS,\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : \n'
            + '\n'
            + '0028103462 con fecha 01/06/2024 04:53:15 en estado CERRADO\n'
            + 'FECHA REPARACION  : 01/06/2024 06:07:29\n'
            + 'Planta            : MS,OMEG,OMEGA\n'
            + 'Causa             : OTRAMS,\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : \n'
            + '\n'
            + '0028103228 con fecha 01/06/2024 02:03:08 en estado CERRADO\n'
            + 'FECHA REPARACION  : 01/06/2024 03:17:51\n'
            + 'Planta            : MS,OMEG,OMEGA\n'
            + 'Causa             : OTRAMS,\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : \n'
            + '\n'
            + '...\n'
            + 'Fin historico de las ultimas incidencias abiertas en OMEGA\n',
    Responsable: 'OMRSCOIPTR-01',
    Cad_Delegacion: 'OMEGA,OMRSCOIPTR-04, OMRSSOTX-OMEGA, OMRSCOIPTR-01',
  },
  {
    Secuencia: 'INC-0028097991',
    Jalon: '',
    TRAT_ESP: 'ELEMENTO ROJO',
    SAF: 'NO SOLICITADO',
    Estado: 'ACTIVO',
    Red: 'Red Fusión',
    Prov: 'Las Palmas',
    F_Presen: '31/05/2024 8:53',
    Sev: '4',
    Equipo: 'emtlpsc2-103',
    E_Act: 'Retenida',
    Raiz: '',
    E_Raiz: '',
    Resp_Raiz: '',
    ID2: 'Link-Down',
    TIPO_SINTOMA: 'ENLACE CAIDO',
    Sintoma: 'emtlpsc2-103:SNMP-WARNING-linkDown:4/1/2\n'
            + 'emtlpsc2-103 /// FUSION&HL4 /// TIMOS\n'
            + 'emtlpsc2-103: 263397 Base SNMP-WARNING-linkDown-2004 [4/1/2]: Interface 4/1/2 is not operational\n'
            + 'Equipo_extremo: hl4lpta2-101\n'
            + 'Puerto_extremo: 9/1/10\n'
            + 'USUARIO_FINAL_GRI: OMRSCOIPTR-01\n'
            + 'FRU:  PART NUMBER:  LMATERIAL: \n'
            + '---\n'
            + 'Historico de incidencias abiertas por OMEGA\n'
            + '\n'
            + '0028096795 con fecha 31/05/2024 03:14:58 en estado CERRADO\n'
            + 'FECHA REPARACION  : 31/05/2024 03:29:08\n'
            + 'Planta            : MS,OMEG,OMEGA\n'
            + 'Causa             : OTRAMS,\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : \n'
            + '\n'
            + '0028095888 con fecha 30/05/2024 22:43:57 en estado CERRADO\n'
            + 'FECHA REPARACION  : 30/05/2024 23:59:20\n'
            + 'Planta            : MS,OMEG,OMEGA\n'
            + 'Causa             : OTRAMS,\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : \n'
            + '\n'
            + '0028095319 con fecha 30/05/2024 19:27:23 en estado CERRADO\n'
            + 'FECHA REPARACION  : 30/05/2024 19:41:06\n'
            + 'Planta            : OP,OPRP,OPRP\n'
            + 'Causa             : OPRPOT,OPOPOU\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : ENLACE_ENTRE_CENTRALES_SIN_TX\n'
            + '\n'
            + 'TOTAL: 3\n'
            + 'Fin historico de las ultimas incidencias abiertas en OMEGA\n',
    Responsable: 'OMRSCOIPTR-01',
    Cad_Delegacion: 'OMEGA,OMRSCOIPTR-04, OMRSSOTX-OMEGA, OMRSCOIPTR-01',
  },
  {
    Secuencia: 'INC-0028097319',
    Jalon: '',
    TRAT_ESP: 'Nulo',
    SAF: 'NO SOLICITADO',
    Estado: 'ACTIVO',
    Red: 'Red Fusión',
    Prov: 'Barcelona',
    F_Presen: '31/05/2024 6:04',
    Sev: '3',
    Equipo: 'hl3bep1-301',
    E_Act: 'Retenida',
    Raiz: '',
    E_Raiz: '',
    Resp_Raiz: '',
    ID2: 'Link-Down',
    TIPO_SINTOMA: 'ENLACE CAIDO',
    Sintoma: 'hl3bep1-301:DAEMON-LFMD_3AH_LINK:et-5/0/0\n'
            + 'hl3bep1-301 /// FUSION&HL3 /// JUNOS\n'
            + 'DAEMON-5-LFMD_3AH_LINKDOWN: (et-5/0/0): 802.3ah link-fault status changed to fault with reason [Local link fault]\n'
            + 'Equipo_extremo: hl3btb1-301\n'
            + 'Puerto_extremo: et-5/0/0\n'
            + 'USUARIO_FINAL_GRI: OMRSCOIPTR-01\n'
            + 'FRU:  PART NUMBER:  LMATERIAL: \n'
            + '---\n'
            + 'Historico de incidencias abiertas por OMEGA\n'
            + '\n'
            + '0028015708 con fecha 20/05/2024 14:14:55 en estado CERRADO\n'
            + 'FECHA REPARACION  : 22/05/2024 19:10:38\n'
            + 'Planta            : MS,OMEG,OMEGA\n'
            + 'Causa             : OTRAMS,\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : \n'
            + '\n'
            + '0027954048 con fecha 09/05/2024 23:25:42 en estado CERRADO\n'
            + 'FECHA REPARACION  : 14/05/2024 12:50:33\n'
            + 'Planta            : TX,TXML,TXMLU7\n'
            + 'Causa             : OTRATX,\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : Franqueo automatico por cese de alarma\n'
            + '\n'
            + 'TOTAL: 2\n'
            + 'Fin historico de las ultimas incidencias abiertas en OMEGA\n',
    Responsable: 'OMRSCOIPTR-01',
    Cad_Delegacion: 'OMEGA,OMRSCOIPTR-04, OMRSSOTX-OMEGA, OMRSCOIPTR-01',
  },
  {
    Secuencia: 'INC-0028060636',
    Jalon: '',
    TRAT_ESP: 'Nulo',
    SAF: 'ERROR',
    Estado: 'ACTIVO',
    Red: 'ORANGE',
    Prov: 'Tarragona',
    F_Presen: '27/05/2024 2:02',
    Sev: '3',
    Equipo: 'OLTZ4321002TAR022',
    E_Act: '',
    Raiz: '',
    E_Raiz: '',
    Resp_Raiz: '',
    ID2: 'Link-Down',
    TIPO_SINTOMA: 'FTTH_MAS_OSP_CIRCUITO_ICX',
    Sintoma: 'Conexion con PAI de ORANGE J4321002OLT05PPAI71-62200000000709\n'
            + 'hl4slou1-101:SNMP-WARNING-linkDown:8/1/2\n'
            + 'hl4slou1-101 /// FUSION&HL4 /// TIMOS\n'
            + 'hl4slou1-101: 2427551 Base SNMP-WARNING-linkDown-2004 [8/1/2]: Interface 8/1/2 is not operational\n'
            + 'Equipo_extremo: OLTZ4321002TAR022\n'
            + 'Puerto_extremo: 62200000000709\n'
            + 'MAP_CATEGORIZACION: NOR;GUAORG\n'
            + 'USUARIO_FINAL_GRI: OMRSCOIPTR-01\n'
            + 'FRU:  PART NUMBER:  LMATERIAL: ',
    Responsable: 'OMRSCOIPTR-01',
    Cad_Delegacion: 'OMEGA,OMRSCOIPTR-01, OMRSSOTX-OMEGA, ORANGE_IF, ORANGE_IF',
  },
  {
    Secuencia: 'INC-0028044932',
    Jalon: '',
    TRAT_ESP: 'Nulo',
    SAF: 'NO SOLICITADO',
    Estado: 'ACTIVO',
    Red: 'Red Fusión',
    Prov: 'Madrid',
    F_Presen: '24/05/2024 1:17',
    Sev: '3',
    Equipo: 'hl4galga1-101',
    E_Act: 'Cerrada',
    Raiz: 'INC-0028052632',
    E_Raiz: 'CERRADO',
    Resp_Raiz: 'CNSI_ASTRO',
    ID2: 'Link-Down',
    TIPO_SINTOMA: 'ENLACE CAIDO',
    Sintoma: 'hl4galga1-101:SNMP-WARNING-linkDown:10/2/8\n'
            + 'hl4galga1-101 /// FUSION&HL4 /// TIMOS\n'
            + 'hl4galga1-101: 1643956 Base SNMP-WARNING-linkDown-2004 [10/2/8]: Interface 10/2/8 is not operational\n'
            + 'Equipo_extremo: hl3mno1-101\n'
            + 'Puerto_extremo: 1/2/23\n'
            + 'USUARIO_FINAL_GRI: OMRSCOIPTR-01\n'
            + 'FRU: 10GBASE-LR PART NUMBER: SPP5300LR-A8 LMATERIAL: No_encontrado\n'
            + 'ENLACES DE MISMO GRUPO DE REDUNDANCIA CORTADOS\n'
            + 'hl4galga1-101   10/2/10\n'
            + '---\n'
            + 'Historico de incidencias abiertas por OMEGA\n'
            + 'No hay incidencias relacionadas',
    Responsable: 'OMRSCOIPTR-01',
    Cad_Delegacion: 'OMEGA,OMRSCOIPTR-04, OMRSSOTX-OMEGA, OMRSCOIPTR-01, CNSI, OMRSSUTX-JDS, OMRSSOTX-OMEGA',
  },
  {
    Secuencia: 'INC-0028017004',
    Jalon: '',
    TRAT_ESP: 'Nulo',
    SAF: 'NO SOLICITADO',
    Estado: 'ACTIVO',
    Red: 'Red Fusión',
    Prov: 'Lerida',
    F_Presen: '20/05/2024 17:30',
    Sev: '3',
    Equipo: 'hl3lbo1-101',
    E_Act: 'Cerrada',
    Raiz: 'INC-0028134565',
    E_Raiz: 'CERRADO',
    Resp_Raiz: 'OMRSRTCNT-AN-OMEGA',
    ID2: 'Link-Down',
    TIPO_SINTOMA: 'ENLACE CAIDO',
    Sintoma: 'hl3lbo1-101:SNMP_Traps_RIMA:Link_Sondeo:5/1/c1/1\n'
            + 'hl3lbo1-101 /// FUSION&HL3 /// TIMOS\n'
            + 'IF 5/1/c1/1 down in hostname: hl3lbo1-101 Conexion con hl2btb1-301 11/1/8 @60017031554211@\n'
            + '(Link Down) Se ha perdido el enlace de comunicacion con el elemento de red (Interfaz 5/1/c1/1)\n'
            + 'Raiz encontrada: INC-0028016918\n'
            + 'Equipo_extremo: hl2btb1-301\n'
            + 'Puerto_extremo: et-11/1/8\n'
            + 'USUARIO_FINAL_GRI: OMRSCOIPTR-01\n'
            + 'FRU:  PART NUMBER:  LMATERIAL: \n'
            + '---\n'
            + 'Historico de incidencias abiertas por OMEGA\n'
            + '\n'
            + '0028014771 con fecha 20/05/2024 12:27:53 en estado CERRADO\n'
            + 'FECHA REPARACION  : 20/05/2024 12:58:17\n'
            + 'Planta            : MS,OMEG,OMEGA\n'
            + 'Causa             : OTRAMS,\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : \n'
            + '\n'
            + '0027918118 con fecha 04/05/2024 16:00:46 en estado CERRADO\n'
            + 'FECHA REPARACION  : 06/05/2024 19:35:43\n'
            + 'Planta            : MS,OMEG,OMEGA\n'
            + 'Causa             : OTRAMS,\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : \n'
            + '\n'
            + '0027844526 con fecha 20/04/2024 15:59:21 en estado CERRADO\n'
            + 'FECHA REPARACION  : 21/04/2024 04:37:51\n'
            + 'Planta            : OP,OPRP,OPRP\n'
            + 'Causa             : OPRPOT,OPOPOU\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : SOLUCIONADO_RAIZ\n'
            + '\n'
            + '0027808438 con fecha 16/04/2024 15:16:29 en estado CERRADO\n'
            + 'FECHA REPARACION  : 19/04/2024 06:40:45\n'
            + 'Planta            : OP,OPRP,OPRP\n'
            + 'Causa             : OPRPOT,OPOPOU\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : rimaDiagnosticadorOK\n'
            + '\n'
            + 'TOTAL: 4\n'
            + 'Fin historico de las ultimas incidencias abiertas en OMEGA\n',
    Responsable: 'OMRSCOIPTR-01',
    Cad_Delegacion: 'OMEGA,OMRSCOIPTR-04, OMRSSOTX-OMEGA, OMRSCOIPTR-01, OMRSSOTX-OMEGA, OMRSSUTX-JDS, OMRSSOTX-OMEGA, OMRSSUTX-JDS, OMRSSOTX-OMEGA, OMRSSUTX-JDS, OMRSRTCNT-AN',
  },
  {
    Secuencia: 'INC-0027947631',
    Jalon: '',
    TRAT_ESP: 'Nulo',
    SAF: 'NO SOLICITADO',
    Estado: 'ACTIVO',
    Red: 'Anillo Crítico',
    Prov: 'Valencia',
    F_Presen: '09/05/2024 2:45',
    Sev: '3',
    Equipo: 'araval1-12',
    E_Act: 'Cerrada',
    Raiz: '',
    E_Raiz: '',
    Resp_Raiz: '',
    ID2: 'Link-Down',
    TIPO_SINTOMA: 'ENLACE CAIDO',
    Sintoma: 'araval1-12:PKT_INFRA-LINK-3-UPDOWN:POS0/6/0/1\n'
            + 'araval1-12 /// ANILLO&Router_Acceso /// IOSXR\n'
            + 'PKT_INFRA-LINK-3-UPDOWN : Interface POS0/6/0/1, changed state to Down\n'
            + 'PKT_INFRA-LINEPROTO-5-UPDOWN : Line protocol on Interface POS0/6/0/1, changed state to Down\n'
            + '(Link Down) Se ha perdido el enlace de comunicacion con el elemento de red (Interfaz POS0/6/0/1)\n'
            + 'Equipo_extremo: aravcr1-12\n'
            + 'Puerto_extremo: POS0/6/1/1\n'
            + 'USUARIO_FINAL_GRI: OMRSCOIPTR-01\n'
            + 'FRU: POM-OC48-LR2-LC-C PART NUMBER: POM-OC48-LR2-LC-C LMATERIAL: No_encontrado---\n'
            + 'Historico de incidencias abiertas por OMEGA\n'
            + 'No hay incidencias relacionadas',
    Responsable: 'OMRSCOIPTR-01',
    Cad_Delegacion: 'OMEGA,OMRSCOIPTR-04, OMRSSOTX-OMEGA, OMRSCOIPTR-01',
  },
  {
    Secuencia: 'INC-0027947104',
    Jalon: '',
    TRAT_ESP: 'Nulo',
    SAF: 'NO SOLICITADO',
    Estado: 'ACTIVO',
    Red: 'Anillo Crítico',
    Prov: 'Valencia',
    F_Presen: '09/05/2024 0:54',
    Sev: '3',
    Equipo: 'aravcr1-11',
    E_Act: 'Cerrada',
    Raiz: '',
    E_Raiz: '',
    Resp_Raiz: '',
    ID2: 'Link-Down',
    TIPO_SINTOMA: 'ENLACE CAIDO',
    Sintoma: 'aravcr1-11:PKT_INFRA-LINEPROTO-5-UPDOWN:POS0/6/1/1\n'
            + 'aravcr1-11 /// ANILLO&Router_Acceso /// IOSXR\n'
            + 'PKT_INFRA-LINEPROTO-5-UPDOWN : Line protocol on Interface POS0/6/1/1, changed state to Down\n'
            + 'PKT_INFRA-LINK-3-UPDOWN : Interface POS0/6/1/1, changed state to Down\n'
            + '(Link Down) Se ha perdido el enlace de comunicacion con el elemento de red (Interfaz POS0/6/1/1)\n'
            + 'Equipo_extremo: araval1-11\n'
            + 'Puerto_extremo: POS0/6/0/1\n'
            + 'USUARIO_FINAL_GRI: OMRSCOIPTR-01\n'
            + 'FRU: POM-OC48-LR2-LC PART NUMBER: POM-OC48-LR2-LC LMATERIAL: 00681172---\n'
            + 'Historico de incidencias abiertas por OMEGA\n'
            + 'No hay incidencias relacionadas',
    Responsable: 'OMRSCOIPTR-01',
    Cad_Delegacion: 'OMEGA,OMRSCOIPTR-04, OMRSSOTX-OMEGA, OMRSCOIPTR-01',
  },
  {
    Secuencia: 'INC-0027629201',
    Jalon: '',
    TRAT_ESP: 'Nulo',
    SAF: 'NO SOLICITADO',
    Estado: 'ACTIVO',
    Red: 'Red Fusión',
    Prov: 'Baleares',
    F_Presen: '17/03/2024 21:52',
    Sev: '3',
    Equipo: 'hl3pmcm1-101',
    E_Act: 'Cerrada',
    Raiz: 'INC-0028067152',
    E_Raiz: 'CERRADO',
    Resp_Raiz: 'OMRSRTCNT-AN',
    ID2: 'Link-Down',
    TIPO_SINTOMA: 'ENLACE CAIDO',
    Sintoma: 'hl3pmcm1-101:SNMP-WARNING-linkDown:5/2/c6/1\n'
            + 'hl3pmcm1-101 /// FUSION&HL3 /// TIMOS\n'
            + 'hl3pmcm1-101: 4828740 Base SNMP-WARNING-linkDown-2004 [5/2/c6/1]: Interface 5/2/c6/1 is not operational\n'
            + 'IF 5/2/c6/1 down in hostname: hl3pmcm1-101 Conexion con hl4eiin1-101 5/1/1\n'
            + '(Link Down) Se ha perdido el enlace de comunicacion con el elemento de red (Interfaz 5/2/c6/1)\n'
            + 'Equipo_extremo: hl4eiin1-101\n'
            + 'Puerto_extremo: 5/1/1\n'
            + 'USUARIO_FINAL_GRI: OMRSCOIPTR-01\n'
            + 'FRU:  PART NUMBER:  LMATERIAL: ---\n'
            + 'Historico de incidencias abiertas por OMEGA\n'
            + 'No hay incidencias relacionadas',
    Responsable: 'OMRSCOIPTR-01',
    Cad_Delegacion: 'OMEGA,OMRSCOIPTR-04, OMRSSOTX-OMEGA, OMRSCOIPTR-01, OMRSSOTX-OMEGA, OMRSSUTX-JDS, OMRSSOTX-OMEGA, OMRSSUTX-JDS, OMRSSOTX-OMEGA, OMRSSUTX-JDS, OMRSSOTX-OMEGA, OMRSSUTX-JDS, OMRSSOTX-OMEGA, OMRSSUTX-JDS, OMRSSUTX-JDS, OMRSSOTX-OMEGA, OMRSSUTX-JDS',
  },
  {
    Secuencia: 'INC-0027546112',
    Jalon: '',
    TRAT_ESP: 'Nulo',
    SAF: 'NO SOLICITADO',
    Estado: 'ACTIVO',
    Red: 'Red Fusión',
    Prov: 'Barcelona',
    F_Presen: '04/03/2024 20:27',
    Sev: '3',
    Equipo: 'hl2bep1-101',
    E_Act: '',
    Raiz: 'INC-0028043971',
    E_Raiz: 'ACTIVO',
    Resp_Raiz: 'OMRSRTCNT',
    ID2: 'Link-Down',
    TIPO_SINTOMA: 'ENLACE CAIDO',
    Sintoma: 'hl2bep1-101:SNMP-WARNING-linkDown:7/2/c13/1\n'
            + 'hl2bep1-101 /// FUSION&HL2 /// TIMOS\n'
            + 'HL2BEP1-101: 19080700 Base SNMP-WARNING-linkDown-2004 [7/2/c13/1]: Interface 7/2/c13/1 is not operational\n'
            + 'Equipo_extremo: hl3gicr1-101\n'
            + 'Puerto_extremo: 5/1/c2/1\n'
            + 'USUARIO_FINAL_GRI: OMRSCOIPTR-01\n'
            + 'FRU:  PART NUMBER:  LMATERIAL: ---\n'
            + 'Historico de incidencias abiertas por OMEGA\n'
            + 'No hay incidencias relacionadas',
    Responsable: 'OMRSCOIPTR-01',
    Cad_Delegacion: 'OMEGA,OMRSCOIPTR-04, OMRSSOTX-OMEGA, OMRSCOIPTR-01, OMRSSOTX-OMEGA, OMRSSUTX-JDS, OMRSSOTX-OMEGA, OMRSSOTX-OMEGA, OMRSSUTX-JDS, OMRSSUTX-JDS, OMRSSOTX-OMEGA, OMRSSUTX-JDS, OMRSCOIPTR-04, OMRSCOIPTR-01, OMRSSUTX-JDS, OMRSSOTX-OMEGA, OMRSSUTX-JDS, OMRSSUTX-JDS, OMRSSOTX-OMEGA, OMRSSUTX-JDS',
  },
  {
    Secuencia: 'INC-0027502806',
    Jalon: '',
    TRAT_ESP: 'Nulo',
    SAF: 'NO SOLICITADO',
    Estado: 'ACTIVO',
    Red: 'Red Fusión',
    Prov: 'Sevilla',
    F_Presen: '26/02/2024 13:25',
    Sev: '3',
    Equipo: 'hl3sor1-101',
    E_Act: 'Cerrada',
    Raiz: 'INC-0027937388',
    E_Raiz: 'ACTIVO',
    Resp_Raiz: 'OMRSCOIPTR-01',
    ID2: 'HW-Error',
    TIPO_SINTOMA: 'ENLACE CAIDO',
    Sintoma: 'hl3sor1-101:SNMP-WARNING-linkDown:1/1/15\n'
            + 'hl3sor1-101 /// FUSION&HL3 /// TIMOS\n'
            + 'hl3sor1-101: 477411 Base SNMP-WARNING-linkDown-2004 [1/1/15]: Interface 1/1/15 is not operational\n'
            + '(Link Down) Se ha perdido el enlace de comunicacion con el elemento de red (Interfaz 1/1/15)\n'
            + 'IF 1/1/15 down in hostname: hl3sor1-101 Conexion con hl4osu1-101 4/2/6\n'
            + 'Equipo_extremo: hl4osu1-101\n'
            + 'Puerto_extremo: 4/2/6\n'
            + 'USUARIO_FINAL_GRI: OMRSCOIPTR-01\n'
            + 'FRU:  PART NUMBER:  LMATERIAL: \n'
            + '---\n'
            + 'Historico de incidencias abiertas por OMEGA\n'
            + '\n'
            + '0027383329 con fecha 07/02/2024 02:13:50 en estado CERRADO\n'
            + 'FECHA REPARACION  : 07/02/2024 06:33:37\n'
            + 'Planta            : RN,RNRF,RNRFRF\n'
            + 'Causa             : RNTA,SC0112\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : rimaDiagnosticadorOK\n'
            + '\n'
            + '0027315356 con fecha 27/01/2024 15:39:05 en estado CERRADO\n'
            + 'FECHA REPARACION  : 27/01/2024 19:33:33\n'
            + 'Planta            : PE,PEMS,PEMSFC\n'
            + 'Causa             : PEMSMP,PECOOL\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : CABLE DE 32 FO CORTADO. SE RECUPERA SERVICIO\n'
            + '\n'
            + '0026899743 con fecha 18/11/2023 17:15:48 en estado CERRADO\n'
            + 'FECHA REPARACION  : 19/11/2023 01:33:25\n'
            + 'Planta            : PE,PECE,PECEFI\n'
            + 'Causa             : PECEFE,PEFOEM\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : \n'
            + '\n'
            + 'TOTAL: 3\n'
            + 'Fin historico de las ultimas incidencias abiertas en OMEGA\n',
    Responsable: 'OMRSCOIPTR-01',
    Cad_Delegacion: 'OMEGA,OMRSCOIPTR-04, OMRSSOTX-OMEGA, OMRSCOIPTR-01, OMRSSUTX-JDS, OMRSSOTX-OMEGA, OMRSSUTX-JDS, OMRSSOTX-OMEGA, OMRSSOTX-OMEGA, OMRSSUTX-JDS, OMRSSOTX-OMEGA, OMRSSUTX-JDS, OMRSSOTX-OMEGA, OMRSSUTX-JDS',
  },
  {
    Secuencia: 'INC-0027408904',
    Jalon: '',
    TRAT_ESP: 'Nulo',
    SAF: 'NO SOLICITADO',
    Estado: 'ACTIVO',
    Red: 'Imagenio Red',
    Prov: 'Guipuzcoa',
    F_Presen: '10/02/2024 4:22',
    Sev: '3',
    Equipo: 'mmrossbi1-01',
    E_Act: 'Retenida',
    Raiz: '',
    E_Raiz: '',
    Resp_Raiz: '',
    ID2: 'HW-Error',
    TIPO_SINTOMA: 'ENLACE CAIDO',
    Sintoma: 'mmrossbi1-01:SNMP-WARNING-linkDown:3/2/1\n'
            + 'mmrossbi1-01 /// IMAGENIO&Router_Catchup_CSL /// TIMOS\n'
            + 'TMNX: 414422 Base SNMP-WARNING-linkDown-2004 [3/2/1]: Interface 3/2/1 is not operational\n'
            + '(Link Down) Se ha perdido el enlace de comunicacion con el elemento de red (Interfaz 3/2/1)\n'
            + 'Equipo_extremo: mmrossbi1-02\n'
            + 'Puerto_extremo: 3/2/1\n'
            + 'USUARIO_FINAL_GRI: OMRSCOIPTR-01\n'
            + 'FRU: 100GBASE-SR10 PART NUMBER: FTLC8281RCNM-A5 LMATERIAL: No_encontrado\n'
            + '---\n'
            + 'Historico de incidencias abiertas por OMEGA\n'
            + '\n'
            + '0027359978 con fecha 02/02/2024 20:11:47 en estado CERRADO\n'
            + 'FECHA REPARACION  : 02/02/2024 20:14:07\n'
            + 'Planta            : MS,OMEG,OMEGA\n'
            + 'Causa             : OTRAMS,\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : OPV_ATLAS_SIN_ASIGNACION No existe asignacion en atlas eleSup_oc=OP\n'
            + '\n'
            + '0027359962 con fecha 02/02/2024 20:08:47 en estado CERRADO\n'
            + 'FECHA REPARACION  : 02/02/2024 20:11:00\n'
            + 'Planta            : MS,OMEG,OMEGA\n'
            + 'Causa             : OTRAMS,\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : OPV_ATLAS_SIN_ASIGNACION No existe asignacion en atlas eleSup_oc=OP\n'
            + '\n'
            + '0027357805 con fecha 02/02/2024 13:45:37 en estado CERRADO\n'
            + 'FECHA REPARACION  : 02/02/2024 13:47:51\n'
            + 'Planta            : MS,OMEG,OMEGA\n'
            + 'Causa             : OTRAMS,\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : OPV_ATLAS_SIN_ASIGNACION No existe asignacion en atlas eleSup_oc=OP\n'
            + '\n'
            + 'TOTAL: 3\n'
            + 'Fin historico de las ultimas incidencias abiertas en OMEGA\n',
    Responsable: 'OMRSCOIPTR-01',
    Cad_Delegacion: 'OMEGA,OMRSCOIPTR-04, OMRSCOIPTR-01, OMRSCOIPAC-02, OMRSCOIPAN-01',
  },
  {
    Secuencia: 'INC-0027321951',
    Jalon: '',
    TRAT_ESP: 'Nulo',
    SAF: 'NO SOLICITADO',
    Estado: 'ACTIVO',
    Red: 'Red Fusión',
    Prov: 'Barcelona',
    F_Presen: '29/01/2024 12:37',
    Sev: '3',
    Equipo: 'emtcdla0338-101',
    E_Act: 'Retenida',
    Raiz: '',
    E_Raiz: '',
    Resp_Raiz: '',
    ID2: 'HW-Error',
    TIPO_SINTOMA: 'ENLACE CAIDO',
    Sintoma: 'emtcdla0338-101:SNMP-WARNING-linkDown:2/1/1\n'
            + 'emtcdla0338-101 /// FUSION&HL4 /// TIMOS\n'
            + 'emtcdla0338-101: 210933 Base SNMP-WARNING-linkDown-2004 [2/1/1]: Interface 2/1/1 is not operational\n'
            + 'tmnxPortTable: Port 2/1/1 ha cambiado de estado. Estado administrativo: inService, estado operacional: outOfService\n'
            + 'Raiz encontrada: INC-0027321318\n'
            + 'Equipo_extremo: hl4btb8-102\n'
            + 'Puerto_extremo: 10/1/17\n'
            + 'USUARIO_FINAL_GRI: OMRSCOIPTR-01\n'
            + 'FRU:  PART NUMBER:  LMATERIAL: \n'
            + '---\n'
            + 'Historico de incidencias abiertas por OMEGA\n'
            + '\n'
            + '0027276321 con fecha 22/01/2024 20:07:44 en estado CERRADO\n'
            + 'FECHA REPARACION  : 22/01/2024 20:24:37\n'
            + 'Planta            : IP,IPR2,IPR2RA\n'
            + 'Causa             : FACOIP,\n'
            + 'Elemento          : ,,\n'
            + 'Descripcion causa : fibra sucia o falso contacto \n'
            + '\n'
            + 'TOTAL: 1\n'
            + 'Fin historico de las ultimas incidencias abiertas en OMEGA\n',
    Responsable: 'OMRSCOIPTR-01',
    Cad_Delegacion: 'OMEGA,OMRSCOIPTR-04, OMRSCOIPTR-01',
  },
]
