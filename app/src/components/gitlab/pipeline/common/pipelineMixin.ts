import { formatDistanceToNow, parseISO } from 'date-fns'
import GitApi from '@/api/gitApi'

interface Pipeline {
  status: string
}

interface Project {
  id: number
  name: string
}

interface Job {
  artifacts_file: any
}

interface User {
  rol: string
}

const pipelineMixin = {
  watch: {
    pipeline(this: { pipeline: Pipeline }) {
      const needToCancel: boolean = ![
        'created', 'waiting_for_resource', 'preparing', 'pending', 'running', 'manual', 'scheduled',
      ].includes(this.pipeline.status)

      if (needToCancel)
        this.cancelAutoUpdate()
    },
  },
  methods: {
    async getProject(this: { projectId: number; project: Project | null }) {
      const gitApi = new GitApi()
      try {
        const response = await gitApi.getSingleProject(this.projectId)

        this.project = response.data as Project
        console.log('getProject this.project:', this.project)
      }
      catch (error) {
        console.error('ERROR! getProject error: ', error)
      }
    },
    formatDuration(value: number): string {
      const seconds: number = Math.floor(value % 60)
      const minutes: number = Math.floor((value / 60) % 60)
      const hours: number = Math.floor(value / 3600)
      let durationString: string = ''
      if (hours > 0)
        durationString += `${hours} hour${hours > 1 ? 's' : ''} `

      if (minutes > 0)
        durationString += `${minutes} minute${minutes > 1 ? 's' : ''} `

      durationString += `${seconds} second${seconds !== 1 ? 's' : ''}`

      return durationString
    },
    timeAgo(timestamp: string | null): string {
      console.log('timeAgo for ', timestamp)
      if (timestamp === null || timestamp === undefined)
        return 'Now'

      const parsedDate = parseISO(timestamp)

      return formatDistanceToNow(parsedDate, { addSuffix: true })
    },
    jobLink(route: string, groupId: number, project: Project, execution: { id: number }) {
      return { name: `${route}-job-logs`, params: { id: groupId, projectId: project.id, jobId: execution.id } }
    },
    allowToDownloadArtifacts(user: User, job: Job): boolean {
      console.log('[allowToDownloadArtifacts] user is : ', user)

      return ['developer', 'user', 'root'].includes(user.rol.toLowerCase()) && job.artifacts_file !== null
    },
    cancelAutoUpdate(this: { timer: number | undefined }) {
      clearInterval(this.timer)
    },
  },
  beforeUnmount(this: { cancelAutoUpdate: () => void }) {
    this.cancelAutoUpdate()
  },
}

export default pipelineMixin
