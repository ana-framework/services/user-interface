import { formatDistanceToNow, parseISO } from 'date-fns'
import GitApi from '@/api/gitApi'

const pipelineMixin = {
  watch: {
    pipeline() {
      const needToCansel = ![
        'created', 'waiting_for_resource', 'preparing', 'pending', 'running', 'manual', 'scheduled',
      ].includes(this.pipeline.status)

      if (
        needToCansel
      )
        this.cancelAutoUpdate()
    },
  },
  methods: {
    async getProject() {
      const gitApi = new GitApi()
      try {
        const response = await gitApi.getSingleProject(this.projectId)

        this.project = response.data
        console.log('getProject this.project:', this.project)
      }
      catch (error) {
        console.error('ERROR! getProject error: ', error)
      }
    },
    formatDuration(value) {
      const seconds = Math.floor(value % 60)
      const minutes = Math.floor((value / 60) % 60)
      const hours = Math.floor(value / 3600)
      let durationString = ''
      if (hours > 0)
        durationString += `${hours} hour${hours > 1 ? 's' : ''} `

      if (minutes > 0)
        durationString += `${minutes} minute${minutes > 1 ? 's' : ''} `

      durationString += `${seconds} second${seconds !== 1 ? 's' : ''}`

      return durationString
    },
    timeAgo(timestamp) {
      console.log('timeAgo for ', timestamp)
      if (timestamp === null || timestamp === undefined)
        return 'Now'

      const parsedDate = parseISO(timestamp)

      return formatDistanceToNow(parsedDate, { addSuffix: true })
    },
    jobLink(route, groupId, project, execution) {
      return { name: `${route}-job-logs`, params: { id: groupId, projectId: project.id, jobId: execution.id } }
    },
    allowToDownloadArtifacts(user, job) {
      console.log('[allowToDownloadArtifacts] user is : ', user)

      return ['developer', 'user', 'root'].includes(user.rol.toLowerCase()) && job.artifacts_file !== null
    },
    cancelAutoUpdate() {
      clearInterval(this.timer)
    },
  },
  beforeDestroy() {
    this.cancelAutoUpdate()
  },
}

export default pipelineMixin
