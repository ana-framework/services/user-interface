<template>
  <v-card>
    <header-template
      :pipeline="pipeline"
      :group-id="groupId"
      :route="route + '-workspace'"
      :project="project"
      :user="user"
    />

    <v-card-text>
      <v-btn
        v-if="['developer', 'user', 'root'].includes(user.rol.toLowerCase())"
        :to="{
                  name: 'workspace',
                  params: { id: groupId, project_id: project.id },
                  query: { tab: 1 }
                }"
        variant="text"
        color="primary"
      >
        Execution list
      </v-btn>

      <!-- SEARCH SECTION -->
      <v-row class="mt-2">
        <v-col cols="12" md="4">
          <v-text-field
            v-model="searchQuery"
            placeholder="Search job logs..."
            append-inner-icon="tabler-search"
            @click:append-inner="searchLogs"
            clearable
          />
        </v-col>

        <v-col cols="12" md="2">
          <v-select
            v-model="filterLevel"
            :items="logLevelOptions"
            item-title= "text"
            label="Filter Level"
            dense
          />
        </v-col>

        <v-col cols="12" md="4">
          <v-switch
            v-model="hideSearchSwitch"
            label="Hide Search Lines"
            color="primary"
            inset
          >
            <template #thumb-on>
              <v-icon>tabler-check</v-icon>
            </template>
            <template #thumb-off>
              <v-icon>tabler-x</v-icon>
            </template>
          </v-switch>

          <v-switch
            v-model="showAllConsoles"
            label="Toggle All Consoles"
            color="primary"
            inset
          >
            <template #thumb-on>
              <v-icon>tabler-chevron-up</v-icon>
            </template>
            <template #thumb-off>
              <v-icon>tabler-chevron-down</v-icon>
            </template>
          </v-switch>
        </v-col>
      </v-row>

      <!-- PIPELINE SECTION -->
      <div class="mt-2">
        <app-timeline>
          <app-timeline-item
            v-for="(stage, index) in stagesList"
            :key="stage.id"
            :color="icon[stage.status].color"
            :icon="icon[stage.status].name"
            class="pb-3 pl-6"
          >
            <div class="d-flex justify-space-between align-center mb-2">
              <h4 class="mb-0"><strong>Stage:</strong> {{ stage.name }}</h4>
              <small class="text-muted">Finished {{ timeAgo(stage.finished_at) }}</small>
            </div>

            <div v-for="(job, index) in stage.jobs" :key="index" class="job-container">
              <logs-console-component
                :job="job"
                :filterLevel="filterLevel"
                :searchQuery="searchQuery"
                :hideSearchSwitch="hideSearchSwitch"
                :searchButton="searchButton"
                :showAllConsoles="showAllConsoles"
              />
            </div>
          </app-timeline-item>
        </app-timeline>
      </div>
      <!-- END PIPELINE SECTION -->

      <!-- CANCEL BUTTON SECTION -->
      <div class="d-flex justify-end">
        <v-btn
          color="error"
          class="mt-2 mr-2"
          :loading="spinning"
          :disabled="canceling || ['canceled', 'success', 'failed'].includes(pipeline.status)"
          @click="canselPipeline"
        >
          <v-progress-circular
            v-if="spinning"
            indeterminate
            color="white"
            size="20"
            class="mr-2"
          />
          Cancel pipeline
        </v-btn>
      </div>
      <!-- END CANCEL BUTTON SECTION -->

      <!-- Floating button to scroll to top -->
      <v-btn
        color="primary"
        class="back-to-top"
        @click="scrollToTop"
        icon
      >
        <v-icon>tabler-arrow-up</v-icon>
      </v-btn>

      <!-- Floating button to scroll to bottom -->
      <v-btn
        color="primary"
        class="back-to-bottom"
        @click="scrollToBottom"
        icon
      >
        <v-icon>tabler-arrow-down</v-icon>
      </v-btn>

    </v-card-text>
  </v-card>
</template>

<script>
import GitApiCall from '@/api/gitApi';
import GitlabHelperApi from '@/api/gitlabHelperApi';
import AppTimeline from '@core/components/app-timeline/AppTimeline.vue';
import AppTimelineItem from '@core/components/app-timeline/AppTimelineItem.vue';
import Ripple from 'vue-ripple-directive';
/* Gitlab Views Components */
import HeaderTemplate from '@/components/gitlab/pipeline/common/HeaderTemplate.vue';
import pipelineMixin from '@/components/gitlab/pipeline/common/pipelineMixin.js';
import LogsConsoleComponent from '@/components/gitlab/pipeline/report/LogsConsoleComponent.vue';
import { useRoute } from 'vue-router';
import { useStore } from 'vuex';



export default {
  name: 'ExecutionLogsComponent',
  mixins: [pipelineMixin],
  components: {
    LogsConsoleComponent,
    HeaderTemplate,
    AppTimeline,
    AppTimelineItem,
  },
  directives: { Ripple },
  props: {
    route: {
      type: String,
      default: 'vendors',
    },
  },
  data() {
    return {
      canceling: false,
      spinning: false,
      user: { username: '', rol: '' },
      groupId: 0,
      projectId: 0,
      pipelineId: 0,
      project: {},
      pipeline: {},
      stagesList: [],
      formJobsContent: {},
      timer: '',
      icon: {
        pending: { color: 'primary', name: 'LoaderIcon' },
        manual: { color: 'warning', name: 'SettingsIcon' },
        running: { color: 'primary', name: 'MoreHorizontalIcon' },
        created: { color: 'secondary', name: 'MoreHorizontalIcon' },
        canceled: { color: 'secondary', name: 'MinusIcon' },
        success: { color: 'success', name: 'CheckIcon' },
        failed: { color: 'danger', name: 'XIcon' },
        skipped: { color: 'secondary', name: 'ChevronsRightIcon' },
      },
      jobId: 0,
      job: { pipeline: {} },
      logsListPerJobs: {},
      logFileData: [],
      processedLogs: {}, // Almacena los logs procesados
      time: '',
      // Para mostrar u ocultar la consola de los jobs
      logsLoaded: {},
      // SECCION DE BUSQUEDA
      hideSearchSwitch: false,
      showAllConsoles: true,
      searchQuery: '',
      filterLevel: 'All', // Esto se actualizará con el valor seleccionado en el dropdown
      searchButton: 0,
      logLevelOptions: [
        { value: 'All', text: 'All Levels' }, // Default option
        { value: 'TRACE', text: 'TRACE' },
        { value: 'DEBUG', text: 'DEBUG' },
        { value: 'INFO', text: 'INFO' },
        { value: 'WARNING', text: 'WARNING' },
        { value: 'ERROR', text: 'ERROR' },
        { value: 'CRITICAL', text: 'CRITICAL' },
      ],
    };
  },
  watch: {
    // If the hideSearchSwitch change the value (true or false), the searchQuery and filterLevel are restarted
    hideSearchSwitch() {
      this.searchQuery = ''; // default
      this.filterLevel = 'All'; // default
      this.searchLogs(); // empty search for reset
    },
  },
  computed: {},
  async mounted() {
    // Initialization Data and functions here
    const store = useStore()
    const route = useRoute()
    console.log('[LogsConsoleComponent] store is:', store)
    console.log('[LogsConsoleComponent] route is:', route)
    this.user = store.state.user;
    this.groupId = route.params.id;
    this.projectId = route.params.projectId;
    this.pipelineId = route.params.pipelineId;
    await this.getProject();
    await this.getPipeline();
    await this.getStagesList();
    await this.getFormsJobsContent();
    await this.statusRequest();
  },
  methods: {
    async getJob() {
      console.log('getJob ... .');
      const gitApi = new GitApiCall();
      try {
        const response = await gitApi.getJob(this.projectId, this.jobId);
        this.job = response.data;
        console.log('getJob this.job', this.job);
      } catch (error) {
        console.error('ERROR! getJob error: ', error);
      }
    },
    async getPipeline() {
      console.log('getPipeline ... .');
      const gitApi = new GitApiCall();
      try {
        const response = await gitApi.getPipeline(this.projectId, this.pipelineId);
        this.pipeline = response.data;
        console.log('getPipeline this.pipeline:', this.pipeline);
      } catch (error) {
        console.error('ERROR! getPipeline error: ', error);
      }
    },
    async getStagesList() {
      console.log('getStagesList ... .');
      const gitHelperApi = new GitlabHelperApi();
      console.log('getStagesList: idProject: ', this.projectId, ' idPipeline: ', this.pipelineId);
      try {
        const response = await gitHelperApi.getStagesList(this.projectId, this.pipelineId,[]);
        this.stagesList = response.data.data;
        console.log('getStagesList this.stagesList: ', this.stagesList);
      } catch (error) {
        console.error('ERROR! getProjects', error);
      }
    },
    async statusRequest() {
      console.log('statusRequest pipline.status: ', this.pipeline.status);
      const needUpdate = [
        'created', 'waiting_for_resource', 'preparing', 'pending', 'running', 'manual', 'scheduled',
      ].includes(this.pipeline.status) || typeof this.pipeline.status === 'undefined';
      if (
        needUpdate
      ) {
        this.timer = setInterval(() => {
          this.getPipeline();
          this.getStagesList();
        }, 5000);
      }
    },
    async getFormsJobsContent() {
      const git = new GitApiCall();
      try {
        const response = await git.getFileContent(this.projectId, 'ui%2Fforms%2Fjobs.json/raw?ref=main');
        this.formJobsContent = response.data;
        console.log('getFormsJobsContent this.fileContentData: ', this.formJobsContent);
      } catch (error) {
        this.formJobsContent = {};
        console.error('ERROR! getProjects', error);
      }
    },
    async canselPipeline() {
      this.canceling = true;
      this.spinning = true;
      const gitApi = new GitApiCall();
      try {
        const response = await gitApi.cancelPipeline(this.projectId, this.pipelineId);
        console.log('cancelPipeline: ', response.data);
      } catch (error) {
        console.error('error cancelPipeline', error);
      }
      this.spinning = false;
    },
    async downloadArtifacts(jobId, filename) {
      const gitApi = new GitApiCall();
      try {
        // Call the GitApi.downloadArtifacts method and wait for the response
        const response = await gitApi.downloadArtifacts(this.projectId, jobId);

        // Handle the response here
        const blob = new Blob([response.data], { type: 'application/zip' });
        const link = document.createElement('a');
        link.href = URL.createObjectURL(blob);
        link.download = `${filename}.zip`;
        link.click();
        console.log('Download successful!');
      } catch (error) {
        console.error('Error downloading artifacts', error);
      }
    },
    async retryJob(jobId) {
      const gitApi = new GitApiCall();
      try {
        const response = await gitApi.retryJob(this.projectId, jobId);
        console.log('retryJob: ', response.data);
        this.timer = setInterval(() => {
          this.getPipeline();
          this.getStagesList();
        }, 5000);
      } catch (error) {
        console.error('Error retrying job', error);
      }
    },
    listOfFormsItems(stepName) {
      return this.formJobsContent[stepName];
    },
    // 'auto' or 'smooth'
    scrollToTop() {
      window.scrollTo({
        top: 0,
        behavior: 'auto',
      });
    },
    scrollToBottom() {
      window.scrollTo({
        top: document.body.scrollHeight,
        behavior: 'auto',
      });
    },
    // Function to filter Logs
    searchLogs() {
      this.searchButton += 1; // Incrementa la clave para forzar la actualización
    },
    /*
    downloadPDF() {
      const element = document.documentElement;
      const options = {
        margin: 10,
        filename: 'test_report.pdf',
        image: { type: 'jpeg', quality: 0.98 },
        html2canvas: { scale: 2 },
        jsPDF: { unit: 'mm', format: 'a4', orientation: 'portrait' }
      };
      html2pdf().from(element).set(options).save();
    },
    downloadHTML() {
      // Crear un nuevo documento HTML con el contenido actual
      const htmlContent = document.documentElement.outerHTML;
      const newDocument = document.implementation.createHTMLDocument();
      newDocument.write(htmlContent);

      // Descargar el documento HTML
      const blob = new Blob([newDocument.documentElement.outerHTML], { type: 'text/html' });
      const url = URL.createObjectURL(blob);

      const a = document.createElement('a');
      a.href = url;
      a.download = 'test_report.html';
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    },
    sendViaEmail() {
      // Definir el asunto y cuerpo en inglés y con el contenido de la página web
      const subject = encodeURIComponent("Report - Your English Subject");
      const body = encodeURIComponent(document.documentElement.outerHTML);
      const mailtoLink = `mailto:?subject=${subject}&body=${body}`;

      window.location.href = mailtoLink;
    },
    */
  },
};

</script>
