import { formatDistanceToNow, parseISO } from 'date-fns'

export const statusColor = (status: string) => {
  const colors = {
    pending: 'primary',
    manual: 'warning',
    running: 'primary',
    created: 'secondary',
    canceled: 'secondary',
    success: 'success',
    failed: 'error',
    skipped: 'secondary',
  }

  return colors[status]
}

export const ratioColor = (ratio: number) => {
  if (ratio < 3)
    return 'error'
  else if (ratio < 5)
    return 'warning'
  else
    return 'success'
}

export const timeAgo = (timestamp: string | null) => {
  if (timestamp)
    return formatDistanceToNow(parseISO(timestamp), { addSuffix: true })

  return ''
}
