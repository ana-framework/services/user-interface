validInputValues: ['input', 'password', 'email', 'selector', 'file']

Si el array tiene dos keys, uno para el título y otro para el valor, puedes utilizar la propiedad value-field en el
componente v-select para indicar qué key corresponde al valor de la opción. Además, puedes usar la propiedad v-model
para establecer el valor seleccionado y la propiedad default-value para definir la opción por defecto.

Aquí tienes un ejemplo con un array que tiene dos keys: title para el título y value para el valor:

<template>
  <div>
    <label>Seleccione la marca</label>
    <v-select
      :options="marcas"
      v-model="marcaSeleccionada"
      :value-field="'value'" <!-- Indica que 'value' es el campo del valor -->
      default-value="vw" <!-- Opción por defecto con el valor 'vw' -->
    />
  </div>
</template>

<script>
export default {
  data() {
    return {
      marcas: [
        { title: 'Saab', value: 'saab' },
        { title: 'VW', value: 'vw' },
        { title: 'Audi', value: 'audi' },
      ],
      marcaSeleccionada: 'vw', // Opción por defecto
    };
  },
};
</script>

----------------------------------------------------------------
<template>
  <form-wizard
      color="#0473bc"
      title=""
      subtitle=""
      finish-button-text="Launch Pipeline"
      back-button-text="Previous"
      class="steps-transparent mb-3"
      @on-complete="launchPipelineButton"
  >

    <!-- account details tab -->
    <tab-content
        v-for="(group, group_key, _index) in fileContentData"
        :title="group.title"
        :icon="'feather ' + group.icon"
        v-bind:key="group_key"
    >
      <b-row>
        <b-col
            cols="12"
            class="mb-2"
        >
          <h5 class="mb-0">
            {{ group.cardTitle }}
          </h5>
          <small class="text-muted">
            {{ group.cardSubtitle }}
          </small>
        </b-col>
        <b-col md="6" cols="12" v-for="(content, content_key, _index) in group.content" v-bind:key="content_key">

          <!-- Check if input type is a valid type -->
          <b-alert
              v-if="!validInputValues.includes(content.type.toLowerCase())"
              variant="danger"
              show
          >
            <div class="alert-body">
              <feather-icon
                  icon="InfoIcon"
                  class="mr-50"
              />
              <span>
                  <strong>ERROR!</strong> Wrong format for {{ content.id }} variable. {{ content.type }} is not valid,
                  it must be one of {{ validInputValues }}.
                </span>
            </div>
          </b-alert>

          <!-- If input type is a selector, this is implemented -->
          <b-form-group
              v-else-if="content.type.toLowerCase()==='selector'"
              :label="content.label"
              :label-for="content.id"
          >
            <v-select
                :options="content.options"
                v-model="envVars[group_key][content_key]"
                value="value"
                label="label"
            />
            <!-- v-select
                :dir="$store.state.appConfig.isRTL ? 'rtl' : 'ltr'"
                label="title"
                :options="content.options"
                v-model="envVars[group_key][content_key]"

                :value="marcaSeleccionada"

                :value-field="'value'"
                :label-field="'label'"


                :options="marcas"
                v-model="marcaSeleccionada"
                value="value"
                label="label"
            / -->
          </b-form-group>
          <!-- v-model="envVars[group_key][content_key][-options]"
               :value-field="'value'"        Indica que 'value' es el campo del valor
          -->

          <!-- If input type is a file_input, this is implemented -->
          <b-form-group
              v-else-if="content.type.toLowerCase()==='file'"
              :label="content.label"
              :label-for="content.id"
          >
            <b-form-file
                v-model="envVars[group_key][content_key]"
                ref="file-input"
                placeholder="Choose a file or drop it here..."
                drop-placeholder="Drop file here..."
            />
          </b-form-group>

          <!-- Else simple input is implemented -->
          <b-form-group
              v-else
              :label="content.label"
              :label-for="content.id"
          >
            <b-form-input
                :id="content.id"
                :placeholder="envVars[group_key][content_key]"
                :type="content.type"
                v-model="envVars[group_key][content_key]"
            />
          </b-form-group>
        </b-col>
      </b-row>
    </tab-content>
  </form-wizard>
</template>

<script>
import { FormWizard, TabContent } from 'vue-form-wizard';
import vSelect from 'vue-select';
import 'vue-form-wizard/dist/vue-form-wizard.min.css';
import {
  BRow,
  BCol,
  BFormGroup,
  BFormInput,
  BFormFile,
  BAlert,
} from 'bootstrap-vue';
import ToastificationContent from '@core/components/toastification/ToastificationContent';
import GitApiCall from '@/api/gitApi';

export default {
  name: 'PipelineSetupFormComponent',
  components: {
    FormWizard,
    TabContent,
    BRow,
    BCol,
    BFormGroup,
    BFormInput,
    BFormFile,
    vSelect,
    BAlert,
    // eslint-disable-next-line vue/no-unused-components
    ToastificationContent,
  },
  props: {
    groupId: {
      type: Number,
      default: 0,
    },
    fileContentData: {
      type: Object,
    },
    branch: {
      type: String,
      default: 'main',
    },
    idProject: {
      type: Number,
      default: 0,
    },
    route: {
      type: String,
      default: 'vendors',
    },
  },
  data() {
    return {
      validInputValues: ['input', 'password', 'email', 'selector', 'file'],
      envVars: {},
      firstUpdate: true, // Bandera para rastrear la primera actualización
      statusButton: -1,
    };
  },
  mounted() {
    this.createDefaultValues();
  },
  watch: {
    fileContentData(newValue) {
      // TODO Esto seria lo nuevo, En este ejemplo, el bloque watch se encarga de actualizar los valores iniciales
      //  tanto del formulario como del selector cada vez que fileContentData cambie. Esto garantiza que los valores
      //  se actualicen cuando fileContentData cambie y se reflejen en la interfaz.
      // Actualizar los valores iniciales aquí
      // Supongamos que fileContentData tiene propiedades como formData y selectedOption

      this.envVars = newValue.envVars; // Actualiza el formulario
      this.statusButton = newValue.statusButton; // Actualiza la opción status button     //// No lo tengo claro

      if (this.firstUpdate) {
        // Si es la primera actualización, asigna un valor por defecto a marcaSeleccionada
        this.createDefaultValues();
        this.firstUpdate = false; // Marca la primera actualización como realizada

      console.log('HA CAMBIADO envVars:', this.envVars);
      console.log('HA CAMBIADO statusButton:', this.statusButton);
      }

    },
  },
  methods: {
    // Este metodo se lanza cuando el usuario cambia la opcion en el selector, es la opcion mas facil para mostrar el cambio
    // Cada vez que el usuario cambie la opción en el selector, se llamará a este método y se imprimirá la propiedad
    handleMarcaSeleccionadaChange() {
      console.log('HA CAMBIADO envVars:', this.envVars);

    },
    // When push the button, launches a new pipeline
    async launchPipelineButton() {
      const envVars = [];
      console.log('EnvVars:', this.envVars);
      console.log("launchPipelineButton this.$refs['file-input']:", this.$refs['file-input']);

      // TODO: !! is important to solve this eslint problems
      // eslint-disable-next-line no-restricted-syntax, guard-for-in
      for (const groupKey in this.envVars) {
        // eslint-disable-next-line no-restricted-syntax, guard-for-in
        for (const contentKey in this.envVars[groupKey]) {
          const value = this.envVars[groupKey][contentKey];
          const { type } = this.fileContentData[groupKey].content[contentKey];
          console.log('launchPipelineButton {groupKey}:', groupKey);
          console.log('launchPipelineButton [type]:', type);
          console.log('launchPipelineButton contentKey:', contentKey);
          console.log('launchPipelineButton value:', value);
          console.log('----------------------------------:');

          if (type === 'file') {
            if (value) {
              envVars.push({
                key: contentKey,
                variable_type: 'file',
                // eslint-disable-next-line no-await-in-loop
                value: await value.text(),
              });
            }
          } else if (type === 'selector') {
            envVars.push({
              key: contentKey,
              value: value.value,
            });
          } else {
            envVars.push({
              key: contentKey,
              value,
            });
          }
        }
      }
      console.log('launchPipelineButton this.idProject:', this.idProject);
      const git = new GitApiCall();
      try {
        const response = await git.launchPipeline(this.idProject, envVars, this.branch);
        const launchPipeline = response.data;
        console.log('launchPipelineButton launchPipeline: ', launchPipeline);
        const route = {
          name: `${this.route}-job-list`,
          // TODO: this.groupId is undefine.
          params: { id: this.groupId, project_id: this.idProject, pipeline_id: launchPipeline.id },
        };
        console.log('launchPipelineButton route: ', route);
        this.$router.push(route);
      } catch (error) {
        console.error('ERROR launchPipelineButton', error);
      }
      // change the status of the button
      this.statusButton = this.idProject;
    },
    // Cancel last pipeline
    async cancelPipelineButton() {
      const git = new GitApiCall();
      try {
        const idPipeline = await this.getPipelineId(this.idProject);
        // this cancel the pipeline
        const cancelPipeline = await git.cancelPipeline(this.idProject, idPipeline);
        console.log('cancelPipelineButton cancelPipeline: ', cancelPipeline);
      } catch (error) {
        console.error('ERROR cancelPipelineButton', error);
      }
      // change the status of the button
      this.statusButton = -1;
    },
    // We get the last pipeline number
    async getPipelineId() {
      const git = new GitApiCall();
      try {
        const responseList = await git.getPipelinesList(this.idProject);
        const resp = JSON.parse(JSON.stringify(responseList.data));
        console.log('getPipelineId resp: ', resp);
        const { id } = resp[0];
        console.log('getPipelineId id: ', id);
        return id;
      } catch (error) {
        console.error('ERROR getPipelineId', error);
      }
      return null;
    },
    // Create default values
    createDefaultValues() {
      Object.keys(this.fileContentData).forEach((group) => {
        this.envVars[group] = {};

        Object.keys(this.fileContentData[group].content).forEach((key) => {
          if (this.fileContentData[group].content[key].type === 'selector') {
            this.envVars[group][key] = this.fileContentData[group].content[key].default;      // this is new to render the default value
          } else {
            this.envVars[group][key] = this.fileContentData[group].content[key].default;
          }
        });
      });

      console.log('createDefaultValues this.EnvVars: ', this.envVars);
    },
  },
};
</script>

<style lang="scss">
@import '@core/scss/vue/libs/vue-wizard.scss';
@import '@core/scss/vue/libs/vue-select.scss';
</style>

----------------------------------------------------------------
ESTA VARIANTE FUNCIONA:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

<template>
  <form-wizard
      color="#0473bc"
      title=""
      subtitle=""
      finish-button-text="Launch Pipeline"
      back-button-text="Previous"
      class="steps-transparent mb-3"
      @on-complete="launchPipelineButton"
  >

    <!-- account details tab -->
    <tab-content
        v-for="(group, group_key, _index) in fileContentData"
        :title="group.title"
        :icon="'feather ' + group.icon"
        v-bind:key="group_key"
    >
      <b-row>
        <b-col
            cols="12"
            class="mb-2"
        >
          <h5 class="mb-0">
            {{ group.cardTitle }}
          </h5>
          <small class="text-muted">
            {{ group.cardSubtitle }}
          </small>
        </b-col>
        <b-col md="6" cols="12" v-for="(content, content_key, _index) in group.content" v-bind:key="content_key">

          <!-- Check if input type is a valid type -->
          <b-alert
              v-if="!validInputValues.includes(content.type.toLowerCase())"
              variant="danger"
              show
          >
            <div class="alert-body">
              <feather-icon
                  icon="InfoIcon"
                  class="mr-50"
              />
              <span>
                  <strong>ERROR!</strong> Wrong format for {{ content.id }} variable. {{ content.type }} is not valid,
                  it must be one of {{ validInputValues }}.
                </span>
            </div>
          </b-alert>

          <!-- If input type is a selector, this is implemented -->
          <b-form-group
              v-else-if="content.type.toLowerCase()==='selector'"
              :label="content.label"
              :label-for="content.id"
          >
            <v-select
                :options="content.options"
                v-model="envVars[group_key][content_key]"
                :value-field="'value'"
                :label-field="'label'"
                @input="handleMarcaSeleccionadaChange(group_key, content_key, $event)"
            />
            <!-- v-select
                :dir="$store.state.appConfig.isRTL ? 'rtl' : 'ltr'"
                label="title"
                :options="content.options"
                v-model="envVars[group_key][content_key]"

                :value="marcaSeleccionada"

                :value-field="'value'"
                :label-field="'label'"


                :options="marcas"
                v-model="marcaSeleccionada"
                value="value"
                label="label"


                value="value"
                label="label"
                @input="handleMarcaSeleccionadaChange"
            / -->
          </b-form-group>
          <!-- v-model="envVars[group_key][content_key][-options]"
               :value-field="'value'"        Indica que 'value' es el campo del valor
          -->

          <!-- If input type is a file_input, this is implemented -->
          <b-form-group
              v-else-if="content.type.toLowerCase()==='file'"
              :label="content.label"
              :label-for="content.id"
          >
            <b-form-file
                v-model="envVars[group_key][content_key]"
                ref="file-input"
                placeholder="Choose a file or drop it here..."
                drop-placeholder="Drop file here..."
            />
          </b-form-group>

          <!-- Else simple input is implemented -->
          <b-form-group
              v-else
              :label="content.label"
              :label-for="content.id"
          >
            <b-form-input
                :id="content.id"
                :placeholder="envVars[group_key][content_key]"
                :type="content.type"
                v-model="envVars[group_key][content_key]"
            />
          </b-form-group>
        </b-col>
      </b-row>
    </tab-content>
  </form-wizard>
</template>

<script>
import { FormWizard, TabContent } from 'vue-form-wizard';
import vSelect from 'vue-select';
import 'vue-form-wizard/dist/vue-form-wizard.min.css';
import {
  BRow,
  BCol,
  BFormGroup,
  BFormInput,
  BFormFile,
  BAlert,
} from 'bootstrap-vue';
import ToastificationContent from '@core/components/toastification/ToastificationContent';
import GitApiCall from '@/api/gitApi';

export default {
  name: 'PipelineSetupFormComponent',
  components: {
    FormWizard,
    TabContent,
    BRow,
    BCol,
    BFormGroup,
    BFormInput,
    BFormFile,
    vSelect,
    BAlert,
    // eslint-disable-next-line vue/no-unused-components
    ToastificationContent,
  },
  props: {
    groupId: {
      type: Number,
      default: 0,
    },
    fileContentData: {
      type: Object,
    },
    branch: {
      type: String,
      default: 'main',
    },
    idProject: {
      type: Number,
      default: 0,
    },
    route: {
      type: String,
      default: 'vendors',
    },
  },
  data() {
    return {
      validInputValues: ['input', 'password', 'email', 'selector', 'file'],
      envVars: {},
      firstUpdate: true,
      statusButton: -1,
    };
  },
  mounted() {
    // TODO: Ni Mounted, ni befouremounted funcionan porque esta funcion depende de la llamada api a gitlab y hasta que
    // la web no carga no tenemos el valor del contentdata que viene de la API.
    // this.createDefaultValues();
  },
  watch: {
    fileContentData() {
      // TODO Esto seria lo nuevo, En este ejemplo, el bloque watch se encarga de actualizar los valores iniciales
      //  tanto del formulario como del selector cada vez que fileContentData cambie. Esto garantiza que los valores
      //  se actualicen cuando fileContentData cambie y se reflejen en la interfaz.
      // Actualizar los valores iniciales aquí
      // Supongamos que fileContentData tiene propiedades como formData y selectedOption
      if (this.firstUpdate) { // Saber si el objeto esta vacio
          this.createDefaultValues();
          console.log('DENTRO HA CAMBIADO envVars:', this.envVars);
          console.log('DENTRO HA CAMBIADO statusButton:', this.statusButton);
          this.firstUpdate = false;
      }
    },
  },
  methods: {
    handleMarcaSeleccionadaChange(groupKey, contentKey, newValue) {
      console.log('this.envVars:', this.envVars);
      console.log('groupKey:', groupKey);
      console.log('contentKey:', contentKey);
      console.log('newValue:', newValue);
      console.log('--------------------++++++++++++++++---------------');
      // Lógica para manejar el cambio con groupKey y contentKey
      this.envVars[groupKey][contentKey] = newValue;      // this is new to render the default value

      // Forzar la actualización de la vista
      this.$forceUpdate();

    },
    // When push the button, launches a new pipeline
    async launchPipelineButton() {
      const envVars_gitlab = [];
      console.log('EnvVars:', this.envVars);
      console.log("launchPipelineButton this.$refs['file-input']:", this.$refs['file-input']);

      // TODO: !! is important to solve this eslint problems
      // eslint-disable-next-line no-restricted-syntax, guard-for-in
      for (const groupKey in this.envVars) {
        // eslint-disable-next-line no-restricted-syntax, guard-for-in
        for (const contentKey in this.envVars[groupKey]) {
          const value = this.envVars[groupKey][contentKey];
          const { type } = this.fileContentData[groupKey].content[contentKey];
          console.log('launchPipelineButton {groupKey}:', groupKey);
          console.log('launchPipelineButton [type]:', type);
          console.log('launchPipelineButton contentKey:', contentKey);
          console.log('launchPipelineButton value:', value);
          console.log('----------------------------------:');

          if (type === 'file') {
            if (value) {
              envVars_gitlab.push({
                key: contentKey,
                variable_type: 'file',
                // eslint-disable-next-line no-await-in-loop
                value: await value.text(),
              });
            }
          } else if (type === 'selector') {
            envVars_gitlab.push({
              key: contentKey,
              value: value.value,
            });
          } else {
            envVars_gitlab.push({
              key: contentKey,
              value,
            });
          }
        }
      }
      console.log('launchPipelineButton this.idProject:', this.idProject);
      const git = new GitApiCall();
      try {
        const response = await git.launchPipeline(this.idProject, envVars_gitlab, this.branch);
        const launchPipeline = response.data;
        console.log('launchPipelineButton launchPipeline: ', launchPipeline);
        const route = {
          name: `${this.route}-job-list`,
          // TODO: this.groupId is undefine.
          params: { id: this.groupId, project_id: this.idProject, pipeline_id: launchPipeline.id },
        };
        console.log('launchPipelineButton route: ', route);
        this.$router.push(route);
      } catch (error) {
        console.error('ERROR launchPipelineButton', error);
      }
      // change the status of the button
      this.statusButton = this.idProject;
    },
    // Cancel last pipeline
    async cancelPipelineButton() {
      const git = new GitApiCall();
      try {
        const idPipeline = await this.getPipelineId(this.idProject);
        // this cancel the pipeline
        const cancelPipeline = await git.cancelPipeline(this.idProject, idPipeline);
        console.log('cancelPipelineButton cancelPipeline: ', cancelPipeline);
      } catch (error) {
        console.error('ERROR cancelPipelineButton', error);
      }
      // change the status of the button
      this.statusButton = -1;
    },
    // We get the last pipeline number
    async getPipelineId() {
      const git = new GitApiCall();
      try {
        const responseList = await git.getPipelinesList(this.idProject);
        const resp = JSON.parse(JSON.stringify(responseList.data));
        console.log('getPipelineId resp: ', resp);
        const { id } = resp[0];
        console.log('getPipelineId id: ', id);
        return id;
      } catch (error) {
        console.error('ERROR getPipelineId', error);
      }
      return null;
    },
    // Create default values
    createDefaultValues() {
      Object.keys(this.fileContentData).forEach((group) => {
        this.envVars[group] = {};
        Object.keys(this.fileContentData[group].content).forEach((key) => {
          this.envVars[group][key] = this.fileContentData[group].content[key].default;
        });
      });

      console.log('createDefaultValues this.EnvVars: ', this.envVars);
    },
  },
};
</script>

<style lang="scss">
@import '@core/scss/vue/libs/vue-wizard.scss';
@import '@core/scss/vue/libs/vue-select.scss';
</style>

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::











{
   "GROUP_1": {
      "title": "Infrastructure Variables",
      "icon": "icon-cloud",
      "cardTitle": "Infrastructure and Network Function variables of the pipeline",
      "cardSubtitle": "You must fill in the variables correctly to launch the pipeline with the correct version and in the corresponding environment.",
      "content": {   
            "DEPLOYMENT":{
               "type":"input",
               "id":"deployment",
               "label":"PCF Deployment",
               "default":"True",
               "required":true
            },
            "PCF_NAMESPACE":{
               "type":"input",
               "id":"pcf_namespace",
               "label":"PCF Namespace",
               "default":"pcf",
               "required":true
            },
              "DBTIER_NAMESPACE":{
                 "type":"input",
                 "id":"dbtier_namespace",
                 "label":"DB Namespace",
                 "default":"occne-cndbtier",
                 "required":true
              },
              "REPOSITORY_URL":{
                 "type":"selector",
                 "id":"repository_url",
                 "label":"Repository URL",
                 "value":"https://harbor.vmware.5glabaltran.com",
                 "required":true,
                 "default":{
                    "title":"Harbor"
                 },
                 "options":[
                    {
                       "title":"Harbor",
                       "value":"https://harbor.vmware.5glabaltran.com"
                    },
                    {
                       "title":"Nexus",
                       "value":"https://nexus.vmware.5glabaltran.com"
                    }
                 ]
              },
              "TARGET_CLUSTER":{
                 "type":"selector",
                 "id":"target_cluster",
                 "label":"Target K8s Cluster",
                 "value":"tkganaclient",
                 "required":true,
                 "default":{
                    "title":"TKG ANA Client"
                 },
                 "options":[
                    {
                       "title":"TKG ANA Client",
                       "value":"tkganaclient"
                    },
                    {
                       "title":"Bare-Metal",
                       "value":"bare-metal"
                    },
                    {
                       "title":"Kubernetes Open Stack",
                       "value":"openstack"
                    }
                 ]
              },
              "PCF_VALUES_FILE":{
                 "type":"file",
                 "id":"pcf_values_file",
                 "label":"PCF Values File",
                 "value":"default value",
                 "required":false
              },
              "DBTIER_VALUES_FILE":{
                 "type":"file",
                 "id":"dbtier_values_file",
                 "label":"DB Values File",
                 "value":"default value",
                 "required":false
              },
              "TCA_USER":{
                 "type":"input",
                 "id":"tca_user",
                 "label":"TCA User",
                 "default":"TCA_Admin@vsphere.local",
                 "required":true
              },
              "TCA_PASSWORD":{
                 "type":"password",
                 "id":"tca_password",
                 "label":"TCA Password",
                 "default":"TC@_admin123!",
                 "required":true
              }
           }
      },
      "GROUP_2": {
         "title": "Test Tool Variables",
         "icon": "icon-file-text",
         "cardTitle": "Global variables of the pipeline",
         "cardSubtitle": "You must fill in the variables correctly to launch the pipeline with the correct version and in the corresponding environment.",
         "content": {
              "VENDOR":{
                 "type":"input",
                 "id":"vendor",
                 "label":"Testing Vendor",
                 "default":"Spirent",
                 "required":true
              },
              "TOOL":{
                 "type":"input",
                 "id":"tool",
                 "label":"Testing Tool",
                 "default":"Landslide",
                 "required":true
              },
              "LANDSLIDE_IP":{
                 "type":"input",
                 "id":"landslide_ip",
                 "label":"IP",
                 "default":"192.168.100.221",
                 "required":true
              },
              "LANDSLIDE_PORT":{
                 "type":"input",
                 "id":"landslide_port",
                 "label":"Port",
                 "default":"8080",
                 "required":true
              }
         }
       },
       "GROUP_3": {
         "title": "Use Cases/Test Cases Variables",
         "icon": "icon-edit",
         "cardTitle": "Verification and configuration variables",
         "cardSubtitle": "Enter the variables necessary to configure and check the correct functioning of the environment",
         "content": {
              "LANDSLIDE_USERNAME":{
                 "type":"input",
                 "id":"landslide_username",
                 "label":"User Name",
                 "default":"sms",
                 "required":true
              },
              "LANDSLIDE_PASSWORD":{
                 "type":"password",
                 "id":"landslide_password",
                 "label":"User password",
                 "default":"a1b2c3d4",
                 "required":true
              },
              "LANDSLIDE_LIBRARY":{
                 "type":"input",
                 "id":"landslide_library",
                 "label":"Library Name",
                 "default":"PCFtest",
                 "required":true
              },
              "LANDSLIDE_TESTCASE":{
                 "type":"input",
                 "id":"landslide_testcase",
                 "label":"Test Case Name",
                 "default":"Test SMF to PCF - Oracle - Tanzu",
                 "required":true
              },
              "UPDATE_PARAMETER":{
                 "type":"input",
                 "id":"update_parameter",
                 "label":"Update Parameter",
                 "default":"False",
                 "required":true
              }
         }
       },
       "GROUP_4": {
         "title": "Test Configuration Variables",
         "icon": "icon-code",
         "cardTitle": "Verification and configuration variables",
         "cardSubtitle": "Enter the variables necessary to configure and check the correct functioning of the environment",
         "content": {
              "LANDSLIDE_TESTCASE_OVERRIDE":{
                 "type":"input",
                 "id":"landslide_testcase_override",
                 "label":"New Test Case Name",
                 "default":"",
                 "required":true
              },
              "LANDSLIDE_PARAM":{
                 "type":"input",
                 "id":"landslide_param",
                 "label":"Parameters",
                 "default":"{ \"Sessions\" : 1, \"SbiTransRate\" : \"1.0\" }",
                 "required":false
              },
              "LANDSLIDE_STEPS":{
                 "type":"input",
                 "id":"landslide_steps",
                 "label":"Test Case Steps",
                 "default":"[{ \"delaySec\" : 0, \"tcActivity\" : \"Start\" }, { \"delaySec\" : 45, \"tcActivity\" : \"Stop\" }]",
                 "required":false
              }
         }
       }
}
==============================================================================================================
--------------------------------------------------------------------------------------------------------------
<template>


    <!-- Card for Dropdown Selection -->
    <b-card class="p-0 mb-1">

      <b-card-body class="pb-1">

        <b-row>
          <!-- 1 COL ++++++++++++++++++++++++++++++++ -->
          <b-col cols="12" class="mb-2" >
            <h4 class="mb-0">
              Custom Group Configuration
            </h4>
            <small class="text-muted">
              In this section, you can disable groups of variables in the pipeline forms.
            </small>
          </b-col>

          <!-- 2 COL ++++++++++++++++++++++++++++++++ -->
          <b-col md="2" cols="12">
              <!-- If input type is a checkbox_group, this is implemented -------------------------------------- -->
              <b-form-group>
                <b-form-checkbox>
                  Schedule Pipeline
                </b-form-checkbox>
              </b-form-group>
          </b-col>
        </b-row>

      </b-card-body>

    </b-card>

  <form-wizard
      color="#0473bc"
      title=""
      subtitle=""
      finish-button-text="Launch Pipeline"
      back-button-text="Previous"
      class="steps-transparent mb-3"
      @on-complete="launchPipelineButton"
  >

    <!-- account details tab -->
    <tab-content
        v-for="(group, group_key, _index) in contentDataModel"
        v-if="!gruposDisabled.includes(group_key)"
        :title="group.title"
        :icon="'feather ' + group.icon"
        v-bind:key="group_key"
    >
      <b-row>

        <!-- 1 COL ++++++++++++++++++++++++++++++++ -->
        <b-col cols="12" class="mb-2" >
          <h4 class="mb-0">
            Custom Group Configuration
          </h4>
          <small class="text-muted">
            In this section, you can disable groups of variables in the pipeline forms.
          </small>
        </b-col>

        <!-- 2 COL ++++++++++++++++++++++++++++++++ -->
        <b-col md="2" cols="12"
               v-for="(group, group_key, _index) in contentDataModel"
               v-bind:key="group_key"
        >
            <!-- If input type is a checkbox_group, this is implemented -------------------------------------- -->
            <b-form-group
                :label-for="group_key"
            >
              <b-form-checkbox
                  v-model="checkboxValues[group_key]"
                  :required="true"
                  @change="handleCheckboxGroupChange(group_key, checkboxValues[group_key])"
                >
                {{ checkboxValues[group_key] ? 'Disable' : 'Enable' }} {{ contentDataModel[group_key].title }}
              </b-form-checkbox>
            </b-form-group>
        </b-col>

        <!-- SEPARATION LINE -->
        <b-col md="12" cols="12">
          <hr>
        </b-col>
        <!-- END SEPARATION LINE -->

        <!-- TITLE AND SUBTITLE LINE FOR GROUPS -->
        <!-- 3 COL ++++++++++++++++++++++++++++++++ -->
        <b-col cols="12" class="mb-2" >
          <h4 class="mb-0">
            {{ group.cardTitle }}
          </h4>
          <small class="text-muted">
            {{ group.cardSubtitle }}
          </small>
        </b-col>
        <!-- END TITLE AND SUBTITLE LINE FOR GROUPS -->

        <b-col md="12" cols="12">
          <!-- Agrega el componente de alerta para mostrar errores -->
          <b-alert
              v-if="errorMessage"
              variant="danger"
              dismissible
              show
              @dismissed="errorMessage = ''">
            <div class="alert-body">
              <feather-icon icon="InfoIcon" class="mr-50" />
              <span> <strong>ERROR!</strong> {{ errorMessage }} </span>
            </div>
          </b-alert>

        </b-col>

        <b-col md="6" cols="12" v-for="(content, content_key, _index) in group.content" v-bind:key="content_key">

          <!-- Check if input type is a valid type ---------------------------------------------- -->
          <b-alert
              v-if="!validInputValues.includes(content.type.toLowerCase())"
              variant="danger"
              show
          >
            <div class="alert-body">
              <feather-icon
                  icon="InfoIcon"
                  class="mr-50"
              />
              <span>
                  <strong>ERROR!</strong> Wrong format for {{ content.id }} variable. {{ content.type }} is not valid,
                  it must be one of {{ validInputValues }}.
                </span>
            </div>
          </b-alert>

          <!-- If input type is a selector, this is implemented ---------------------------------------------- -->
          <b-form-group
              v-else-if="content.type.toLowerCase()==='selector'"
              :label="content.label"
              :label-for="content.id"
          >
            <v-select
                :options="content.options"
                v-model="envVars[group_key][content_key]"
                :value-field="'value'"
                :label-field="'label'"
                @input="handleMarcaSeleccionadaChange"
                :required="content.required"
            />
          </b-form-group>

          <!-- If input type is a file_input, this is implemented ---------------------------------------------- -->
          <b-form-group
              v-else-if="content.type.toLowerCase()==='file'"
              :label="content.label"
              :label-for="content.id"
          >
            <b-form-file
                v-model="envVars[group_key][content_key]"
                ref="file-input"
                placeholder="Choose a file or drop it here..."
                drop-placeholder="Drop file here..."
                :required="content.required"
            />
          </b-form-group>

          <!-- If input type is a checkbox, this is implemented ---------------------------------------------- -->
          <b-form-group
              v-else-if="content.type.toLowerCase()==='checkbox'"
              :label="content.label"
              :label-for="content.id"
          >
            <b-form-checkbox
                v-model="envVars[group_key][content_key]"
                :required="content.required"
              >
              {{ content.text }}
            </b-form-checkbox>
          </b-form-group>

          <!-- If input type is a textarea, this is implemented -------------------------------------- -->
          <b-form-group
              v-else-if="content.type.toLowerCase()==='textarea'"
              :label="content.label"
              :label-for="content.id"
          >
            <b-form-textarea
              :id="content.id"
              :placeholder="content.default"
              v-model="envVars[group_key][content_key]"
              :required="content.required"
            />
          </b-form-group>

          <!-- Else simple input is implemented --------------------------------------------------------- -->
          <b-form-group
              v-else
              :label="content.label"
              :label-for="content.id"
          >
            <b-form-input
                :id="content.id"
                :placeholder="envVars[group_key][content_key]"
                :type="content.type"
                v-model="envVars[group_key][content_key]"
                :required="content.required"
            />
          </b-form-group>
        </b-col>
      </b-row>

    </tab-content>

  </form-wizard>

</template>

<script>
import { FormWizard, TabContent } from 'vue-form-wizard';
import vSelect from 'vue-select';
import 'vue-form-wizard/dist/vue-form-wizard.min.css';
import {
  BRow,
  BCol,
  BFormGroup,
  BFormInput,
  BFormFile,
  BFormTextarea,
  BFormCheckbox,
  BAlert,
} from 'bootstrap-vue';
import ToastificationContent from '@core/components/toastification/ToastificationContent';
import GitApiCall from '@/api/gitApi';

export default {
  name: 'PipelineSetupFormComponent',
  components: {
    FormWizard,
    TabContent,
    BRow,
    BCol,
    BFormGroup,
    BFormInput,
    BFormFile,
    BFormTextarea,
    BFormCheckbox,
    vSelect,
    BAlert,
    // eslint-disable-next-line vue/no-unused-components
    ToastificationContent,
  },
  props: {
    groupId: {
      type: Number,
      default: 0,
    },
    fileContentData: {
      type: Object,
    },
    branch: {
      type: String,
      default: 'main',
    },
    idProject: {
      type: Number,
      default: 0,
    },
    route: {
      type: String,
      default: 'vendors',
    },
  },
  data() {
    return {
      validInputValues: ['input', 'password', 'email', 'selector', 'file', 'checkbox', 'textarea'],
      envVars: {},
      statusButton: -1,
      contentDataModel: {},
      gruposDisabled: [],
      checkboxValues: [],
      errorMessage: '', // Inicializa la variable errorMessage aquí
    };
  },
  watch: {
    fileContentData() {
      // Como se hace en el watch solo cargamos envVars a partir del fichero via API, una sola vez.
      // Si es la primera vez que carga la web
      // Inicializamos todos los valores del formulario
      this.createDefaultValues();

      /*
      Crear una copia independiente de fileContentData para poder borrar los elementos que qerramos y
      no perder los datos
      */
      this.contentDataModel = JSON.parse(JSON.stringify(this.fileContentData));

      console.log('fileContentData -> envVars:', this.envVars);
      console.log('fileContentData -> statusButton:', this.statusButton);
    },
  },
  methods: {
    /* Esta funcion maneja solo los Checkbox de grupos, para poder cambiar el visual */
    handleCheckboxGroupChange(text, isChecked) {
      console.log('Texto:', text); // El contenido de content.text [Es el grupo que queremos desactivar]
      console.log('Estado del Checkbox:', isChecked); // true o false [Es el valor del checkbox]

      if (isChecked) {
        // Se ejecuta cuando el checkbox se deshabilita
        console.log('Checkbox disabled');

        // esto es para el v-if que quita o pone de la vista el grupo completo
        this.gruposDisabled.splice(this.gruposDisabled.indexOf(text), 1);

        // Forzamos la actualización de la vista
        this.$forceUpdate();
      } else {
        // Se ejecuta cuando el checkbox se habilita
        console.log('Checkbox enabled');

        // Esto es para el v-if que quita o pone de la vista el grupo completo
        this.gruposDisabled.push(text);

        // Forzamos la actualizacion del visual
        this.$forceUpdate();
      }

      console.log('this.envVars:', this.envVars);
      console.log('this.fileContentData:', this.fileContentData);
      console.log('this.contentDataModel:', this.contentDataModel);
    },

    // La reactividad del componente funciona si imprimimos this.envVars en un <p>, pero no directamtente en el select.
    // Por eso forzamos a que se actualice la vista con this.$forceUpdate(); Eso ocurre porque le estamos asignando un
    // valor por defecto, cuando no tiene valor por defecto si es reactivo.
    handleMarcaSeleccionadaChange() {
      console.log('this.envVars:', this.envVars);
      this.$forceUpdate();
    },
    // When push the button, launches a new pipeline
    async launchPipelineButton() {
      const gitlabInputVars = [];
      console.log('EnvVars:', this.envVars);
      console.log("launchPipelineButton this.$refs['file-input']:", this.$refs['file-input']);

      // TODO: !! is important to solve this eslint problems
      // eslint-disable-next-line no-restricted-syntax, guard-for-in
      for (const groupKey in this.envVars) {
        // Solo leemos las variables de envVars que no se han deshabilitado en gruposDisabled
        if (!this.gruposDisabled.includes(groupKey)) {
          // eslint-disable-next-line no-restricted-syntax, guard-for-in
          for (const contentKey in this.envVars[groupKey]) {
            const value = this.envVars[groupKey][contentKey];
            const { type } = this.fileContentData[groupKey].content[contentKey];

            console.log('launchPipelineButton {groupKey}:', groupKey);
            console.log('launchPipelineButton [type]:', type);
            console.log('launchPipelineButton contentKey:', contentKey);
            console.log('launchPipelineButton value:', value);
            console.log('----------------------------------:');

            if (type === 'file') {
              if (value) {
                gitlabInputVars.push({
                  key: contentKey,
                  variable_type: 'file',
                  // eslint-disable-next-line no-await-in-loop
                  value: await value.text(),
                });
              }
            } else if (type === 'selector') {
              gitlabInputVars.push({
                key: contentKey,
                value: value.value,
              });
            } else {
              gitlabInputVars.push({
                key: contentKey,
                value,
              });
            }
          }
        }
      }
      console.log('launchPipelineButton this.idProject:', this.idProject);
      const git = new GitApiCall();
      try {
        const response = await git.launchPipeline(this.idProject, gitlabInputVars, this.branch);
        const launchPipeline = response.data;
        console.log('launchPipelineButton launchPipeline: ', launchPipeline);
        const route = {
          name: `${this.route}-pipeline-execution`,
          // TODO: this.groupId is undefine.
          params: { id: this.groupId, project_id: this.idProject, pipeline_id: launchPipeline.id },
        };
        console.log('launchPipelineButton route: ', route);
        this.$router.push(route);
      } catch (error) {
        console.error('ERROR launchPipelineButton', error);
        // Mostrar el mensaje de error en la vista
        this.errorMessage = `An error occurred while launching the pipeline. ${error}`;
      }
      // change the status of the button
      this.statusButton = this.idProject;
    },
    // Create default values
    createDefaultValues() {
      Object.keys(this.fileContentData).forEach((group) => {
        this.envVars[group] = {};
        this.checkboxValues[group] = true; // esto es para los checkboxes de grupo
        Object.keys(this.fileContentData[group].content).forEach((key) => {
          this.envVars[group][key] = this.fileContentData[group].content[key].default;
        });
      });

      console.log('createDefaultValues this.EnvVars: ', this.envVars);
    },
  },
};
</script>

<style lang="scss">
@import '@core/scss/vue/libs/vue-wizard.scss';
@import '@core/scss/vue/libs/vue-select.scss';
</style>

-------------------------------------------

contentDataModel es la variable que carga el fichero main.x.json cuando viene de gitlab
a partir de esta variable se rellena envVars que sera la variable v-model de los formularios.
esta variable es un poco compleja porque no es ni de tipo lista, ni string, es un Objeto con los
grupos, y dentro de cada grupo variables inputs con todos sus datos; por eso no podemos hacer un
watch o watch profundo.

Un posible trabajo es simplificar esta variable.

```json
{
  "hide_on": {
    "equals": {
      "PIPELINE_TYPE": "rollback",
      "DEPLOY_VAR": true
    },
    "not_equals": {
      "VM_NUM": "1",
      "OTHER_KEY": "some_value"
    }
  }
}
```

## Distintos tipos de problemas:
Sentido comun:
Puede que alguien defina por ejemplo un diccionario donde alguna de las llaves KEY que dependa no exista:
```json
{
  "hide_on": {
    "equals": {
      "PIPELINE_TYPE": "rollback"
    }
  }
}
```
Aqui es mandatorio que <code>PIPELINE_TYPE</code> exista para que podamos ocultarlo correctamente.
En caso contrario y se referencie una <code>KEY</code> que no exista el formulario mostrara un error, y tendra que corregirse
