import type { AxiosRequestConfig, AxiosResponse } from 'axios'
import axios from 'axios'
import store from '@/store'

export default class GitApiCall {
  private headers: Record<string, string>
  private uri: string
  private vendorId: string
  private uiBackendProject: string

  constructor() {
    const gitlabToken = store.getters.gitlabToken

    this.headers = {
      Authorization: `Bearer ${gitlabToken}`,
    }
    this.uri = `${window.GITLAB_URI}/api/v4` // This is only for GitLab v4 API
    this.vendorId = window.GITLAB_VENDOR_ID
    this.uiBackendProject = window.UI_BACKEND_PROJECT
  }

  /**
   * Get the boundary for a FormData object
   * @returns {string}
   */
  private formDataBoundary(): string {
    return `----FormBoundary${Math.random().toString(36).substring(2)}`
  }

  /**
   * Get the body of a FormData object
   * @param formData
   * @param boundary
   * @returns {module:buffer.Blob}
   */
  private getFormDataBody(formData: FormData, boundary: string): Blob {
    // Step 2: Create a new Blob object from the FormData object's entries iterator.
    const formDataEntries = Array.from(formData.entries())
    const formDataBodyParts: string[] = []

    // Step 3: Convert each entry in the array to a string, and join them together with the appropriate multipart boundary and content disposition formatting.
    formDataEntries.forEach(([key, value]) => {
      const part = `--${boundary}\r\nContent-Disposition: form-data; name="${key}"\r\n\r\n${value}\r\n`

      formDataBodyParts.push(part)
    })

    // Step 4: Add the final boundary to the end of the string.
    formDataBodyParts.push(`--${boundary}--`)

    // Step 5: Convert the string to a Blob object, and set the Content-Type header to multipart/form-data with the appropriate boundary.
    return new Blob(formDataBodyParts, { type: `multipart/form-data; boundary=${boundary}` })
  }

  async downloadArtifacts(projectId: string, jobId: string) {
    try {
      const config: AxiosRequestConfig = {
        method: 'get',
        responseType: 'blob',
        url: `${this.uri}/projects/${projectId}/jobs/${jobId}/artifacts`,
        headers: this.headers,
      }

      console.log('downloadArtifacts: ', config)

      return await axios(config)
    }
    catch (error) {
      console.error('Failed to download artifacts:', error)
      throw error // Rethrow or handle error as needed
    }
  }

  async retryJob(projectId: string, jobId: string) {
    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${this.uri}/projects/${projectId}/jobs/${jobId}/retry`,
      headers: this.headers,
    }

    console.log('retryJob: ', config)

    return axios(config)
  }

  async getGroup(id: string) {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/groups/${id}`,
      headers: this.headers,
    }

    console.log('getGroup: ', config)

    return axios(config)
  }

  // Obtener grupos con opción para filtrar solo los grupos de nivel superior
  async getGroups(topLevelOnly: boolean = true): Promise<AxiosResponse> {
    const params = `?top_level_only=${topLevelOnly}&order_by=path`

    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/groups${params}`,
      headers: this.headers,
    }

    console.log('getGroups config gitApi: ', config)

    return axios(config)
  }

  async getSingleProject(id: string): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/projects/${id}`,
      headers: this.headers,
    }

    console.log('getSingleProject: ', config)

    return axios(config)
  }

  async getProjects(perPage: number = 20, currentPage: number = 1, sortBy: string = 'path', sort: string = 'asc'): Promise<AxiosResponse> {
    const params = `?owned=true&per_page=${perPage}&page=${currentPage}&order_by=${sortBy}&sort=${sort}`

    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/projects/${params}`,
      headers: this.headers,
    }

    console.log('getProjects config gitApi: ', config)

    return axios(config)
  }

  async editProjectLink(id: string, branch: string) {
    const link = ref('')
    const project = ref<any>({})
    try {
      const response = await this.getSingleProject(id)

      project.value = response.data
      console.log('editProjectLink project.value:', project)
      console.log('editProjectLink project.path_with_namespace:', project.value.path_with_namespace)
      link.value = `${window.GITLAB_URI}/-/ide/project/${project.value.path_with_namespace}/edit/${branch}/-/`
    }
    catch (error) {
      console.log('ERROR: [editProjectLink] : ', error)
      link.value = ''
    }

    return link.value
  }

  /**
   * Retrieves a list of pipelines for a specified project.
   * @param projectId The ID of the project.
   * @param perPage The number of results per page (default is 20).
   * @param currentPage The current page number (default is 1).
   * @param sortBy The field to sort by (default is 'id').
   * @param sort The sort direction ('asc' or 'desc', default is 'desc').
   * @returns A Promise that resolves to the Axios response containing the list of pipelines.
   */
  /*  async getPipelinesList(
    projectId: string,
    perPage: number = 20,
    currentPage: number = 1,
    sortBy: string = 'id',
    sort: string = 'desc',
  ): Promise<AxiosResponse> {
    const params = `?per_page=${perPage}&page=${currentPage}&order_by=${sortBy}&sort=${sort}`

    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/projects/${projectId}/pipelines/${params}`,
      headers: this.headers,
    }

    console.log('getPipelinesList: ', config)

    return axios(config)
  } */
  async getPipelinesList(
    projectId: string,
    perPage: number = 20,
    currentPage: number = 1,
    sortBy: string = 'id',
    sort: string = 'desc',
  ): Promise<AxiosResponse | void> {
    if (!projectId) {
      console.warn('Warning: projectId is null or undefined.')

      return
    }

    const params = `?per_page=${perPage}&page=${currentPage}&order_by=${sortBy}&sort=${sort}`

    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/projects/${projectId}/pipelines/${params}`,
      headers: this.headers,
    }

    console.log('getPipelinesList: ', config)

    return axios(config)
  }

  /**
   * Retrieves a specific pipeline for a given project and pipeline ID.
   * @param projectId The ID of the project.
   * @param pipelineId The ID of the pipeline.
   * @returns A Promise that resolves to the Axios response containing the pipeline details.
   */
  async getPipeline(
    projectId: string,
    pipelineId: string,
  ): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/projects/${projectId}/pipelines/${pipelineId}`,
      headers: this.headers,
    }

    console.log('getPipeline: ', config)

    return axios(config)
  }

  /**
   * Retrieves the most recent pipeline for a given project.
   * @param projectId The ID of the project.
   * @returns A Promise that resolves to the Axios response containing the details of the last pipeline.
   */
  async getLastPipeline(
    projectId: string,
  ): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/projects/${projectId}/pipelines/?sort=desc&per_page=1`,
      headers: this.headers,
    }

    console.log('getLastPipeline: fetching latest pipeline ID', config)

    return axios(config)
  }

  /**
   * Retrieves a list of jobs for a given project and pipeline.
   * @param idProject The ID of the project.
   * @param idPipeline The ID of the pipeline.
   * @returns A Promise that resolves to the Axios response containing the list of jobs.
   */
  async getJobsList(
    idProject: string,
    idPipeline: string,
  ): Promise<AxiosResponse> {
    const params = {
      per_page: 100,
      include_retried: false,
    }

    // Constructing the parameter string from the params object
    const paramString = Object.entries(params)
      .map(([key, value]) => `${key}=${value}`)
      .join('&')

    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/projects/${idProject}/pipelines/${idPipeline}/jobs?${paramString}`,
      headers: this.headers,
    }

    console.log('getJobsList: ', config)

    return axios(config)
  }

  /**
   * Retrieves the content of a file from a project repository.
   * @param idProject The ID of the project.
   * @param filePath The path of the file in the repository.
   * @returns A Promise that resolves to the Axios response containing the file content.
   */
  async getFileContent(
    idProject: string,
    filePath: string,
  ): Promise<AxiosResponse> {
    // Enhancing the headers for this specific request
    const headers = {
      ...this.headers,
      'Content-Type': 'application/json', // Ensuring JSON content type for this request
    }

    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/projects/${idProject}/repository/files/${filePath}`,
      headers,
    }

    console.log('getFileContent: ', config)

    return axios(config)
  }

  /**
   * Triggers a pipeline manually and passes environment variables.
   * @param idProject The ID of the project.
   * @param envVars An array of environment variables to be passed.
   * @param branch The branch on which the pipeline is to be launched.
   * @returns A Promise that resolves to the Axios response.
   */
  async launchPipeline(
    idProject: string,
    envVars: EnvVar[],
    branch: string,
  ): Promise<AxiosResponse> {
    console.info('INFO: launchPipeline: ', idProject, branch)
    console.debug('DEBUG: envVars:  ', envVars)

    const data = JSON.stringify({
      ref: branch,
      variables: envVars,
    })

    const headers = {
      ...this.headers,
      'Content-Type': 'application/json', // Ensuring JSON content type for this request
    }

    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${this.uri}/projects/${idProject}/pipeline/`,
      headers,
      data,
    }

    console.log('gitApiCall launchPipeline config: ', config)

    return axios(config)
  }

  /**
   * Retrieves all schedules for a given project.
   * @param idProject The ID of the project.
   * @param perPage Number of schedules per page (default is 20).
   * @param currentPage Current page number for pagination (default is 1).
   * @returns A Promise that resolves to the Axios response containing the list of schedules.
   */
  async getSchedulesList(
    idProject: string,
    perPage: number = 20,
    currentPage: number = 1,
  ): Promise<AxiosResponse> {
    console.info('INFO: getSchedulesList: ', idProject)

    const params = `?per_page=${perPage}&page=${currentPage}`

    const headers = {
      ...this.headers,
      'Content-Type': 'application/json',
    }

    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/projects/${idProject}/pipeline_schedules${params}`,
      headers,
    }

    console.debug('DEBUG: getSchedulesList config: ', config)

    return axios(config)
  }

  /**
   * Creates a new schedule for a pipeline in a project.
   * @param idProject The ID of the project.
   * @param branch The branch for which the schedule is created.
   * @param cron The cron expression for the schedule.
   * @param description The description of the schedule.
   * @param active Whether the schedule is active or not.
   * @returns A Promise that resolves to the Axios response.
   */
  async schedulePipeline(
    idProject: string,
    branch: string,
    cron: string,
    description: string,
    active: boolean,
  ): Promise<AxiosResponse> {
    console.info('INFO: schedulePipeline: ', idProject, branch, description, active)
    console.debug(`DEBUG: cron: '${cron}'`)

    const formData = new FormData()

    formData.append('description', description)
    formData.append('ref', branch)
    formData.append('cron', cron)
    formData.append('cron_timezone', 'UTC')
    formData.append('active', active.toString())

    const boundary = this.formDataBoundary()

    const config: AxiosRequestConfig = {
      method: 'post',
      maxBodyLength: Number.POSITIVE_INFINITY,
      url: `${this.uri}/projects/${idProject}/pipeline_schedules/`,
      headers: {
        ...this.headers,
        'Content-Type': `multipart/form-data; boundary=${boundary}`,
      },
      data: this.getFormDataBody(formData, boundary),
    }

    console.debug('DEBUG: schedulePipeline config: ', config)

    return axios(config)
  }

  /**
   * Adds or updates a variable for a pipeline schedule.
   * @param idProject The ID of the project.
   * @param idSchedule The ID of the pipeline schedule.
   * @param key The key of the variable.
   * @param value The value of the variable.
   * @param variable_type The type of the variable (optional).
   * @returns A Promise that resolves to the Axios response.
   */
  async addVariableToSchedule(
    idProject: string,
    idSchedule: string,
    key: string,
    value: string,
    variable_type?: string,
  ): Promise<AxiosResponse> {
    console.info('INFO: addVariableToSchedule: ', idProject, idSchedule, key, variable_type)

    const formData = new FormData()

    formData.append('key', key)
    formData.append('value', value)
    if (variable_type)
      formData.append('variable_type', variable_type)

    const boundary = this.formDataBoundary()

    const config: AxiosRequestConfig = {
      method: 'post',
      maxBodyLength: Number.POSITIVE_INFINITY,
      url: `${this.uri}/projects/${idProject}/pipeline_schedules/${idSchedule}/variables`,
      headers: {
        ...this.headers,
        'Content-Type': `multipart/form-data; boundary=${boundary}`,
      },
      data: this.getFormDataBody(formData, boundary),
    }

    console.debug('DEBUG: addVariableToSchedule config: ', config)

    return axios(config)
  }

  /**
   * Deletes a scheduled pipeline from a project.
   * @param idProject The ID of the project.
   * @param idSchedule The ID of the scheduled pipeline to delete.
   * @returns A Promise that resolves to the Axios response.
   */
  async deleteSchedulePipeline(
    idProject: string,
    idSchedule: string,
  ): Promise<AxiosResponse> {
    console.info('INFO: deleteSchedulePipeline: ', idProject, idSchedule)

    const headers = {
      ...this.headers,
      'Content-Type': 'application/json', // Ensures JSON content type for DELETE request
    }

    const config: AxiosRequestConfig = {
      method: 'delete',
      url: `${this.uri}/projects/${idProject}/pipeline_schedules/${idSchedule}`,
      headers,
    }

    console.debug('DEBUG: deleteSchedulePipeline config: ', config)

    return axios(config)
  }

  /**
   * Triggers a job manually within a project, with the option to pass environment variables.
   * @param idProject The ID of the project.
   * @param idJob The ID of the job to play.
   * @param envVars Environment variables to pass to the job.
   * @returns A Promise that resolves to the Axios response.
   */
  async playJob(
    idProject: string,
    idJob: string,
    envVars: JobVariable[],
  ): Promise<AxiosResponse> {
    const data = JSON.stringify({
      job_variables_attributes: envVars,
    })

    const headers = {
      ...this.headers,
      'Content-Type': 'application/json',
    }

    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${this.uri}/projects/${idProject}/jobs/${idJob}/play`,
      headers,
      data,
    }

    console.log('play manual job: ', config)

    return axios(config)
  }

  /**
   * Cancels a pipeline for a specific project.
   * @param idProject The ID of the project.
   * @param idPipeline The ID of the pipeline to cancel.
   * @returns A Promise that resolves to the Axios response.
   */
  async cancelPipeline(
    idProject: string,
    idPipeline: string,
  ): Promise<AxiosResponse> {
    const data = JSON.stringify({
      ref: 'main', // Assuming 'main' is a required field for cancellation.
    })

    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${this.uri}/projects/${idProject}/pipelines/${idPipeline}/cancel`,
      headers: this.headers,
      data,
    }

    console.log('cancel pipeline git: ', config)

    return axios(config)
  }

  /**
   * Creates a test case by posting to a project's endpoint.
   * @param nameProject The name of the project.
   * @param pathProject The path of the project.
   * @returns A Promise that resolves to the Axios response.
   */
  async createTestCase(
    nameProject: string,
    pathProject: string,
  ): Promise<AxiosResponse> {
    const formData = new FormData()

    formData.append('name', nameProject)
    formData.append('path', pathProject)

    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${this.uri}/projects`,
      headers: {
        ...this.headers,
        'Content-Type': 'multipart/form-data', // This header will be set automatically by browsers if FormData is used
      },
      data: formData,
    }

    console.log('create test case: ', config)

    return axios(config)
  }

  /**
   * Retrieves a job from a project.
   * @param idProject The ID of the project.
   * @param idJob The ID of the job.
   * @returns A Promise that resolves to the Axios response.
   */
  async getJob(
    idProject: string,
    idJob: string,
  ): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/projects/${idProject}/jobs/${idJob}`,
      headers: this.headers,
    }

    console.log('getJob git: ', config)

    return axios(config)
  }

  /**
   * Cancels a job within a project.
   * @param idProject The ID of the project.
   * @param idJob The ID of the job.
   * @returns A Promise that resolves to the Axios response.
   */
  async cancelJob(
    idProject: string,
    idJob: string,
  ): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${this.uri}/projects/${idProject}/jobs/${idJob}/cancel`,
      headers: this.headers,
    }

    console.log('cancelJob: ', config)

    return axios(config)
  }

  async getLog(idProject: string, idJob: string): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/projects/${idProject}/jobs/${idJob}/trace`,
      headers: this.headers,
    }

    console.log('getLog git: ', config)

    return axios(config)
  }

  async getSubGroup(idGroup: string): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/groups/${idGroup}/subgroups`,
      headers: this.headers,
    }

    console.log('getSubGroup git: ', config)

    return axios(config)
  }

  async getProjectsFromGroup(idProject: string): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/groups/${idProject}/projects`,
      headers: this.headers,
    }

    console.info('INFO: getProjectsFromGroup: ', idProject)
    console.log('getProjectsFromGroup git: ', config)

    return axios(config)
  }

  async getBranches(idProject: string): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/projects/${idProject}/repository/branches`,
      headers: this.headers,
    }

    console.log('getBranches git: ', config)

    return axios(config)
  }

  /**
   * Retrieves a user by their username.
   * @param username The username of the user to retrieve.
   * @returns A Promise that resolves to the Axios response.
   */
  async getUserByUserName(username: string): Promise<AxiosResponse> {
    console.info(`INFO: getUserByUserName with username: ${username}`)

    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/users?username=${encodeURIComponent(username)}`, // Properly encoding the username to handle special characters
      headers: this.headers,
    }

    console.log('getUserByUserName git: ', config)

    return axios(config)
  }
}
