import axios from 'axios'
import type { AxiosRequestConfig, AxiosResponse } from 'axios'

class SettingsApi {
  // Declare headers for API calls 📡
  private headers: Record<string, string>

  // Base URI for settings API
  private uri: string

  constructor() {
    // Set common headers, mainly for content type
    this.headers = {
      'Content-Type': 'application/json',
    }

    // Construct server URL from window location 🌍
    const protocol = window.location.protocol
    const hostname = window.location.hostname
    const port = window.location.port

    // Handle port in the URL properly: only add if it exists 🔍
    const serverUrl = `${protocol}//${hostname}${port ? `:${port}` : ''}`

    // Set the base URI for UI settings
    this.uri = `${serverUrl}/ui-settings`
  }

  /**
   * Fetch a JSON file from the server settings directory 📂
   * @param source - The name of the JSON file to fetch without extension
   * @returns A Promise resolving to the Axios response containing the JSON file
   */
  async getJsonFile(source: string): Promise<AxiosResponse> {
    console.info('SettingsApi.getJsonFile source:', source) // Info log for debugging 🛠️

    const config: AxiosRequestConfig = {
      method: 'get', // HTTP method
      url: `${this.uri}/${source}.json`, // Construct URL to the JSON file
      headers: this.headers, // Use predefined headers
    }

    console.debug('SettingsApi.getJsonFile config:', config) // Debug log with full Axios config

    return axios(config) // Execute the HTTP request using Axios
  }
}

export default SettingsApi
