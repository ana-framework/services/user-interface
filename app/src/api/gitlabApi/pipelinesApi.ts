import type { AxiosRequestConfig, AxiosResponse } from 'axios'
import axios from 'axios'
import store from '@/store'

export interface Pipeline {
  id: number
  status: string
  ref: string
  web_url: string
  created_at: string
  updated_at: string
}

export default class PipelinesApi {
  private uri: string
  private headers: Record<string, string>
  private vendorId: number

  constructor() {
    this.uri = `${window.GITLAB_URI}/api/v4`
    this.headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${store.getters.gitlabToken}`,
    }
    this.vendorId = Number(window.GITLAB_VENDOR_ID)
  }

  async getPipelines(
    projectId: string,
    perPage: number = 20,
    currentPage: number = 1,
    sortBy: string = 'id',
    sort: string = 'desc',
  ): Promise<AxiosResponse> {
    const params = `?per_page=${perPage}&page=${currentPage}&order_by=${sortBy}&sort=${sort}`

    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/projects/${projectId}/pipelines/${params}`,
      headers: this.headers,
    }

    console.log('getPipelinesList: ', config)

    return axios(config)
  }
}
