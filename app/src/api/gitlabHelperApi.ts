import store from '@/store'
import type { AxiosRequestConfig, AxiosResponse } from 'axios'
import axios from 'axios'

interface GitLabTokenData {
  gitlab_admin_token: string
  email: string
  full_name: string
  username: string
  user_id: string
  password: string
  admin: boolean
}

class GitlabHelperApi {
  private gitlabToken: string
  private headers: Record<string, string>
  private uri: string

  constructor() {
    this.gitlabToken = store.getters.gitlabToken

    const accessToken = store.getters.token

    this.headers = {
      'Authorization': `Bearer ${accessToken}`,
      'X-Auth-Token': `Bearer ${this.gitlabToken}`,
      'Content-Type': 'application/json',
    }
    this.uri = `${window.BACKEND_URI}/api/v1${window.GITLAB_END_POINT}`
  }

  /**
   * Create a GitLab token for a user 🛠️
   * @param email - User's email address
   * @param fullName - Full name of the user
   * @param username - Username for the GitLab account
   * @param userId - User ID in the system
   * @param password - Password for the GitLab account
   * @param admin - Whether the user should have admin rights (default: false)
   * @returns A Promise resolving to the Axios response
   */
  async createGitLabToken(email: string, fullName: string, username: string, userId: number, password: string, admin = false): Promise<AxiosResponse> {
    console.info('GitLabApi.createGitLabToken', { email, fullName, username, userId, admin }) // Info log for input parameters
    const data: GitLabTokenData = {
      gitlab_admin_token: this.gitlabToken, // Replace with actual admin token
      email,
      full_name: fullName,
      username,
      user_id: userId.toString(),
      password,
      admin,
    }
    const config: AxiosRequestConfig = {
      
      method: 'post',
      url: `${this.uri}/users/token`,
      headers: this.headers,
      data,
      
    }
    console.debug('GitLabApi.createGitLabToken config:', config) // Debug log with the Axios configuration
    // return axios(config) // Execute the HTTP request using Axios and return the promise
    try {
      const response = await axios(config) // Execute the HTTP request using Axios and return the promise
      if (response && response.data) {
        return response
      } else {
        throw new Error('Response data is undefined')
      }
    } catch (error) {
      console.error('Error in createGitLabToken:', error)
      throw error
    }
  };

  async renewGitLabToken(tokenId, userId, gitlabUserId, gitUsername): Promise<AxiosResponse> {
    const data = {
      token_id: tokenId,
      user_id: userId,
      gitlab_id: gitlabUserId,
      username: gitUsername,
    }

    const config: AxiosRequestConfig = {
      
      method: 'post',
      url: `${this.uri}/users/${userId}/token/${tokenId}/rotate`,
      headers: this.headers,
      data,
      
    }

    console.log('renewGroupToken: ', config)

    return axios(config)
  };

  async deleteGitLabToken(tokenId: string, userId: string): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'delete',
      url: `${this.uri}/users/${userId}/token/${tokenId}`,
      headers: this.headers,
    }

    console.log('deleteGitLabToken: ', config)

    return axios(config)
  }

  async getStagesList(projectId: string, pipelineId: string, selectedJobStatus: string[]): Promise<AxiosResponse> {
    const paramStr = selectedJobStatus.length > 0 ? `?${selectedJobStatus.map(stage => `status[]=${stage}`).join('&')}` : ''
    const url = `${this.uri}/projects/${projectId}/pipelines/${pipelineId}/stages${paramStr}`

    const config: AxiosRequestConfig = {
      method: 'get',
      url,
      headers: this.headers,
    }

    console.log('getStagesList: ', config)

    return axios(config)
  }

  async getJobsList(projectId: string, pipelineId: string, selectedJobStatus: string[]): Promise<AxiosResponse> {
    const paramStr = selectedJobStatus.length > 0 ? `?${selectedJobStatus.map(stage => `status[]=${stage}`).join('&')}` : ''
    const url = `${this.uri}/projects/${projectId}/pipelines/${pipelineId}/jobs${paramStr}`

    const config: AxiosRequestConfig = {
      method: 'get',
      url,
      headers: this.headers,
    }

    console.log('getJobsList: ', config)

    return axios(config)
  }

  async createGroupToken(groupId, userId): Promise<AxiosResponse> {
    const data = {
      user_id: userId,
    }

    const config = {
      method: 'post',
      url: `${this.uri}/groups/${groupId}/access_tokens`,
      headers: this.headers,
      data,
    }

    console.log('createProjectToken: ', config)

    return axios(config)
  }

  async renewGroupToken(groupId, tokenId, userId): Promise<AxiosResponse> {
    const data = {
      user_id: userId,
    }

    const config = {
      method: 'post',
      url: `${this.uri}/groups/${groupId}/access_tokens/${tokenId}/rotate`,
      headers: this.headers,
      data,
    }

    console.log('renewGroupToken: ', config)

    return axios(config)
  }

  async deleteGroupToken(groupId, tokenId, userId): Promise<AxiosResponse> {
    const data = {
      user_id: userId,
    }

    const config = {
      method: 'delete',
      url: `${this.uri}/groups/${groupId}/access_tokens/${tokenId}`,
      headers: this.headers,
      data,
    }

    console.log('deleteGroupToken: ', config)

    return axios(config)
  }

  async createUserAccessToken(userId, gitLabId, gitlabUsername): Promise<AxiosResponse> {
    const data = {
      gitlab_id: gitLabId,
      username: gitlabUsername,
    }

    const config = {
      method: 'post',
      url: `${this.uri}/users/${userId}/token`,
      headers: this.headers,
      data,
    }

    console.log('createUserAccessToken: ', config)

    return axios(config)
  }
}

export default GitlabHelperApi
