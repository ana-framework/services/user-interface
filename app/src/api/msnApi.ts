import type { AxiosRequestConfig, AxiosResponse } from 'axios'
import axios from 'axios'

declare global {
  interface Window {
    BACKEND_URI: string
    MSN_END_POINT: string
  }
}

interface UserData {
  name?: string
  userId?: string
  description?: string
}

interface ReporterParams {
  reporter?: string
  email?: string
  url?: string
}

class MsnApiV1 {
  private headers: Record<string, string>
  private uri: string

  constructor() {
    // Log the values of BACKEND_URI and MSN_END_POINT
    console.log('BACKEND_URI:', window.BACKEND_URI)
    console.log('MSN_END_POINT:', window.MSN_END_POINT)
    
    // Ensure BACKEND_URI and MSN_END_POINT are defined
    if (!window.BACKEND_URI || !window.MSN_END_POINT) {
      throw new Error('BACKEND_URI or MSN_END_POINT is not defined in the global window object');
    }

    // Read the access token from sessionStorage
    const accessToken = sessionStorage.getItem('accessToken') || '';

    // Check if the access token is empty
    if (!accessToken) {
      console.warn('Access token is not available in sessionStorage');
    }

    // Set headers
    this.headers = {
      'Authorization': `Bearer ${accessToken}`,
      'Content-Type': 'application/json',
    }

    // Set URI
    this.uri = `${window.BACKEND_URI}/api/v1${window.MSN_END_POINT}`
  }

  
  async getAllEvents(perPage: number = 20, currentPage: number = 1): Promise<AxiosResponse> {
    const params = `?per_page=${perPage}&page=${currentPage}`
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/events${params}`,
      headers: this.headers,
    }
    console.log(config)
    return axios(config)
  }
  
  async createEvent(userData: UserData): Promise<AxiosResponse> {
    const data = JSON.stringify(userData)
    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${this.uri}/events`,
      headers: this.headers,
      data,
    }
    console.log('createEvent: ', config)
    return axios(config)
  }

  async deleteEvent(eventId: string): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'delete',
      url: `${this.uri}/events/${eventId}`,
      headers: this.headers,
    }
    console.log('deleteEvent: ', config)
    return axios(config)
  }

  async getAllSubscribers(perPage: number = 20, currentPage: number = 1): Promise<AxiosResponse> {
    const params = `?per_page=${perPage}&page=${currentPage}`
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/subscribers${params}`,
      headers: this.headers,
    }
    console.log('getAllSubscribers', config)
    return axios(config)
  }


  async getSubscriberByID(userID: string | number): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/subscribers/${userID}`,
      headers: this.headers,
    }
    console.log('getSubscriberFromUserID', config)
    return axios(config)
  }
  
  async createSubscriber(name: string, userId?: string | number, description?: string): Promise<AxiosResponse> {
    const uid = userId === undefined ? null : userId.toString()
    const desc = description === undefined ? null : description
    const data = { name, user_id: uid, description: desc }
    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${this.uri}/subscribers`,
      headers: this.headers,
      data,
    }
    console.log('createSubscriber: ', config)
    return axios(config)
  }

  async deleteSubscriber(subscriberId: string | number): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'delete',
      url: `${this.uri}/subscribers/${subscriberId}`,
      headers: this.headers,
    }
    console.log('deleteSubscriber: ', config)
    return axios(config)
  }

  async getReportersBySubscriberID(subscriberID: string | number): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/subscribers/${subscriberID}/reporters-settings`,
      headers: this.headers,
    }
    console.log('getReportersBySubscriberID: ', config)
    return axios(config)
  }

  async createReporter(params: ReporterParams, subscriberId: string | number): Promise<AxiosResponse> {
    const data = JSON.stringify(params)
    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${this.uri}/subscribers/${subscriberId}/reporters-settings`,
      headers: this.headers,
      data,
    }
    console.log('createReporter: ', config)
    return axios(config)
  }

  async updateReporter(subscriberId: string | number, reporterId: string | number, params: ReporterParams): Promise<AxiosResponse> {
    const data = JSON.stringify(params)
    const config: AxiosRequestConfig = {
      method: 'put',
      url: `${this.uri}/subscribers/${subscriberId}/reporters-settings/${reporterId}`,
      headers: this.headers,
      data,
    }
    console.log('updateReporter: ', config)
    return axios(config)
  }

  async deleteReporter(subscriberId: string | number, reporterId: string | number): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'delete',
      url: `${this.uri}/subscribers/${subscriberId}/reporters-settings/${reporterId}`,
      headers: this.headers,
    }
    console.log('deleteReporter: ', config)
    return axios(config)
  }

  async getSubscriptionBySubscriber(eventId: string | number, subscriberId: string | number): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/events/${eventId}/subscriptions/${subscriberId}`,
      headers: this.headers,
    }
    console.log('getSubscriptionBySubscriber: ', config)
    return axios(config)
  }

  async createSubscription(eventId: string | number, subscriberId: string | number, reporterId: string | number): Promise<AxiosResponse> {
    const data = { reporters: [reporterId] }
    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${this.uri}/events/${eventId}/subscriptions/${subscriberId}`,
      headers: this.headers,
      data,
    }
    console.log('createSubscription: ', config)
    return axios(config)
  }

  async addReporterToSubscription(eventId: string | number, subscriberId: string | number, reporterId: string | number): Promise<AxiosResponse> {
    const data = { reporter: reporterId }
    const config: AxiosRequestConfig = {
      method: 'put',
      url: `${this.uri}/events/${eventId}/subscriptions/${subscriberId}/reporters`,
      headers: this.headers,
      data,
    }
    console.log('addReporterToSubscription: ', config)
    return axios(config)
  }

  async deleteReporterFromSubscription(eventId: string | number, subscriberId: string | number, reporterId: string | number): Promise<AxiosResponse> {
    const data = { reporter: reporterId }
    const config: AxiosRequestConfig = {
      method: 'delete',
      url: `${this.uri}/events/${eventId}/subscriptions/${subscriberId}/reporters`,
      headers: this.headers,
      data,
    }
    console.log('deleteReporterFromSubscription: ', config)
    return axios(config)
  }
}

export default MsnApiV1
