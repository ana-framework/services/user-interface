import type { AxiosRequestConfig, AxiosResponse } from 'axios';
import axios from 'axios';

interface Model {
  name: string;
  params: Record<string, any>;
  type: string;
}

interface ModelsResponse {
  models: Model[];
  status: boolean;
}

interface ContextResponse {
  actual_context: string;
  status: boolean;
}

export default class aiApiConnect {
  private uri: string;

  constructor() {
    this.uri = `${window.LOCAL_URI}`;
  }

  async getModels(): Promise<AxiosResponse<ModelsResponse>> {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/list_models`,
    };
    
    try {
      const response = await axios<ModelsResponse>(config);
      
      // Imprimir los nombres de los modelos en la consola
      response.data.models.forEach(model => {
        console.log('Nombre del modelo:', model.name);
      });

      return response;
    } catch (error) {
      console.error('Error al obtener los modelos:', error);
      throw error;
    }
  }

  async getContext(): Promise<AxiosResponse<ContextResponse>> {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/get_context`,
    };
    
    try {
      const response = await axios<ContextResponse>(config);
      
      // Imprimir el contexto actual en la consola
      console.log(':: API :: Contexto actual:', response.data.actual_context);

      return response;
    } catch (error) {
      console.error('Error al obtener el contexto:', error);
      throw error;
    }
  }

  async setContext(newContext: string): Promise<AxiosResponse<ContextResponse>> {
    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${this.uri}/set_context`,
      data: { context: newContext }
    };
    
    try {
      const response = await axios<ContextResponse>(config);
      
      // Imprimir el nuevo contexto en la consola
      console.log(':: API :: DATA:', response.data);
      // console.log(':: API :: Nuevo contexto establecido:', response.data.context);

      return response;
    } catch (error) {
      console.error('Error al establecer el contexto:', error);
      throw error;
    }
  }

  async uploadFile(file: File): Promise<AxiosResponse> {
    console.log(':: API :: Intentando subir archivo');
  
    // Crear el FormData y usar 'files' en lugar de 'file'
    const formData = new FormData();
    formData.append('files', file); // Cambia 'file' por 'files' para que coincida con Postman
    
    // Log de depuración
    console.log('Contenido de FormData:', formData.get('files')); 
    console.log('Nombre del archivo:', file.name);
    console.log('Tamaño del archivo:', file.size);
  
    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${this.uri}/upload_files`,
      data: formData,
      maxBodyLength: Infinity,

    };
  
    try {
      const response = await axios(config);
      console.log('Archivo subido exitosamente:', response.data);
      return response;
    } catch (error) {
      if (error.response) {
        console.error('Error en la respuesta de la API:', error.response.data);
      } else {
        console.error('Error al subir el archivo:', error);
      }
      throw error;
    }
  }
  
  


  // Nuevo método para listar archivos subidos
  async listUploadedFiles(): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/list_uploaded_files`,
      maxBodyLength: Infinity,
    };

    try {
      const response = await axios(config);
      console.log('Archivos subidos:', response.data);
      return response;
    } catch (error) {
      console.error('Error al listar los archivos subidos:', error);
      throw error;
    }
  }

  async aiCommunication(model: string, question: string): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${window.LOCAL_URI}/ask_question`,
      data: {
        model,
        question,
      },
    };
    console.log('Enviando pregunta:', question, 'con el modelo:', model);
    return axios(config);
  }

}
