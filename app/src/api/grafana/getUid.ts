
import axios, { AxiosHeaders } from 'axios';

export default class GrafanaApiV1 {
  private uri: string;
  private token: string;
  private configHeaders: AxiosHeaders;

  constructor() {
    this.url: 'https://grafana.demo.vmware.5glabaltran.com/api/dashboards/uid/alertmanager-overview';
    this.token = 'YWRtaW46cHJvbS1vcGVyYXRvcg=='; // Token en base64
    this.configHeaders = this.getHeaders(); // Configuración de headers
  }

  // Método privado para generar los headers
  private getHeaders() {
    return new AxiosHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Basic ${this.token}`, // Autorización con token
    });
  }

  // Método para realizar la petición
  public async fetchDashboards() {
    try {
      const response = await axios.get(this.uri, {
        headers: this.configHeaders,
        maxBodyLength: Infinity,
      });

      console.log(JSON.stringify(response.data));
      return response.data; // Retorna los datos obtenidos
    } catch (error) {
      console.error('Error al obtener los dashboards:', error);
      throw error;
    }
  }
}
