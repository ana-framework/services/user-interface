import type { AxiosRequestConfig, AxiosResponse } from 'axios'
import axios from 'axios'
import store from '@/store' // Import the store from the Vuex store

interface Health {
  failure_duration: string | null
  id: number
  last_failure: string
  last_failure_id: number
  last_success: string
  last_success_id: number
  ratio: number
  size: number
  status: string
}

interface Namespace {
  id: number
}

export interface Project {
  id: number
  name: string
  description: string
  health: Health
  namespace: Namespace
}

export default class ProjectsApi {
  private uri: string
  private headers: Record<string, string>

  constructor() {
    this.uri = `${window.BACKEND_URI}/api/v1${window.GITLAB_END_POINT}`
    this.headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${store.getters.token}`,
      'X-Auth-Token': `Bearer ${store.getters.gitlabToken}`,
    }
  }

  /**
   * Fetch all projects from GitLab
   * @returns A Promise resolving to the Axios response
   */
  async getProjects(): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/projects`,
      headers: this.headers,
    }

    console.debug('DEBUG ProjectsApi.getProjects', config)

    return axios(config)
  }

  /**
   * Fetch a project from GitLab
   * @param projectId The project ID
   * @returns A Promise resolving to the Axios response
   */
  async getProject(projectId: number): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/projects/${projectId}`,
      headers: this.headers,
    }

    console.debug('DEBUG ProjectsApi.getProject', config)

    return axios(config)
  }
}
