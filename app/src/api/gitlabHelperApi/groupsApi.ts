import type { AxiosRequestConfig, AxiosResponse } from 'axios'
import axios from 'axios'
import store from '@/store' // Import the store from the Vuex store

export interface Group {
  id: number
  name: string
  description: string
  web_url: string
}

export default class GroupsApi {
  private uri: string
  private headers: Record<string, string>

  constructor() {
    this.uri = `${window.BACKEND_URI}/api/v1${window.GITLAB_END_POINT}`
    this.headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${store.getters.token}`,
      'X-Auth-Token': `Bearer ${store.getters.gitlabToken}`,
    }
  }

  /**
   * Fetch all projects from GitLab group
   * @returns A Promise resolving to the Axios response
   */
  async getProjects(groupId: number, page: number = 1, perPage: number = 10): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/groups/${groupId}/projects?page=${page}&per_page=${perPage}`,
      headers: this.headers,
    }

    console.debug('DEBUG GroupsApi.getProjects', config)

    return axios(config)
  }
}
