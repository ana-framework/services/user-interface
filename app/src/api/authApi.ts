import UserApiV1 from '@/api/userApi'
import store from '@/store'
import jwtDecode from 'jwt-decode'

interface User {
  id: string
  username: string
  email: string
  git_lab_user: {
    id: number
    token: string
  }
}

export default class AuthApi {
  private userApi: UserApiV1

  constructor() {
    this.userApi = new UserApiV1()
  }

  async login(username: string, password: string) {
    // Clear sessionStorage in order to avoid conflicts
    sessionStorage.removeItem('user');
    
    const response = await this.userApi.getToken(username, password)
    if (response.status !== 200)
      throw new Error(response.data.message)
    
    const token = response.data.data

    console.log('LOG UserApiV1.login Token', token)
    store.commit('setToken', token)

    const payload: User = jwtDecode(token)

    const userResponse = await this.userApi.getUser(payload.id)
    const user = userResponse.data.data

    store.commit('setUser', user)
    console.log('LOG UserApiV1.login User', user)
    
    if (user.git_lab_user) {
      store.commit('setGitlabToken', user.git_lab_user.token)
    }
    else {
      console.log('LOG UserApiV1.login gitlabToken', 'null')

      // todo: send a message to the user to connect his gitlab account.
    }
  }

  async logout() {
    console.debug('DEBUG UserApiV1.logout')
    store.commit('deleteToken')
    store.commit('deleteUser')
    store.commit('deleteGitlabToken')
  }
}
