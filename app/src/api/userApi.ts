import type { AxiosRequestConfig } from 'axios'
import axios, { AxiosHeaders } from 'axios'
import store from '@/store'

interface UserData {
  id?: string
  name?: string
  email?: string
  surname_1?: string
  surname_2?: string
  role?: string
  password?: string
  old_password?: string
  new_password?: string
  username?: string
  token_id?: number
  token?: string
}

export default class UserApiV1 {
  private uri: string
  private user: any
  private userProperties: string[]

  constructor() {
    this.uri = `${window.BACKEND_URI}/api/v1${window.USER_END_POINT}`
    this.user = store.getters.user
    this.userProperties = ['email', 'name', 'surname_1', 'surname_2']
  }

  private getHeaders() {
    return new AxiosHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${store.getters.token}`,
    })
  };

  private helperUserData(userData: UserData): Partial<UserData> {
    const data: Partial<UserData> = {}
    for (const item of this.userProperties) {
      if (userData[item])
        data[item] = userData[item]
    }
    console.log('helperUserData.data: ', data)

    return data
  }

  /* This function is used for authAPI
  * */
  async getToken(username: string, password: string) {
    const data = {
      username,
      password,
    }

    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${this.uri}/login`,
      headers: {}, // Additional headers can be added here if needed
      data,
    }

    console.debug('DEBUG UserApiV1.login', config)

    return axios(config)
  }

  /* This function is used for authAPI
  * */
  async getUser(userId: string) {
    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/users/${userId}`,
      headers: this.getHeaders(),
    }

    console.debug('UserApiV1.getUser', config)

    return axios(config)
  }

  /**
   * Retrieves details for a specific user by their ID.
   * @param userId Optional. The ID of the user to retrieve. If not provided, defaults to the current user's ID.
   * @returns A Promise that resolves to the Axios response containing the user's details.
   */
  async getUserById(userId?: string): Promise<AxiosResponse> {
    // Use the provided userId or default to the current user's ID
    userId = userId ?? this.user.id

    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/users/${userId}`,
      headers: this.getHeaders(),
    }

    console.log('getUserById:', config)

    return axios(config)
  }

  /**
   * Creates a new user with the provided user data.
   * @param userData The data for the user to be created. This should include all necessary user attributes.
   * @returns A Promise that resolves to the Axios response indicating the result of the user creation process.
   */
  async createUser(userData: Record<string, any>): Promise<AxiosResponse> {
    const data = JSON.stringify(userData)

    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${this.uri}/users`,
      headers: this.getHeaders(),
      data,
    }

    console.log('createUser:', config)

    return axios(config)
  }

  /**
   * Updates an existing user with the provided data.
   * @param userData The data for updating the user, including the user's ID and any other fields that may be updated.
   * @returns A Promise that resolves to the Axios response indicating the result of the update operation.
   */
  async updateUser(userData: { id: string } & Record<string, any>): Promise<AxiosResponse> {
    const data = JSON.stringify(this.helperUserData(userData))
    const userId = userData.id

    const config: AxiosRequestConfig = {
      method: 'put',
      url: `${this.uri}/users/${userId}`,
      headers: this.getHeaders(),
      data,
    }

    console.log('updateUser:', config)

    return axios(config)
  }

  /**
   * Updates the role of a specified user.
   * @param role The new role to be assigned to the user.
   * @param userId Optional. The ID of the user whose role is to be updated. Defaults to the current user's ID if not provided.
   * @returns A Promise that resolves to the Axios response indicating the result of the role update operation.
   */
  async updateRoleUser(role: string, userId?: string): Promise<AxiosResponse> {
    userId = userId ?? this.user.id // Default to the current user's ID if userId is not provided

    const data = JSON.stringify({ role })

    const config: AxiosRequestConfig = {
      method: 'put',
      url: `${this.uri}/users/${userId}/role`,
      headers: this.getHeaders(),
      data,
    }

    console.log('updateRoleUser:', config)

    return axios(config)
  }

  /**
   * Updates or sets the password for a specified user.
   * @param password The new password to be set for the user.
   * @param userId Optional. The ID of the user whose password is being set. Defaults to the current user's ID if not provided.
   * @returns A Promise that resolves to the Axios response indicating the result of the password update operation.
   */
  async createUserPassword(password: string, userId?: string): Promise<AxiosResponse> {
    // Default to the current user's ID if userId is not provided
    userId = userId ?? this.user.id

    const data = JSON.stringify({ password })

    const config: AxiosRequestConfig = {
      method: 'put',
      url: `${this.uri}/users/${userId}/password`,
      headers: this.getHeaders(),
      data,
    }

    console.log('createUserPassword:', config)

    return axios(config)
  }

  /**
   * Changes the password for a specified user.
   * @param oldPassword The current password of the user.
   * @param newPassword The new password to be set for the user.
   * @param userId Optional. The ID of the user whose password is being changed. Defaults to the current user's ID if not provided.
   * @returns A Promise that resolves to the Axios response indicating the result of the password change operation.
   */
  async changePassword(
    oldPassword: string = '',
    newPassword: string,
    userId?: string,
  ): Promise<AxiosResponse> {
    // Default to the current user's ID if userId is not provided
    userId = userId ?? this.user.id

    const data = JSON.stringify({
      old_password: oldPassword,
      new_password: newPassword,
    })

    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${this.uri}/users/${userId}/password/change`,
      headers: this.getHeaders(),
      data,
    }

    console.log('changePassword:', config)

    return axios(config)
  }

  /**
   * Retrieves a list of users with pagination.
   * @param perPage The number of users to return per page.
   * @param page The page number of the user listing.
   * @returns A Promise that resolves to the Axios response containing the list of users.
   */
  async listUsers(perPage: number, page: number): Promise<AxiosResponse> {
    // Build the query parameters string
    const params = `?per_page=${perPage}&page=${page}`

    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/users${params}`,
      headers: this.getHeaders(),
    }

    console.log('listUsers:', config)

    return axios(config)
  }

  /**
   * Deletes a user from the system based on the provided user ID.
   * @param userId The unique identifier of the user to be deleted.
   * @returns A Promise that resolves to the Axios response indicating the result of the delete operation.
   */
  async deleteUser(userId: string): Promise<AxiosResponse> {
    const config: AxiosRequestConfig = {
      method: 'delete',
      url: `${this.uri}/users/${userId}`,
      headers: this.getHeaders(),
    }

    console.log('deleteUser:', config)

    return axios(config)
  }

  /**
   * Retrieves GitLab user information by user ID.
   * @param idUser Optional. The ID of the user whose GitLab information is being requested. Defaults to the current user's ID if not provided.
   * @returns A Promise that resolves to the Axios response containing the GitLab user's details.
   */
  async getGitUserById(idUser?: string): Promise<AxiosResponse> {
    // Default to the current user's ID if idUser is not provided
    idUser = idUser ?? this.user.id

    const config: AxiosRequestConfig = {
      method: 'get',
      url: `${this.uri}/users/${idUser}/gitlab`,
      headers: this.getHeaders(),
    }

    console.log('getGitUserById:', config)

    return axios(config)
  }

  /**
   * Creates a GitLab user associated with the specified system user ID.
   * @param idUser Optional. The ID of the system user to associate with the GitLab user. Defaults to the current user's ID if not provided.
   * @param username The GitLab username to create.
   * @param token The access token for the GitLab account.
   * @returns A Promise that resolves to the Axios response indicating the result of the GitLab user creation operation.
   */
  async createGitUser(idUser?: string, username: string, token: string): Promise<AxiosResponse> {
    // Default to the current user's ID if idUser is not provided
    idUser = idUser ?? this.user.id

    const data = {
      username,
      token,
    }

    const config: AxiosRequestConfig = {
      method: 'post',
      url: `${this.uri}/users/${idUser}/gitlab`,
      headers: this.getHeaders(),
      data,
    }

    console.log('createGitUser:', config)

    return axios(config)
  }

  /**
   * Updates the token for a GitLab user associated with a given system user ID.
   * @param idUser Optional. The ID of the system user associated with the GitLab account. Defaults to the current user's ID if not provided.
   * @param tokenId The ID of the token to update.
   * @param token The new token value.
   * @returns A Promise that resolves to the Axios response indicating the result of the token update operation.
   */
  async updateGitUserToken(idUser?: string, tokenId: number, token: string): Promise<AxiosResponse> {
    console.info('INFO: updateGitUserToken:', idUser, tokenId, token)

    // Default to the current user's ID if idUser is not provided
    idUser = idUser ?? this.user.id

    const data = {
      token_id: tokenId,
      token,
    }

    const config: AxiosRequestConfig = {
      method: 'put',
      url: `${this.uri}/users/${idUser}/gitlab`,
      headers: this.getHeaders(),
      data,
    }

    console.debug('DEBUG: updateGitUserToken:', config)

    return axios(config)
  }

  /**
   * Deletes the GitLab user integration for a specified system user.
   * @param idUser Optional. The ID of the system user whose GitLab integration is to be deleted. Defaults to the current user's ID if not provided.
   * @returns A Promise that resolves to the Axios response indicating the result of the deletion operation.
   */
  async deleteGitUserIntegration(idUser?: string): Promise<AxiosResponse> {
    // Default to the current user's ID if idUser is not provided
    idUser = idUser ?? this.user.id

    const config: AxiosRequestConfig = {
      method: 'delete',
      url: `${this.uri}/users/${idUser}/gitlab`,
      headers: this.getHeaders(),
    }

    console.log('deleteGitUserIntegration:', config)

    return axios(config)
  }
}
