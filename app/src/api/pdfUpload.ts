import store from '@/store'
import type { AxiosRequestConfig } from 'axios'
import axios, { AxiosHeaders } from 'axios'

export default class FileUploadApi {
  private uri: string

  constructor() {
    // Define la URL del endpoint para subir archivos
    this.uri = `${window.BACKEND_URI}/api/v1/upload`
  }

  // Función para obtener los headers de autorización
  private getHeaders() {
    return new AxiosHeaders({
      'Content-Type': 'multipart/form-data', // Importante para enviar archivos
      'Authorization': `Bearer ${store.getters.token}`,
    })
  };

  /**
   * Realiza la subida de un archivo PDF.
   * @param file El archivo PDF a subir.
   * @returns Una promesa que se resuelve con la respuesta de la solicitud de subida.
   */
  async uploadFile(file: File): Promise<any> {
    const formData = new FormData()
    formData.append('file', file) // Agrega el archivo al formulario

    const config: AxiosRequestConfig = {
      method: 'post',
      url: this.uri,
      headers: this.getHeaders(),
      data: formData,
    }

    console.log('Uploading file:', config)
    
    return axios(config)
  }
}
