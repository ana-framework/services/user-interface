// views/index.ts

// TODO THIS IS NOT USED YET
import type { ActionTree, GetterTree, MutationTree, StoreOptions } from 'vuex'
import { createStore } from 'vuex'

interface State {
  user: any
  idProject: string
  idPipeline: string
  idJob: string
  gitlabToken: string
  selectedJobStatus: string[] | undefined
}

const getDefaultState = (): State => ({
  user: null,
  idProject: '0',
  idPipeline: '0',
  idJob: '0',
  gitlabToken: '',
  selectedJobStatus: undefined,
})

const state = getDefaultState()

const mutations: MutationTree<State> = {
  resetState(state) {
    Object.assign(state, getDefaultState())
  },
  setUser(state, user: any) {
    state.user = user
    console.log('user: ', state.user)
  },
  setGitlabToken(state, token: string) {
    state.gitlabToken = token
    console.log('gitlabToken: ', state.gitlabToken)
  },
  setIdProject(state, idProject: string) {
    state.idProject = idProject
    console.log('idProject: ', state.idProject)
  },
  setIdPipeline(state, idPipeline: string) {
    state.idPipeline = idPipeline
    console.log('idPipeline: ', state.idPipeline)
  },
  setIdJob(state, idJob: string) {
    state.idJob = idJob
    console.log('idJob: ', state.idJob)
  },
  setSelectedJobStatus(state, selectedJobStatus: string | string[]) {
    state.selectedJobStatus = Array.isArray(selectedJobStatus) ? selectedJobStatus : [selectedJobStatus]
    console.log('selectedJobStatus: ', state.selectedJobStatus)
  },
}

const actions: ActionTree<State, State> = {
  resetUserState({ commit }) {
    commit('resetState')
  },
  setUser({ commit }, user: any) {
    commit('setUser', user)
  },
  setGitLabToken({ commit }, token: string) {
    commit('setGitLabToken', token)
  },
  setIdProject({ commit }, idProject: string) {
    commit('setIdProject', idProject)
  },
  setIdPipeline({ commit }, idPipeline: string) {
    commit('setIdPipeline', idPipeline)
  },
  setIdJob({ commit }, idJob: string) {
    commit('setIdJob', idJob)
  },
  setSelectedJobStatus({ commit }, selectedJobStatus: string | string[]) {
    commit('setSelectedJobStatus', selectedJobStatus)
  },
}

const getters: GetterTree<State, State> = {
  user: state => {
    return state.user || JSON.parse(sessionStorage.getItem('user') || 'null')
  },
  gitlabToken: state => {
    return state.gitlabToken || sessionStorage.getItem('gitlabToken') || ''
  },
  idProject: state => state.idProject,
  idPipeline: state => state.idPipeline,
  selectedJobStatus: state => state.selectedJobStatus,
}

const module: StoreOptions<State> = {
  state,
  mutations,
  actions,
  getters,
}

const store = createStore({
  state: {
    user: null,
    idProject: '0',
    idPipeline: '0',
    idJob: '0',
    gitlabToken: '',
    selectedJobStatus: undefined,
  },
  mutations: {
    resetState(state) {
      Object.assign(state, getDefaultState())
    },
    setUser(state, user: any) {
      state.user = user
      console.log('user: ', state.user)
    },
    setGitlabToken(state, token: string) {
      state.gitlabToken = token
      console.log('gitlabToken: ', state.gitlabToken)
    },
    setIdProject(state, idProject: string) {
      state.idProject = idProject
      console.log('idProject: ', state.idProject)
    },
    setIdPipeline(state, idPipeline: string) {
      state.idPipeline = idPipeline
      console.log('idPipeline: ', state.idPipeline)
    },
    setIdJob(state, idJob: string) {
      state.idJob = idJob
      console.log('idJob: ', state.idJob)
    },
    setSelectedJobStatus(state, selectedJobStatus: string | string[]) {
      state.selectedJobStatus = Array.isArray(selectedJobStatus) ? selectedJobStatus : [selectedJobStatus]
      console.log('selectedJobStatus: ', state.selectedJobStatus)
    },

    setUser(state, user) {
      state.user = user
      sessionStorage.setItem('user', JSON.stringify(user))
    },
    setToken(state, token) {
      state.token = token
      sessionStorage.setItem('accessToken', token)
    },

    /// setGitlabToken(state, gitlabToken) {
    ///   state.gitlabToken = gitlabToken
    ///   sessionStorage.setItem('gitlabToken', gitlabToken)
    /// },
    deleteUser(state) {
      console.debug('mutation.deleteUser')
      delete state.user
      sessionStorage.removeItem('user')
    },
    deleteToken(state) {
      console.debug('mutation.deleteToken')
      delete state.token
      sessionStorage.removeItem('accessToken')
    },

    /// deleteGitlabToken(state) {
    ///   console.debug('mutation.gitlabToken')
    ///   delete state.gitlabToken
    ///   sessionStorage.removeItem('gitlabToken')
    /// },
  },
  actions: {
    setUser({ commit }, user) {
      commit('setUser', user)
    },
    setToken({ commit }, token) {
      commit('setToken', token)
    },

    /// setGitlabToken({ commit }, gitlabToken) {
    ///   commit('setGitlabToken', gitlabToken)
    /// },
    deleteUser({ commit }) {
      console.debug('actions.deleteUser')
      commit('deleteUser')
    },
    deleteToken({ commit }) {
      console.debug('actions.deleteToken')
      commit('deleteToken')
    },

    /// deleteGitLabToken({ commit }) {
    ///   console.debug('actions.deleteGitLabToken')
    ///   commit('deleteGitLabToken')
    /// },
  },
  getters: {
    user: state => {
      return state.user || JSON.parse(sessionStorage.getItem('user'))
    },
    token: state => {
      return state.token || sessionStorage.getItem('accessToken') || ''
    },

    /// gitlabToken: state => {
    ///   return state.gitlabToken || sessionStorage.getItem('gitlabToken') || ''
    /// },
  },
})

export default createStore<State>(module)
