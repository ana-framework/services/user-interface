import { createStore } from 'vuex'

// Modules
/// / import app from './app';
/// / import appConfig from './app-config';
/// / import verticalMenu from './vertical-menu';

const store = createStore({
  state: {
    user: JSON.parse(sessionStorage.getItem('user')) || null,
    token: sessionStorage.getItem('accessToken') || null,
    gitlabToken: sessionStorage.getItem('gitlabToken') || null,
  },
  modules: {
    /// / app,
    /// / appConfig,
    /// / verticalMenu,
    /// / views,
  },
  mutations: {
    setUser(state, user) {
      state.user = user
      sessionStorage.setItem('user', JSON.stringify(user))
    },
    setToken(state, token) {
      state.token = token
      sessionStorage.setItem('accessToken', token)
    },
    setGitlabToken(state, gitlabToken) {
      state.gitlabToken = gitlabToken
      sessionStorage.setItem('gitlabToken', gitlabToken)
    },
    deleteUser(state) {
      console.debug('mutation.deleteUser')
      delete state.user
      sessionStorage.removeItem('user')
    },
    deleteToken(state) {
      console.debug('mutation.deleteToken')
      delete state.token
      sessionStorage.removeItem('accessToken')
    },
    deleteGitlabToken(state) {
      console.debug('mutation.gitlabToken')
      delete state.gitlabToken
      sessionStorage.removeItem('gitlabToken')
    },
  },
  actions: {
    setUser({ commit }, user) {
      commit('setUser', user)
    },
    setToken({ commit }, token) {
      commit('setToken', token)
    },
    setGitlabToken({ commit }, gitlabToken) {
      commit('setGitlabToken', gitlabToken)
    },
    deleteUser({ commit }) {
      console.debug('actions.deleteUser')
      commit('deleteUser')
    },
    deleteToken({ commit }) {
      console.debug('actions.deleteToken')
      commit('deleteToken')
    },
    deleteGitLabToken({ commit }) {
      console.debug('actions.deleteGitLabToken')
      commit('deleteGitLabToken')
    },
  },
  getters: {
    user: state => state.user,
    token: state => state.token,
    gitlabToken: state => state.gitlabToken,
  },
})

export default store
